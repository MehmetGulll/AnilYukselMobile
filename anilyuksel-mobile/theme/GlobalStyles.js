import { StyleSheet } from "react-native";

export const globalStyles = StyleSheet.create({
    container:{
        backgroundColor:'white'
    },
    colors:{
        tagColor: '#777E90',
        borderBottomColor:"#D6D6D6",
        logoColor:'#B81414'
    }
})