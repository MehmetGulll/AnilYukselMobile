import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import "react-native-gesture-handler";
import Login from "./Login/Login";
import SingIn from "./Login/SignIn";
import Start from "./Login/Start";
import SingUp from "./Login/SignUp";
import DealerShip from "./Login/DealerShip";
import Communication from "./Login/Communication";
import SideBar from "../components/SideBar";
import { GlobalProvider } from "../Context/GlobalStates";
import TransactionDetail from "./Transactions/TransactionDetail";
import B2CSideBar from './B2C/B2CSideBar';

const Stack = createNativeStackNavigator();

function Router() {
  return (
    <GlobalProvider>
      <NavigationContainer>
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          <Stack.Screen name="FirstScreen" component={Start} />
          <Stack.Screen name ="Transactional" component={B2CSideBar} />
          <Stack.Screen name="SecondScreen" component={Login} />
          <Stack.Screen name="ThirthScreen" component={SingIn} />
          <Stack.Screen name="FourthScreen" component={SingUp} />
          <Stack.Screen name="FifthScreen" component={DealerShip} />
          <Stack.Screen name="SixthScreen" component={Communication} />
          <Stack.Screen name="SeventhScreen" component={SideBar} />
          <Stack.Screen
            name="TransactionDetail"
            component={TransactionDetail}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </GlobalProvider>
  );
}

export default Router;
