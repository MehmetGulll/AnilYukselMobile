import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import SignIn from "./Login/SignIn";
import SignUp from "./Login/SignUp";
import B2CHome from "./B2C/B2CHome";
import B2CCampaignProducts from "./B2C/B2CCampaignProducts";
import B2CNewProducts from "./B2C/B2CNewProducts";
import B2CBasket from "./B2C/B2CBasket";
import B2CCommunication from "./B2C/B2CCommunication";
import B2CProductDetail from "./B2C/B2CProductDetail";

const Stack = createNativeStackNavigator();

function B2CRouter() {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="B2CHome" component={B2CHome} />
      <Stack.Screen name="SignIn" component={SignIn} />
      <Stack.Screen name="SignUp" component={SignUp} />
      <Stack.Screen
        name="B2CCampaignProducts"
        component={B2CCampaignProducts}
      />
      <Stack.Screen name="B2CNewProducts" component={B2CNewProducts} />
      <Stack.Screen name="B2CBasket" component={B2CBasket} />
      <Stack.Screen name="B2CCommunication" component={B2CCommunication} />
      <Stack.Screen name="B2CProductDetail" component={B2CProductDetail} />
    </Stack.Navigator>
  );
}

export default B2CRouter;
