import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import BottomTab from "../components/BottomTab";
import ProductDetails from "./Products/ProductDetails";
import Basket from "./Basket/Basket";
import Payment from "./Pay/Payment";
// import Feature from "./Home/Feature";
import NewProduct from "./Products/NewProduct";
import PromotionalProducts from "./Products/PromotionalProducts";
import ContactUs from "./Home/ContactUs";
import Currents from "./Currents/Currents.js";
import Transactions from "./Transactions/Transactions";
import TransactionDetail from "./Transactions/TransactionDetail";
import PaymentResult from "./Pay/PaymentResult";
import PaymentConfirm from "./Pay/PaymentConfirm";
import Home from "./Home/Home";

const Stack = createNativeStackNavigator();

function StackRender() {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="Ana Sayfa" component={BottomTab} />
      <Stack.Screen name = "Home" component={Home} />
      <Stack.Screen name="EighthScreen" component={ProductDetails} />
      <Stack.Screen name="Sepetim" component={Basket} />
      <Stack.Screen name="OdemeYap" component={Payment} />
      <Stack.Screen name="Iletişim" component={ContactUs} />
      {/* <Stack.Screen name="Feature" component={Feature} /> */}
      <Stack.Screen name="NewProduct" component={NewProduct} />
      <Stack.Screen name="Promotional" component={PromotionalProducts} />
      <Stack.Screen name="Currents" component={Currents} />
      <Stack.Screen name="Transactions" component={Transactions} />
      <Stack.Screen name="TransactionDetail" component={TransactionDetail} />
      <Stack.Screen name="PaymentResult" component={PaymentResult} />
      <Stack.Screen name="PaymentConfirm" component={PaymentConfirm} />
    </Stack.Navigator>
  );
}

export default StackRender;
