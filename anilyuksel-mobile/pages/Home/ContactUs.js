import React, { useEffect,useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Linking,
  TouchableOpacity,
} from "react-native";
import Icon from "react-native-vector-icons/AntDesign";
import axios from "axios";

const apihost = "https://b2b.anilyukselboya.com.tr/json/api";

function ContactUs() {
  const [contact, setContact] = useState(null);
  useEffect(() => {
    const communicationInformations = async () => {
      const response = await axios.get(`${apihost}/sitesettings/`);
      setContact(response.data.response_data[0].settings.customerphone);
    };
    communicationInformations();
  },[]);
  return (
    <View style={styles.communicationContainer}>
      <View style={styles.communicationTag}>
        <Text style={styles.communicationTagText}>İletişim Bilgileri</Text>
      </View>
      <View style={styles.communicationContacts}>
        {/* <TouchableOpacity
          style={styles.contactContainer}
          onPress={() => Linking.openURL("mailto:info@stgroup.com.tr")}
        >
          <Icon name="mail" size={30} color="#191970" />
          <Text style={styles.contactText}>info@stgroup.com.tr</Text>
        </TouchableOpacity> */}
        <TouchableOpacity
          style={styles.contactContainer}
          onPress={() => Linking.openURL(`tel:${contact}` )}
        >
          <Icon name="phone" size={30} color="#191970" />
          <Text style={styles.contactText}>{contact}</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.communicationInformations}>
        <Text style={styles.communicationTagText}>Adres Bilgisi</Text>
        <Text style={styles.communicationCompanyInformation}>
          ANIL YÜKSEL BOYA HIRDAVAT İNŞ. MALZ. SAN. VE TİC. LTD. ŞTİ
        </Text>
        <Text style={styles.communicationCompanyInformation}>
          Seyhan Mahallesi 661/5 sokak No.38 Buca, 35400 Karabağlar/İzmir
        </Text>
        <Text style={styles.communicationCompanyInformation}>
          <Text style={{ color: "#777E90" }}>İzmir /</Text>
          <Text style={{ color: "#b81414" }}> Türkiye</Text>
        </Text>
      </View>
      <View style={styles.locationContainer}>
        <Text style={styles.communicationTagText}>Konum Bilgisi</Text>
        <TouchableOpacity
          onPress={() =>
            Linking.openURL(
              "https://www.google.com/maps/place/An%C4%B1l+Y%C3%BCksel+Boya+H%C4%B1rdavat+%C4%B0n%C5%9F.+Malz.+San.+ve+Tic.+Ltd.+%C5%9Eti./@38.3769935,27.1407583,15z/data=!4m2!3m1!1s0x0:0xb4f55b6887041755?sa=X&ved=2ahUKEwjwqrKgx-2BAxXCh_0HHR8kCcsQ_BJ6BAg8EAA&ved=2ahUKEwjwqrKgx-2BAxXCh_0HHR8kCcsQ_BJ6BAhTEAg"
            )
          }
        >
          <Text style={styles.mapNavigation}>
            Konumu Görmek İçin Tıklayınız
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default ContactUs;

const styles = StyleSheet.create({
  communicationContainer: {
    flex: 1,
    backgroundColor: "white",
  },
  communicationTag: {
    marginTop: 10,
    alignItems: "center",
  },
  backIcon: {
    marginTop: 50,
    flexDirection: "row",
    marginLeft: 20,
  },
  communicationTagText: {
    fontWeight: "bold",
    fontSize: 25,
    color: "#777E90",
  },
  communicationInformations: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  communicationContacts: {
    alignItems: "center",
    padding: 15,
    flexDirection: "column",
  },
  contactText: {
    color: "#191970",
    marginLeft: 20,
    fontWeight: "bold",
  },
  contactContainer: {
    flexDirection: "row",
    borderWidth: 2,
    borderRadius: 50,
    borderColor: "#e9e9e9",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15,
    padding: 15,
    width: "100%",
  },
  communicationInformations: {
    margin: 10,
    marginTop: 30,
    alignItems: "center",
  },
  communicationCompanyInformation: {
    textAlign: "center",
    marginTop: 5,
    color: "#191970",
    fontWeight: "bold",
    fontSize: 20,
  },
  locationContainer: {
    flex: 1,
    alignItems: "center",
  },
  mapNavigation: {
    padding: 10,
    borderRadius: 20,
    marginTop: 15,
    color: "#09A3C5",
    backgroundColor: "white",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 10,
  },
});
