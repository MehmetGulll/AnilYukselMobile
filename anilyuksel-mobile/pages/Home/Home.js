import React, { useContext } from "react";
import Cards from "./Cards";
import DropDownHome from '../../components/DropDownHome'
import { ScrollView } from "react-native";
import { globalStyles } from "../../theme/GlobalStyles";
import { GlobalContext } from "../../Context/GlobalStates";


function Home() {
  const {selectedSellerName, setSelectedSellerName,currentType} = useContext(GlobalContext);
  return (
    <ScrollView style = {globalStyles.container}>
      {currentType!=="b2c" && <DropDownHome text={selectedSellerName} /> }
      <Cards />
    </ScrollView>
  );
}
export default Home;


