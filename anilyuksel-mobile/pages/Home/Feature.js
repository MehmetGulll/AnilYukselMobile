// import React, { useContext, useEffect, useState } from "react";
// import {
//   View,
//   Text,
//   StyleSheet,
//   FlatList,
//   TouchableOpacity,
//   Image,
//   ScrollView,
//   Dimensions
// } from "react-native";
// import axios from "axios";
// import { useNavigation } from "@react-navigation/native";
// import { SliderBox } from "react-native-image-slider-box";
// import { GlobalContext } from "../../Context/GlobalStates";
// import AsyncStorage from "@react-native-async-storage/async-storage";

// const apihost = "https://b2b.anilyukselboya.com.tr/json/api";

// function Feature() {
//   const navigation = useNavigation();
//   const { product, setAllProducts, allProducts } = useContext(GlobalContext);
//   const [displayProducts, setDisplayProducts] = useState([]);
//   useEffect(()=>{
//     const randomProducts = allProducts.sort(()=> 0.5 - Math.random()).slice(0,10);
//     setDisplayProducts(randomProducts)
//   },[])
//   const images = [
//     require("../../assets/tempo3000.jpg"),
//     require("../../assets/smart3000.jpg"),
//     require("../../assets/kobra500.jpg"),
//   ];
//   {console.log(allProducts)}
  

  
//   return (
//     <ScrollView style={{ backgroundColor: "white" }}>
//       <SliderBox
//         images={images}
//         dotColor="#B81414"
//         inactiveDotColor="black"
//         dotStyle={{ height: 20, width: 20, borderRadius: 50 }}
//         imageLoadingColor="black"
//         autoplay={true}
//         autoplayInterval={4000}
//         circleLoop={true}
//         sliderBoxHeight={250}
//       />
//       <View style={styles.homeProductsContainer}>
//         <View style={styles.homeProductsTitles}>
//           <View style={styles.productText}>
//             <Text style={{ fontSize: 20, fontWeight: 700 }}>Ürünler</Text>
//           </View>
//           <View style={styles.allProduct}>
//             <Text
//               style={{ fontSize: 13, color: "#9B9B9B" }}
//               onPress={()=>navigation.navigate("Ürün Ara")}
//             >
//               Hepsini Gör
//             </Text>
//           </View>
//         </View>
//         <View style={styles.itemMainContainer}>
//           <FlatList
//             data={displayProducts}
//             horizontal
//             keyExtractor={(item) => item.id}
//             renderItem={({ item }) => (
//               <TouchableOpacity
//                 style={styles.itemContainer}
//                 onPress={() => navigation.navigate("EighthScreen", { item })}
//               >
//                 <Image source={{uri: item.Picture[0]}} style={styles.itemImage} />
//                 <Text style={styles.itemStock}>Stok Adet: {item.StockCount}</Text>
//                 <Text style={styles.itemName}>{item.Name}</Text>
//                 <Text style={styles.itemCode}>{item.ProductCode}</Text>
//                 <Text style={styles.itemNewPrice}>
//                 {new Intl.NumberFormat("tr-TR", {
//                   style: "currency",
//                   currency: "TRY",
//                 }).format(Number(item.ItemPrice))}
//                 </Text>
//               </TouchableOpacity>
//             )}
//           />
//         </View>
//         <View style={styles.homeProductsTitles}>
//           <View style={styles.productText}>
//             <Text style={{ fontSize: 20, fontWeight: 700 }}>Kampanyalı</Text>
//           </View>
//           <View style={styles.allProduct}>
//             <Text
//               style={{ fontSize: 13, color: "#9B9B9B" }}
//               onPress={() => navigation.navigate("Promotional")}
//             >
//               Hepsini Gör
//             </Text>
//           </View>
//         </View>
//         <View style={styles.itemMainContainer}>
//           <FlatList
//             data={product}
//             keyExtractor={(item) => item.id}
//             horizontal
//             renderItem={({ item }) => (
//               <TouchableOpacity
//                 style={styles.itemContainerOffer}
//                 onPress={() => navigation.navigate("EighthScreen", { item })}
//               >
//                 <Image source={item.image} style={styles.itemImageOffer} />
//                 <View style={styles.itemInformations}>
//                   <Text style={styles.itemName}>{item.title}</Text>
//                   <Text style={styles.itemCode}>{item.productCode}</Text>
//                   <Text style={styles.itemNewPrice}>
//                     {new Intl.NumberFormat("tr-TR", {
//                       style: "currency",
//                       currency: "TRY",
//                     }).format(
//                       Number(item.oldPrice) -
//                         Number(item.oldPrice) * Number(item.discount / 100)
//                     )}
//                   </Text>
//                 </View>
//               </TouchableOpacity>
//             )}
//           />
//         </View>
//         <View style={styles.homeProductsTitles}>
//           <View style={styles.productText}>
//             <Text
//               style={{ fontSize: 20, fontWeight: 700 }}
//               onPress={() => navigation.navigate("")}
//             >
//               Yeni Ürünler
//             </Text>
//           </View>
//           <View style={styles.allProduct}>
//             <Text
//               style={{ fontSize: 13, color: "#9B9B9B" }}
//               onPress={() => navigation.navigate("Ürün Ara")}
//             >
//               Hepsini Gör
//             </Text>
//           </View>
//         </View>
//         <View style={styles.itemMainContainer}>
//           <FlatList
//             data={product}
//             horizontal
//             keyExtractor={(item) => item.id}
//             renderItem={({ item }) => (
//               <TouchableOpacity
//                 style={styles.itemContainer}
//                 onPress={() => navigation.navigate("EighthScreen", { item })}
//               >
//                 <Image source={item.image} style={styles.itemImage} />
//                 <Text style={styles.itemStock}>Stok Adet: {item.stock}</Text>
//                 <Text style={styles.itemName}>{item.title}</Text>
//                 <Text style={styles.itemCode}>{item.productCode}</Text>
//                 <Text style={styles.itemNewPrice}>
//                   {new Intl.NumberFormat("tr-TR", {
//                     style: "currency",
//                     currency: "TRY",
//                   }).format(
//                     Number(item.oldPrice) -
//                       Number(item.oldPrice) * Number(item.discount / 100)
//                   )}
//                 </Text>
//               </TouchableOpacity>
//             )}
//           />
//         </View>
//       </View>
//     </ScrollView>
//   );
// }

// const styles = StyleSheet.create({
//   homeProductsContainer: {
//     alignItems: "center",
//   },

//   homeProductsTitles: {
//     flexDirection: "row",
//     marginTop: 30,
//   },
//   productText: {
//     flex: 1,
//     alignItems: "center",
//     justifyContent: "center",
//   },
//   allProduct: {
//     flex: 1,
//     alignItems: "center",
//     justifyContent: "center",
//   },
//   itemContainer: {
//     flexDirection: "column",
//     alignItems: "center",
//     marginTop: 20,
//     margin: 5,
//     width: Dimensions.get('window').width/2
//   },
//   itemContainerOffer: {
//     flexDirection: "row",
//     alignItems: "center",
//     marginLeft: 8,
//     marginTop: 20,
//   },
//   itemInformations: {
//     marginRight: 10,
//     alignItems: "center",
//   },
//   itemMainContainer: {
//     alignItems: "center",
//     flex: 1,
//   },
//   itemName: {
//     color: "#212121",
//     fontWeight: "700",
//     padding: 10,
//     textAlign:'center'
//   },
//   itemCode: {
//     color: "grey",
//     justifyContent: "center",
//   },
//   itemNewPrice: {
//     marginTop: 5,
//     marginBottom: 15,
//     fontWeight: "700",
//   },
//   itemImage: {
//     width: 150,
//     height: 150,
//     borderRadius: 28,
//     margin: 10,
//     backgroundColor:'grey'
//   },
//   itemImageOffer: {
//     width: 100,
//     height: 100,
//     borderRadius: 28,
//     margin: 10,
//   },
// });

// export default Feature;
