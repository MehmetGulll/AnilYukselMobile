import React,{useContext} from "react";
import { View, StyleSheet, Text, Image, TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { GlobalContext } from "../../Context/GlobalStates";

const apihost = "https://b2b.anilyukselboya.com.tr/json/api";

function Cards() {
  const {currentType} = useContext(GlobalContext);
  const navigation = useNavigation();
  return (
    <View style={styles.cardsContainer}>
      <TouchableOpacity
        style={styles.cardContainer}
        onPress={() => navigation.navigate("Ürün Ara")}
      >
        <Text style={styles.cardTitleText}>Ürünler</Text>
        <Image
          source={{
            uri: "http://www.anaviboya.com/images/slide/04.jpg",
          }}
          style={styles.cardImage}
        />
      </TouchableOpacity>
      {/* <TouchableOpacity
        style={styles.cardContainer}
        onPress={() => navigation.navigate("Promotional")}
      >
        <Text style={styles.cardTitleText}>Kampanyalı Ürünler</Text>
        <Image
          source={{
            uri: "https://www.alastyr.com/blog/wp-content/uploads/2022/05/kampanya-planlama.jpg",
          }}
          style={styles.cardImage}
        />
      </TouchableOpacity> */}
      <TouchableOpacity
        style={styles.cardContainer}
        onPress={() => navigation.navigate("Sepet")}
      >
        <Text style={styles.cardTitleText}>Sepetim</Text>
        <Image
          source={{
            uri: "https://www.nukteler.com/wp-content/uploads/2022/05/alisveris_sepeti.jpg",
          }}
          style={styles.cardImage}
        />
      </TouchableOpacity>
      {currentType !=="b2c" && <TouchableOpacity
        style={styles.cardContainer}
        onPress={() => navigation.navigate("Currents")}
      >
        <Text style={styles.cardTitleText}>Cari Hesabım</Text>
        <Image
          source={{
            uri: "https://cdn.emlakkulisi.com/resim/orjinal/MzMzOTAxNj-cari-hesap-nedir-cari-hesap-takibi-nasil-yapilir-2022.jpeg",
          }}
          style={styles.cardImage}
        />
      </TouchableOpacity> }
      <TouchableOpacity
        style={styles.cardContainer}
        onPress={() => navigation.navigate("Ödeme Yap")}
      >
        <Text style={styles.cardTitleText}>Ödeme Yap</Text>
        <Image
          source={{
            uri: "https://i.bigpara.com/resize/650x365/i/55big/27102015_kredi_karti.jpg",
          }}
          style={styles.cardImage}
        />
      </TouchableOpacity>

    </View>
  );  
}

export default Cards;

const styles = StyleSheet.create({
  cardContainer: {},
  cardsContainer: {
    marginTop: 10,
  },
  cardTitleText: {
    fontSize: 20,
    fontWeight: "700",
    marginLeft: 40,
  },
  cardImage: {
    margin: 10,
    width: "%100",
    height: 170,
    borderRadius: 28,
  },
});
