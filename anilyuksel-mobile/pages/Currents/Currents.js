import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { useContext, useEffect, useState } from "react";
import {
  View,
  Alert,
  FlatList,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import Button from "../../components/Button";
import axios from "axios";
import DropDownHome from "../../components/DropDownHome";
import { GlobalContext } from "../../Context/GlobalStates";

const apihost = "https://b2b.anilyukselboya.com.tr/json/api";

function Currents() {
  const navigation = useNavigation();
  const {selectedSellerName, setSelectedSellerName,currentType} = useContext(GlobalContext);
  const [currents, setCurrents] = useState([]);
  const [authUser, setAuthUser] = useState(null);
  const [seller, setSeller] = useState(null);

  const handleSellerChange = async(selectedSeller) =>{
    setCurrents([])
    await refresh(selectedSeller)
  }

  useEffect(() => {
    const fetchAuthUserAndSeller = async () => {
      const authUser = JSON.parse(await AsyncStorage.getItem("authUser"));
      console.log(authUser.token);
      const seller = JSON.parse(await AsyncStorage.getItem("seller"));
      setAuthUser(authUser);
      setSeller(seller);

    };
    fetchAuthUserAndSeller();
  }, []);
 

  useEffect(() => {
   const fetchData = async ()=>{
    if(authUser && seller){
      await refresh();
    }
   }
   fetchData();
  }, [authUser, seller, selectedSellerName]);

  useEffect(()=>{
    console.log("Seller güncellendi",currents)
  },[currents])

  useEffect(()=>{
    if(currents.length === 1){
      navigation.navigate("Transactions",{current: currents[0]})
    }
  })

  const refresh = async () => {
    try {
      const response = await axios.get(
        `${apihost}/currents/?token=${authUser.token}&seller_id=${seller.id}`
      );
      if (response.data.response_status) {
        setCurrents(response.data.response_data);
        if (currents.length === 1) {
          navigation.navigate("Transactions", { current: current[0] });
        }
      } else {
        Alert.alert("Oops..", response.data.response_message, [
          { text: "Tamam" },
        ]);
      }
    } catch (error) {
      console.log("error", error);
    }
  };

  const handlePress = (current) => {
    navigation.navigate("Transactions", { current });
  };

  return (
    <View style={{ backgroundColor: "white", flex: 1 }}>
      {currentType && <DropDownHome text={selectedSellerName}/> }
      <FlatList
        data={currents}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <View style = {{margin:25}}>
            <Button
              text={item.Code}
              backgroundColor='#343434'
              color="#FFF"
              onPress={() => {
                handlePress(item);
              }}
            />
          </View>
        )}
      />
    </View>
  );
}

export default Currents;
