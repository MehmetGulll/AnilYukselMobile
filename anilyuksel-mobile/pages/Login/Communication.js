import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Linking,
  TouchableOpacity,
} from "react-native";
import BackIcon from "../../components/BackIcon";
import Icon from "react-native-vector-icons/AntDesign";


function Communication() {
  return (
    <View style={styles.communicationContainer}>
      <BackIcon />
      <View style={styles.communicationTag}>
        <Text style={styles.communicationTagText}>İletişim Bilgileri</Text>
      </View>
      <View style={styles.communicationContacts}>
        {/* <TouchableOpacity
          style={styles.contactContainer}
          onPress={() => Linking.openURL("mailto:info@stgroup.com.tr")}
        >
          <Icon name="mail" size={30} color="#191970" />
          <Text style={styles.contactText}>info@stgroup.com.tr</Text>
        </TouchableOpacity> */}
        <TouchableOpacity
          style={styles.contactContainer}
          onPress={() => Linking.openURL("tel: 0258 251 51 51")}
        >
          <Icon name="phone" size={30} color="#191970" />
          <Text style={styles.contactText}>0(258) 251 51 51</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.communicationInformations}>
        <Text style={styles.communicationTagText}>Adres Bilgisi</Text>
        <Text style={styles.communicationCompanyInformation}>
          ST GRUP ENDÜSTRİYEL ÜRÜNLER BİLGİSAYAR SAN. VE TİC. LTD. ŞTİ.
        </Text>
        <Text style={styles.communicationCompanyInformation}>
          Organize Sanayi Bölgesi 1.Kısım Vali Münir Güney Caddesi No:6
        </Text>
        <Text style={styles.communicationCompanyInformation}>
          <Text style={{ color: "#777E90" }}>Denizli /</Text>
          <Text style={{ color: "#b81414" }}> Türkiye</Text>
        </Text>
      </View>
      <View style={styles.locationContainer}>
        <Text style={styles.communicationTagText}>Konum Bilgisi</Text>
        <TouchableOpacity
          onPress={() =>
            Linking.openURL(
              "https://www.google.com/maps/place/Vatan+Bilgisayar+Bornova/@38.448https://www.google.com/maps/place/ST+Group/@37.8106655,29.2403736,17z/data=!3m1!4b1!4m6!3m5!1s0x14c719d1c44a117b:0xe2864c67a72ce663!8m2!3d37.8106613!4d29.2429485!16s%2Fg%2F11flz3yvvf?entry=ttu"
            )
          }
        >
          <Text style={styles.mapNavigation}>
            Konumu Görmek İçin Tıklayınız
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default Communication;

const styles = StyleSheet.create({
  communicationContainer: {
    flex: 1,
    backgroundColor: "white",
  },
  communicationTag: {
    marginTop: 10,
    alignItems: "center",
  },
  backIcon: {
    marginTop: 50,
    flexDirection: "row",
    marginLeft: 20,
  },
  communicationTagText: {
    fontWeight: "bold",
    fontSize: 25,
    color: "#777E90",
  },
  communicationInformations: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  communicationContacts: {
    alignItems: "center",
    padding: 15,
    flexDirection: "column",
  },
  contactText: {
    color: "#191970",
    marginLeft: 20,
    fontWeight: "bold",
  },
  contactContainer: {
    flexDirection: "row",
    borderWidth: 2,
    borderRadius: 50,
    borderColor: "#e9e9e9",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15,
    padding: 15,
    width: "100%",
  },
  communicationInformations: {
    margin: 10,
    marginTop: 30,
    alignItems: "center",
  },
  communicationCompanyInformation: {
    textAlign: "center",
    marginTop: 5,
    color: "#191970",
    fontWeight: "bold",
    fontSize: 20,
  },
  locationContainer: {
    flex: 1,
    alignItems: "center",
  },
  mapNavigation: {
    padding: 10,
    borderRadius: 20,
    marginTop: 15,
    color: "#09A3C5",
    backgroundColor: "white",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 10,
  },
});
