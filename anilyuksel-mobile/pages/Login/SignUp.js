import React, { useState, useRef, useReducer } from "react";
import {
  View,
  StyleSheet,
  Text,
  Animated,
  ScrollView,
  Alert,
  TouchableOpacity,
  Modal,
} from "react-native";
import Checkbox from "expo-checkbox";
import { useNavigation } from "@react-navigation/native";
import Button from "../../components/Button";
import Logo from "../../components/Logo";
import Input from "../../components/Input";
// import BackIcon from "../../components/BackIcon";
import axios from "axios";

const apihost = "https://b2b.anilyukselboya.com.tr/json/api";

function SingUp() {
  const navigation = useNavigation();
  const [userNameFocus, setUserNameFocus] = useState(false);
  const [passwordFocus, setPasswordFocus] = useState(false);
  const [emailFocus, setEmailFocus] = useState(false);
  const [nameFocus, setNameFocus] = useState(false);
  const [surnameFocus, setSurnameFocus] = useState(false);
  const [cityFocus, setCityFocus] = useState(false);
  const [countyFocus, setCountyFocus] = useState(false);
  const [addressFocus, setAddressFocus] = useState(false);
  const [phoneFocus, setPhoneFocus] = useState(false);
  const [taxnoFocus, setTaxnoFocus] = useState(false);
  const [kvkkChecked, setKvkkChecked] = useState(false);
  const [memberChecked, setMemberChecked] = useState(false);
  const [kvkkModalVisible, setKvkkModalVisible] = useState(false);
  const [memberModalVisible, setMemberModalVisible] = useState(false);
  const [signUpForm, setSignUpForm] = useState({
    username: "",
    password: "",
    password_repeat: "",
    mail: "",
    firstname: "",
    lastname: "",
    city: "",
    county: "",
    address: "",
    phone: "",
    taxno: "",
  });
  const signUp = async () => {
    if (
      !signUpForm.username ||
      !signUpForm.password ||
      !signUpForm.password_repeat ||
      !signUpForm.mail ||
      !signUpForm.firstname ||
      !signUpForm.lastname ||
      !signUpForm.city ||
      !signUpForm.county ||
      !signUpForm.address ||
      !signUpForm.phone
    ) {
      Alert.alert("Hata", "Lütfen işaretli alanları doldurunuz", [
        { text: "Tamam", style: "cancel" },
      ]);
    }
    if (kvkkChecked !== true && memberChecked !== true) {
      Alert.alert("Hata", "Lütfen sözleşmeleri onaylayının", [
        { text: "Tamam", style: "cancel" },
      ]);
    }
    if (
      signUpForm.username ||
      signUpForm.password ||
      signUpForm.password_repeat ||
      signUpForm.mail ||
      signUpForm.firstname ||
      signUpForm.lastname ||
      signUpForm.city ||
      signUpForm.county ||
      signUpForm.address ||
      signUpForm.phone ||
      kvkkChecked === true ||
      memberChecked === true
    ) {
      try {
        const {
          username,
          password,
          password_repeat,
          mail,
          firstname,
          lastname,
          city,
          county,
          phone,
          taxno,
        } = signUpForm;
        const response = await axios.get(
          `${apihost}/signup-new/?username=${username}&password=${password}&password_repeat=${password_repeat}&mail=${mail}&firstName=${firstname}&lastName=${lastname}&city=${city}&county=${county}&phone=${phone}&taxno=${taxno}`
        );
        console.log(response.data);
        if (response.data.response_code) {
          console.log("Başarılı");
          Alert.alert("Başarılı", response.data.response_message, [
            {
              text: "Tamam",
              onPress: () => navigation.navigate("SignIn"),
            },
          ]);
        } else {
          Alert.alert("Hata", response.data.response_message, [
            {
              text: "Tamam",
              onPress: () => navigation.navigate("SignIn"),
            },
          ]);
        }
      } catch (error) {
        console.log("Hata:", error);
      }
    }
  };

  const animatedIsFocusUserName = useRef(new Animated.Value(0)).current;
  const animatedIsFocusPassword = useRef(new Animated.Value(0)).current;
  const animatedIsFocusEmail = useRef(new Animated.Value(0)).current;
  const animatedIsFocusName = useRef(new Animated.Value(0)).current;
  const animatedIsFocusSurname = useRef(new Animated.Value(0)).current;
  const animatedIsFocusCity = useRef(new Animated.Value(0)).current;
  const animatedIsFocusCounty = useRef(new Animated.Value(0)).current;
  const animatedIsFocusAddress = useRef(new Animated.Value(0)).current;
  const animatedIsFocusPhone = useRef(new Animated.Value(0)).current;
  const handleFocusUserName = () => {
    setUserNameFocus(true),
      Animated.timing(animatedIsFocusUserName, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: false,
      }).start();
  };
  const handleFocusPassword = () => {
    setPasswordFocus(true),
      Animated.timing(animatedIsFocusPassword, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: false,
      }).start();
  };
  const handleFocusEmail = () => {
    setEmailFocus(true),
      Animated.timing(animatedIsFocusEmail, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: false,
      }).start();
  };
  const handleFocusName = () => {
    setNameFocus(true),
      Animated.timing(animatedIsFocusName, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: false,
      }).start();
  };
  const handleFocusSurname = () => {
    setSurnameFocus(true),
      Animated.timing(animatedIsFocusSurname, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: false,
      }).start();
  };
  const handleFocusCity = () => {
    setCityFocus(true),
      Animated.timing(animatedIsFocusCity, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: false,
      }).start();
  };
  const handleFocusCounty = () => {
    setCityFocus(true),
      Animated.timing(animatedIsFocusCounty, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: false,
      }).start();
  };
  const handleFocusAddress = () => {
    setCityFocus(true),
      Animated.timing(animatedIsFocusAddress, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: false,
      }).start();
  };
  const handleFocusPhone = () => {
    setCityFocus(true),
      Animated.timing(animatedIsFocusPhone, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: false,
      }).start();
  };
  const handleBlurUserName = () => {
    setUserNameFocus(false),
      Animated.timing(animatedIsFocusUserName, {
        toValue: 0,
        duration: 1000,
        useNativeDriver: false,
      }).start();
  };
  const handleBlurPassword = () => {
    setPasswordFocus(false),
      Animated.timing(animatedIsFocusPassword, {
        toValue: 0,
        duration: 1000,
        useNativeDriver: false,
      }).start();
  };
  const handleBlurEmail = () => {
    setEmailFocus(false),
      Animated.timing(animatedIsFocusEmail, {
        toValue: 0,
        duration: 1000,
        useNativeDriver: false,
      }).start();
  };
  const handleBlurName = () => {
    setNameFocus(false),
      Animated.timing(animatedIsFocusName, {
        toValue: 0,
        duration: 1000,
        useNativeDriver: false,
      }).start();
  };
  const handleBlurSurname = () => {
    setSurnameFocus(false),
      Animated.timing(animatedIsFocusSurname, {
        toValue: 0,
        duration: 1000,
        useNativeDriver: false,
      }).start();
  };
  const handleBlurCity = () => {
    setCityFocus(false),
      Animated.timing(animatedIsFocusCity, {
        toValue: 0,
        duration: 1000,
        useNativeDriver: false,
      }).start();
  };
  const handleBlurCounty = () => {
    setCountyFocus(false),
      Animated.timing(animatedIsFocusCounty, {
        toValue: 0,
        duration: 1000,
        useNativeDriver: false,
      }).start();
  };
  const handleBlurAddress = () => {
    setAddressFocus(false),
      Animated.timing(animatedIsFocusAddress, {
        toValue: 0,
        duration: 1000,
        useNativeDriver: false,
      }).start();
  };
  const handleBlurPhone = () => {
    setCountyFocus(false),
      Animated.timing(animatedIsFocusPhone, {
        toValue: 0,
        duration: 1000,
        useNativeDriver: false,
      }).start();
  };

  const openMemberText = () => {
    setMemberModalVisible(!memberModalVisible);
  };
  const openKvkkText = () => {
    setKvkkModalVisible(!kvkkModalVisible);
  };

  return (
    <ScrollView style={{ backgroundColor: "#fff", flex: 1 }}>
      {/* <BackIcon /> */}
      <View style={styles.logoContainer}>
        <Logo />
      </View>
      <View style={styles.signUpTagContainer}>
        <Text style={styles.signUpTagText}>Üye Ol</Text>
      </View>
      <View style={styles.singUpContainer}>
        <View style={styles.inputInformations}>
          <Input
            placeholder={"* Kullanıcı Adı"}
            onChangeText={(text) =>
              setSignUpForm((prevState) => ({ ...prevState, username: text }))
            }
            onFocus={handleFocusUserName}
            onBlur={handleBlurUserName}
          />
          <Animated.View
            style={[
              styles.animatonBorderColor,
              {
                width: animatedIsFocusUserName.interpolate({
                  inputRange: [0, 1],
                  outputRange: ["0%", "100%"],
                }),
              },
            ]}
          />

          <Input
            placeholder={"* Sifre"}
            secureTextEntry={true}
            onChangeText={(text) =>
              setSignUpForm((prevState) => ({ ...prevState, password: text }))
            }
            onFocus={handleFocusPassword}
            onBlur={handleBlurPassword}
          />
          <Animated.View
            style={[
              styles.animatonBorderColor,
              {
                width: animatedIsFocusPassword.interpolate({
                  inputRange: [0, 1],
                  outputRange: ["0%", "100%"],
                }),
              },
            ]}
          />
          <Input
            placeholder={"* Sifre Tekrar"}
            secureTextEntry={true}
            onChangeText={(text) =>
              setSignUpForm((prevState) => ({
                ...prevState,
                password_repeat: text,
              }))
            }
            onFocus={handleFocusPassword}
            onBlur={handleBlurPassword}
          />
          <Animated.View
            style={[
              styles.animatonBorderColor,
              {
                width: animatedIsFocusPassword.interpolate({
                  inputRange: [0, 1],
                  outputRange: ["0%", "100%"],
                }),
              },
            ]}
          />
          <Input
            placeholder={"* Mail Adresiniz"}
            onChangeText={(text) =>
              setSignUpForm((prevState) => ({ ...prevState, mail: text }))
            }
            onFocus={handleFocusEmail}
            onBlur={handleBlurEmail}
          />
          <Animated.View
            style={[
              styles.animatonBorderColor,
              {
                width: animatedIsFocusEmail.interpolate({
                  inputRange: [0, 1],
                  outputRange: ["0%", "100%"],
                }),
              },
            ]}
          />
          <Input
            placeholder={"* Adınız"}
            onChangeText={(text) =>
              setSignUpForm((prevState) => ({ ...prevState, firstname: text }))
            }
            onFocus={handleFocusName}
            onBlur={handleBlurName}
          />
          <Animated.View
            style={[
              styles.animatonBorderColor,
              {
                width: animatedIsFocusName.interpolate({
                  inputRange: [0, 1],
                  outputRange: ["0%", "100%"],
                }),
              },
            ]}
          />
          <Input
            placeholder={"* Soyadınız"}
            onChangeText={(text) =>
              setSignUpForm((prevState) => ({ ...prevState, lastname: text }))
            }
            onFocus={handleFocusSurname}
            onBlur={handleBlurSurname}
          />
          <Animated.View
            style={[
              styles.animatonBorderColor,
              {
                width: animatedIsFocusSurname.interpolate({
                  inputRange: [0, 1],
                  outputRange: ["0%", "100%"],
                }),
              },
            ]}
          />
          <Input
            placeholder={"* İl"}
            onChangeText={(text) =>
              setSignUpForm((prevState) => ({ ...prevState, city: text }))
            }
            onFocus={handleFocusCity}
            onBlur={handleBlurCity}
          />
          <Animated.View
            style={[
              styles.animatonBorderColor,
              {
                width: animatedIsFocusCity.interpolate({
                  inputRange: [0, 1],
                  outputRange: ["0%", "100%"],
                }),
              },
            ]}
          />
          <Input
            placeholder={"* İlçe"}
            onChangeText={(text) =>
              setSignUpForm((prevState) => ({ ...prevState, county: text }))
            }
            onFocus={handleFocusCounty}
            onBlur={handleBlurCounty}
          />
          <Animated.View
            style={[
              styles.animatonBorderColor,
              {
                width: animatedIsFocusCounty.interpolate({
                  inputRange: [0, 1],
                  outputRange: ["0%", "100%"],
                }),
              },
            ]}
          />

          <Input
            placeholder={"* Adres"}
            onChangeText={(text) =>
              setSignUpForm((prevState) => ({ ...prevState, address: text }))
            }
            onFocus={handleFocusAddress}
            onBlur={handleBlurAddress}
          />
          <Animated.View
            style={[
              styles.animatonBorderColor,
              {
                width: animatedIsFocusAddress.interpolate({
                  inputRange: [0, 1],
                  outputRange: ["0%", "100%"],
                }),
              },
            ]}
          />
          <Input
            placeholder={"* Telefon Numarası"}
            onChangeText={(text) =>
              setSignUpForm((prevState) => ({ ...prevState, phone: text }))
            }
            onFocus={handleFocusPhone}
            onBlur={handleBlurPhone}
          />
          <Animated.View
            style={[
              styles.animatonBorderColor,
              {
                width: animatedIsFocusPhone.interpolate({
                  inputRange: [0, 1],
                  outputRange: ["0%", "100%"],
                }),
              },
            ]}
          />
          <Input
            placeholder={"TC Numarası"}
            onChangeText={(text) =>
              setSignUpForm((prevState) => ({ ...prevState, taxno: text }))
            }
          />
          <View style={{ flexDirection: "row", marginTop: 8 }}>
            <Checkbox value={kvkkChecked} onValueChange={setKvkkChecked} />
            <TouchableOpacity onPress={() => openKvkkText()}>
              <Text style={{ color: "blue", fontSize: 16, marginLeft: 3 }}>
                KVKK Metinini
              </Text>
            </TouchableOpacity>
            <Text style={{ fontSize: 16, marginLeft: 3 }}>Onaylıyorum.</Text>
          </View>
          <Modal
            animationType="slide"
            transparent={true}
            visible={kvkkModalVisible}
          >
            <ScrollView>
              <View style={{ backgroundColor: "#f0f8ff", flex: 1 }}>
                <View>
                  <View style={{ alignItems: "center", marginTop: 10 }}>
                    <Text style={{ fontSize: 28, color: "#666666" }}>
                      KVKK Aydınlatma Metni
                    </Text>
                  </View>
                  <View style={{ marginTop: 10, marginHorizontal: 15 }}>
                    <Text style={{ color: "grey" }}>
                      ANIL YÜKSEL BOYA HIRDAVAT İNŞ.MLZ.SAN VE TİC.LTD.ŞTİ.
                    </Text>
                  </View>
                  <View style={{ marginHorizontal: 15 }}>
                    <Text>
                      KİŞİSEL VERİLERİN KORUNMASI HAKKINDA AYDINLATMA METNİ
                    </Text>
                  </View>
                  <View style={{ marginTop: 35, marginHorizontal: 15 }}>
                    <Text>
                      Kişisel verilerin korunmasını amaçlayan 6698 sayılı
                      Kişisel Verilerin Korunması Kanunu (“KVKK”) nun “Veri
                      Sorumlusunun Aydınlatma Yükümlülüğü” başlıklı 10. maddesi
                      ile Aydınlatma Yükümlülüğünün Yerine Getirilmesinde
                      Uyulacak Usul ve Esaslar Hakkında Tebliğ uyarınca işbu
                      Aydınlatma Metni ile veri sorumlusu sıfatıyla ANIL YÜKSEL
                      BOYA HIRDAVAT İNŞ.MLZ.SAN VE TİC.LTD.ŞTİ. (“Anıl” veya
                      “Şirket”) olarak işlenen kişisel verileriniz hakkında
                      sizleri bilgilendirmeyi hedeflemekteyiz.
                    </Text>
                  </View>
                  <View style={styles.contractTag}>
                    <Text style={styles.contractTagText}>
                      1. İŞLENEN KİŞİSEL VERİLER
                    </Text>
                  </View>
                  <View style={styles.contractText}>
                    <Text>
                      Anıl ile ilişkiniz kapsamında kimlik bilgileriniz (adı
                      soyadı, anne-baba adı, nüfus cüzdanı, TCKN, uyruk, medeni
                      hal, doğum tarihi/yeri, vergi kimlik numarası, ehliyet,
                      pasaport numarası) iletişim bilgileriniz (ev/işyeri
                      adresi, e-posta, cep telefonu, ikametgâh, kayıtlı
                      elektronik posta adresi -KEP), müşteri işlem bilgisi
                      (şikâyet ve talep bilgisi, fatura, senet, çek bilgileri,
                      sipariş bilgisi), işlem güvenliği bilgileriniz (IP
                      adresi), banka hesap verileriniz (banka hesap numarası,
                      fatura/borç bilgisi, IBAN numarası, kredi kartı numarası),
                      şirket yerleşkelerimizin güvenliğine ilişkin veriler (CCTV
                      kayıtları), iş/profesyonel yaşam ve eğitim verileriniz
                      (meslek, mezun olunan okul, meslek için alınan eğitim,
                      sertifikalar, özgeçmiş) ve özel nitelikli verileriniz
                      (sağlık verileri) işlenmektedir.
                    </Text>
                  </View>
                  <View style={styles.contractTag}>
                    <Text style={styles.contractTagText}>
                      2. KİŞİSEL VERİLERİN İŞLENME AMACI
                    </Text>
                  </View>
                  <View style={{ marginHorizontal: 15 }}>
                    <Text>
                      Anıl ile ilişkiniz kapsamında elde edilen kişisel
                      verileriniz aşağıda yer verilen amaçlarla işlenmektedir;
                    </Text>
                  </View>
                  <View style={styles.contractText}>
                    <Text>
                      Mal/ hizmet üretim ve operasyon süreçlerinin
                      yürütülmesi,Mal/ hizmet satış süreçlerinin
                      yürütülmesi,Organizasyon ve etkinlik yönetimi, sosyal
                      sorumluluk ve sivil toplum aktivitelerinin yürütülmesi,Web
                      sitesi ve sosyal medya platformlarından iletilen talep ve
                      şikâyetlerin takibi, müşteri memnuniyetine yönelik
                      çalışmaların yürütülmesi,Proje
                      sunumu,Reklam/kampanya/promosyon süreçleri ile pazarlama
                      ve analiz süreçlerinin yürütülmesi,Eğitim faaliyetlerinin
                      yürütülmesi,Enerji kimlik belgesi taleplerinin ve
                      süreçlerinin takip ve yönetimiİletişim faaliyetlerinin
                      yürütülmesi,Sözleşme süreçlerinin yürütülmesi,Mal/hizmet
                      satış sonrası destek hizmetlerinin yürütülmesi, finans ve
                      muhasebe işlerinin yürütülmesi,Bilgi güvenliği ve fiziksel
                      mekân güvenliğinin temini,Usta ile nihai tüketicinin bir
                      araya getirilmesi,Usta ve müşteri sadakat programı başvuru
                      kayıtlarının yapılması, usta ve müşteri sadakat
                      programlarının yürütülmesi, sadakat programı puan
                      uygulamalarının yürütülmesiMüşteri/tedarikçi/iş ortağı
                      kayıt ve iptallerinin yapılması,Lojistik faaliyetlerin
                      yürütülmesi,Boya ve yalıtım ustalarının
                      belgelendirilmesiHukuk işlerinin takibi ve
                      yürütülmesi,Şirket raporlamalarının yapılması,İşyeri, iş
                      ve çalışan sağlığı ve güvenliğinin sağlanması ile
                      takibi,Kurumsal kaynak planlamasının yapılması.
                    </Text>
                  </View>
                  <View style={styles.contractTag}>
                    <Text style={styles.contractTagText}>
                      3. İŞLENEN KİŞİSEL VERİLERİN AKTARIMI
                    </Text>
                  </View>
                  <View style={styles.contractText}>
                    <Text>
                      Kişisel verileriniz; KVKK nın kişisel verilerin
                      aktarılması ve yurtdışına aktarılmasına ilişkin hükümleri
                      kapsamında işbu Aydınlatma Metni nin 2. maddesinde yer
                      alan amaçlarla, yurt içindeki resmi kurum ve kuruluşlara,
                      bankalara, yurtiçi ve yurtdışındaki grup şirketlerimiz,
                      iştiraklerimiz, iş ortaklarımız, tedarikçi ve hizmet
                      sağlayıcı firmalar ve yetkililerine aktarılabilmektedir.
                    </Text>
                  </View>
                  <View style={styles.contractTag}>
                    <Text style={styles.contractTagText}>
                      4. KİŞİSEL VERİLERİN TOPLANMA YÖNTEMLERİ VE HUKUKİ
                      SEBEPLERİ
                    </Text>
                  </View>
                  <View style={styles.contractText}>
                    <Text>
                      Kişisel verileriniz, Şirketimizle hukuki ilişkinizin
                      kurulması esnasında ve söz konusu ilişkinin devamı
                      süresince sizlerden, üçüncü kişilerden ve yasal
                      mercilerden olmak kaydıyla internet sitesi, muhtelif
                      sözleşmeler, mobil uygulamalar, elektronik posta, başvuru
                      formları gibi araçlar üzerinden, Şirketimiz ile yapılan
                      yazılı veya sözlü iletişim kanalları aracılığıyla sözlü,
                      yazılı veya elektronik ortamda toplanmaktadır. Bu
                      doğrultuda toplanan kişisel verileriniz KVKK nın 5, 6 ve
                      8. maddelerinde belirtilen ve aşağıda yer verilen hukuka
                      uygunluk sebeplerine dayanılarak işlenmektedir.
                    </Text>
                  </View>
                  <View style={styles.contractText}>
                    <Text>
                      Açık rızanızın bulunması,Vergi Usul Kanunu, Türk Borçlar
                      Kanunu, Türk Ticaret Kanunu olmak üzere Şirketimizin tabi
                      olduğu mevzuatta açıkça öngörülmüş olması,Müşterilerin
                      tanınmasına ilişkin ulusal ve uluslararası alandaki ilke
                      ve esaslara uyum sağlamak, mevzuat ve resmi otoritelerce
                      öngörülen bilgi saklama, raporlama, bilgilendirme
                      yükümlülüklerine uymak,Bir sözleşmenin kurulması veya
                      ifasıyla doğrudan doğruya ilgili olması kaydıyla,
                      sözleşmenin taraflarına ait kişisel verilerin işlenmesinin
                      gerekli olması, talep edilen ürün ve hizmetleri sunabilmek
                      ve akdettiğiniz sözleşmelerinin gereğinin yerine
                      getirilmesi,Hukuki yükümlülüğün yerine getirebilmesi için
                      zorunlu olması,İlgili kişinin kendisi tarafından
                      alenileştirilmiş olması,Bir hakkın tesisi, kullanılması
                      veya korunması için veri işlemenin zorunlu olması, İlgili
                      kişinin temel hak ve özgürlüklerine zarar vermemek
                      kaydıyla, veri sorumlusunun meşru menfaatleri için veri
                      işlenmesinin zorunlu olması.
                    </Text>
                  </View>
                  <View style={styles.contractText}>
                    <Text>
                      Özel nitelikli kişisel verileriniz ise aşağıdaki hukuka
                      uygunluk sebeplerine dayanılarak toplanmakta, saklanmakta
                      ve işlenmektedir:
                    </Text>
                  </View>
                  <View style={styles.contractText}>
                    <Text>
                      Açık rızanızın bulunması, Sağlık dışındaki kişisel
                      veriler, kanunlarda öngörülen hallerde açık rızası
                      aranmaksızın, Sağlığa ilişkin kişisel veriler ise ancak
                      kamu sağlığının korunması, koruyucu hekimlik, tıbbî
                      teşhis, tedavi ve bakım hizmetlerinin yürütülmesi, sağlık
                      hizmetleri ile finansmanının planlanması ve yönetimi
                      amacıyla, sır saklama yükümlülüğü altında bulunan kişiler
                      veya yetkili kurum ve kuruluşlar tarafından ilgilinin açık
                      rızası aranmaksızın.
                    </Text>
                  </View>
                  <View style={styles.contractTag}>
                    <Text style={styles.contractTagText}>
                      5. KİŞİSEL VERİSİ İŞLENEN İLGİLİ KİŞİNİN HAKLARI
                    </Text>
                  </View>
                  <View style={styles.contractText}>
                    <Text>
                      Kanun un 11. maddesi hükümleri uyarınca kişisel
                      verilerinize ilişki olarak aşağıdaki haklarınız
                      bulunmaktadır.
                    </Text>
                  </View>
                  <View style={styles.contractText}>
                    <Text>
                      Kişisel veri işlenip işlenmediğini öğrenme,Kişisel
                      verileri işlenmişse buna ilişkin bilgi talep etme,Kişisel
                      verilerin işlenme amacını ve bunların amacına uygun
                      kullanılıp kullanılmadığını öğrenme,Yurt içinde veya yurt
                      dışında kişisel verilerin aktarıldığı üçüncü kişileri
                      bilme,Kişisel verilerin eksik veya yanlış işlenmiş olması
                      hâlinde bunların düzeltilmesini isteme,Kişisel verilerin
                      silinmesini veya yok edilmesini isteme,Kişisel verilerin
                      düzeltilmesi, silinmesi ya da yok edilmesi halinde bu
                      işlemlerin kişisel verilerin aktarıldığı üçüncü kişilere
                      bildirilmesini isteme,İşlenen verilerin münhasıran
                      otomatik sistemler vasıtasıyla analiz edilmesi suretiyle
                      kişinin kendisi aleyhine bir sonucun ortaya çıkmasına
                      itiraz etme,Kişisel verilerin kanuna aykırı olarak
                      işlenmesi sebebiyle zarara uğraması hâlinde zararın
                      giderilmesini talep etme.
                    </Text>
                  </View>
                  <View style={styles.contractTag}>
                    <Text style={styles.contractTagText}>
                      6. HAK VE TALEPLERİNİZ İÇİN BİZİMLE İLETİŞİME GEÇMEK
                      İSTERSENİZ
                    </Text>
                  </View>
                  <View style={styles.contractText}>
                    <Text>
                      İlgili kanun ve sair mevzuat dahilinde öngörülen yasal
                      haklarınız uyarınca taleplerinizi dilekçe ve kimlik
                      doğrulayıcı belgeler ile birlikte “SEYHAN MAHALLESİ 661/5
                      SOKAK NO.38 BUCA/İZMİR, Türkiye” adresimize bizzat elden
                      iletebilir, noter kanalıyla ulaştırabilir
                      veya info@anilyukselboya.com.tradresine güvenli elektronik
                      imzalı olarak iletebilirsiniz. Bunun yanında, “Veri
                      Sorumlusuna Başvuru Usul ve Esasları Hakkına Tebliğ”in 5.
                      maddesi uyarıca talebinizi kimlik doğrulayıcı belgeler ile
                      birlikte info@anilyukselboya.com.tradresine
                      iletebilirsiniz.
                    </Text>
                  </View>
                  <Text></Text>
                </View>
                <View>
                  <Button
                    text="Kapat"
                    onPress={() => setKvkkModalVisible(!kvkkModalVisible)}
                    marginHorizontal={20}
                  />
                </View>
              </View>
            </ScrollView>
          </Modal>
          <View style={{ flexDirection: "row", marginTop: 8 }}>
            <Checkbox value={memberChecked} onValueChange={setMemberChecked} />
            <TouchableOpacity onPress={() => openMemberText()}>
              <Text style={{ color: "blue", fontSize: 16, marginLeft: 3 }}>
                Üyelik Sözleşmesini
              </Text>
            </TouchableOpacity>
            <Modal
              animationType="slide"
              transparent={true}
              visible={memberModalVisible}
            >
              <ScrollView>
                <View style={{ backgroundColor: "#f0f8ff", flex: 1 }}>
                  <View style={styles.contractTag}>
                    <Text style={{ fontSize: 15 }}>Üyelik Sözleşmesi</Text>
                  </View>
                  <View style={styles.contractTag}>
                    <Text style={styles.contractTagText}>1.Taraflar</Text>
                  </View>
                  <View style={styles.contractText}>
                    <Text>
                      1.1. İşbu üyelik sözleşmesi ('Üyelik Sözleşmesi') merkezi
                      SEYHAN MAHALLESİ 661/5 SOKAK NO.38 BUCA/ZİMİR adresinde
                      bulunan ANIL YÜKSEL BOYA HIRDAVAT İNŞ.MLZ.SAN VE
                      TİC.LTD.ŞTİ. ('ANIL) ile Üye ('Üye') arasında, Üye'nin
                      ANIL'nin Websitesi'nde sunduğu Hizmetler'den
                      yararlanmasına ilişkin koşulların belirlenmesi için
                      akdedilmiştir.
                    </Text>
                    <Text>
                      1.2. ANIL ve Üye işbu Üyelik Sözleşmesi'nde münferiden
                      'Taraf' ve müştereken 'Taraflar' olarak anılacaklardır.
                    </Text>
                  </View>
                  <View style={styles.contractTag}>
                    <Text style={styles.contractTagText}>2. TANIMLAR</Text>
                  </View>
                  <View style={styles.contractText}>
                    <Text>
                      Çerez (Cookie) Politikası Websitesi'nin fonksiyonel
                      işlerliğini sağlamak, Üyeler'in alışveriş deneyimini
                      iyileştirmek ve Üyeler'in Websitesi'ni ziyaretlerine
                      ilişkin bilgiler doğrultusunda tercih ve beğenilerine
                      uygun içerik sunmak amacıyla kullanılan çerezler hakkında
                      bilgilendirme içeren ve Websitesinden erişilebilecek olan
                      metni ifade eder.
                    </Text>
                    <Text>
                      Müşteri Kişisel Verilerinin İşlenmesine İlişkin Aydınlatma
                      Metni Üyeler'in Websitesi üzerinden ilettikleri kişisel
                      verilerin, ANIL tarafından hangi amaçlarla ve ne şekilde
                      kullanılacağı gibi konuları düzenleyen ve Websitesi
                      üzerinden erişilebilecek olan metni ifade eder.
                    </Text>
                    <Text>
                      Hesabım Sayfası Üye'nin Websitesi'nde yer alan çeşitli
                      uygulamalardan ve Hizmetler'den yararlanabilmesi için
                      gerekli işlemleri gerçekleştirebildiği, kişisel verilerini
                      ve uygulama bazında kendisinden talep edilen bilgilerini
                      girdiği sadece ilgili Üye tarafından belirlenen kullanıcı
                      adı ve şifre ile erişilebilen Üye'ye özel sayfayı ifade
                      eder.
                    </Text>
                    <Text>
                      Hizmet Üyeler'in ve Ziyaretçiler'in işbu Üyelik Sözleşmesi
                      içerisinde tanımlı olan iş ve işlemlerini
                      gerçekleştirmelerini sağlamak amacıyla ANIL ya da ANIL'nin
                      belirlediği iş ortağı tarafından sunulan hizmet ve
                      uygulamaları ifade eder.
                    </Text>
                    <Text>
                      Sanal Mağaza ANIL'nin Websitesi üzerinde ANIL usul ve
                      kurallarına uygun olarak Satıcılar'a tahsis etmiş olduğu
                      ve Satıcılar'ın bir ya da birden fazla ürün ve/veya
                      hizmetin satışına yönelik içerik ve görsellerden oluşan
                      ilanlarını yayınlayabilme imkânına sahip oldukları sanal
                      alanı ifade eder.
                    </Text>
                    <Text>
                      Satıcı ANIL ile yaptığı Satıcı İş Ortaklığı ve İlan
                      Sözleşmesi kapsamında Websitesi'ne üye olan ve Websitesi
                      üzerinde oluşturduğu hesap üzerinden yayınladığı ilanlar
                      aracılığıyla çeşitli ürün ve/veya hizmetleri satışa arz
                      eden tüzel/gerçek kişi üyeyi ifade eder.
                    </Text>
                    <Text>
                      Üye ANIL ile yaptığı işbu Üyelik Sözleşmesi kapsamında
                      Websitesi'ne üye olan ve Satıcı tarafından Websitesi
                      üzerinde verilen ilanlarla satışa arz edilen ürün ve/veya
                      hizmetleri satın alan gerçek kişiyi ifade eder.
                    </Text>
                    <Text>
                      Websitesi Mülkiyeti ANIL'ye ait olan ve ANIL'nin işbu
                      Sözleşme ile belirlenen Hizmetler'i üzerinde sunmakta
                      olduğu www.anilyukselboya.com alan adına sahip internet
                      sitesini, mobil uygulamalarını ve mobil siteyi ifade eder.
                    </Text>
                    <Text>
                      Ziyaretçi Websitesi'ni Üye olmadan kullanan ve
                      Hizmetler'den faydalanan gerçek kişiyi ifade eder.
                    </Text>
                  </View>
                  <View style={styles.contractTag}>
                    <Text style={styles.contractTagText}>
                      3. ÜYELİK SÖZLEŞMESİ’NİN KAPSAM VE AMACI
                    </Text>
                  </View>
                  <View style={styles.contractText}>
                    <Text>
                      3.1. ANIL, Websitesi'ni işletmekte olup, 6563 sayılı
                      Elektronik Ticaretin Düzenlenmesi Hakkında Kanun uyarınca
                      aracı hizmet sağlayıcıdır.
                    </Text>
                    <Text>
                      3.2. Üyelik Sözleşmesi uyarınca Üye, Websitesi'ne üye
                      olmak, Hizmetler'den faydalanmak ve bu platformda
                      Satıcılar tarafından Sanal Mağazalar'da satılan ürün
                      ve/veya hizmetleri satın almak istemektedir.
                    </Text>
                    <Text>
                      3.3. Üyelik Sözleşmesi'nin amacını, Hizmetler'den
                      yararlanmasına ilişkin koşulların belirlenmesi ve bu
                      doğrultuda Taraflar'ın hak ve yükümlülüklerinin tespiti
                      oluşturmaktadır. Üyelik Sözleşmesi'nin Üye tarafından
                      kabulü ile Üye, Websitesi'nde yer alan ve yer alacak olan
                      Hizmetler'e, kullanıma, içeriklere, uygulamalara ve
                      Üyeler'e yönelik her türlü beyanı da kabul etmiş olduğunu
                      beyan ve taahhüt eder.
                    </Text>
                    <Text>
                      3.4. Şüpheye mahal vermemek adına, işbu Üyelik Sözleşmesi,
                      yalnızca Taraflar arasında olup, Websitesi'nde yer alan ve
                      yer alacak olan Hizmetler'e yönelik şekil ve şartları
                      kapsamaktadır. Üyeler ile Satıcılar arasındaki ilişki işbu
                      Üyelik Sözleşmesi'nin kapsamına girmemektedir ve ANIL,
                      Üyeler ile Satıcılar arasındaki ilişkiden hiçbir şekilde
                      sorumlu değildir. Üyeler, Sanal Mağaza'dan
                      gerçekleştirecekleri işlemlere ilişkin olarak Satıcılar'a
                      karşı 6502 sayılı Tüketicinin Korunması Hakkında Kanun
                      başta olmak üzere sair mevzuat çerçevesinde haklarını
                      arayabileceklerdir.
                    </Text>
                  </View>
                  <View style={styles.contractTag}>
                    <Text style={styles.contractTagText}>
                      4. TARAFLAR’IN HAK VE YÜKÜMLÜLÜKLERİ
                    </Text>
                  </View>
                  <View style={styles.contractText}>
                    <Text>
                      4.1. Üyelik statüsünün kazanılması için, Üyelik
                      Sözleşmesi'nin onaylanması ve üyelik sayfasında talep
                      edilen bilgilerin doğru ve güncel bilgilerle doldurması
                      gerekmektedir. Üye olmak isteyen kullanıcının 18 (on
                      sekiz) yaşını doldurmuş olması gerekmektedir. Üyelik
                      Sözleşmesi'ni doldururken doğru ve güncel bilgi sağlamayan
                      Üye, bu sebeple doğabilecek tüm zararlardan bizzat
                      sorumludur.
                    </Text>
                    <Text>
                      4.2. Üyelik hak ve yükümlülüklerinin hangi kişiye ait
                      olduğu konusunda ihtilaf olması ve bu hususta söz konusu
                      kişilerin ANIL'den talepte bulunması halinde ANIL, ilgili
                      Üyelik hesabı kullanılarak herhangi bir Hizmet için
                      ANIL'ye ödeme yapan en son kişinin Üyelik hesabının sahibi
                      olduğunu kabul ederek, bu doğrultuda işlem yapma hakkına
                      sahiptir.
                    </Text>
                    <Text>
                      4.3. ANIL'nin Websitesi'nde yer alan herhangi bir ürün
                      veya hizmetin satıcısı konumunda olmaması ve 6563 sayılı
                      Elektronik Ticaretin Düzenlenmesi Hakkında Kanun uyarınca
                      yalnızca 'aracı hizmet sağlayıcı' ve 5651 sayılı İnternet
                      Ortamında Yapılan Yayınların Düzenlenmesi ve Bu Yayınlar
                      Yoluyla İşlenen Suçlarla Mücadele Edilmesi Hakkında Kanun
                      uyarınca 'yer sağlayıcı' olması sebebiyle; Websitesi'nde
                      yer alan ve kendisi tarafından yayınlanmamış içeriğe
                      ilişkin bir sorumluluğu bulunmamakta ve söz konusu
                      içeriğin hukuka uygun olup olmadığını kontrol etme gibi
                      bir yükümlülüğü bulunmamaktadır. ANIL, böyle bir
                      yükümlülüğü olmamasına rağmen ve takdiri tamamen kendisine
                      ait olmak üzere, söz konusu içeriği dileği zaman kontrol
                      etme ve gerekli görürse erişime kapatma ve silme hakkına
                      sahiptir. Websitesi'nde yayınladığı herhangi bir görsel,
                      yazılı ve sair içerikle ANIL'nin ya da üçüncü kişilerin
                      haklarını ihlal eden kişiler, ANIL'ye ve/veya söz konusu
                      üçüncü kişilere karşı sorumludur.
                    </Text>
                    <Text>
                      4.4. Üye, Websitesi üzerinden herhangi bir Satıcı'dan
                      yapacağı alımlarda akdedilecek mesafeli satış
                      sözleşmelerinde Satıcı'nın satıcı taraf, kendisinin ise
                      alıcı taraf olduğunu; ANIL'nin bahsi geçen mesafeli satış
                      sözleşmesi ilişkisinde taraf olmadığını; dolayısıyla da
                      kendisine karşı sadece Satıcı'nın yürürlükteki tüketici
                      hukuku mevzuatı ve sair mevzuat kapsamında her anlamda
                      bizzat sorumlu olduğunu kabul ve beyan eder. Bu bağlamda
                      Üye, Sanal Mağaza'da sergilenen ve satılan tüm ürünlerin
                      kalitesinden, mevzuata uygunluğundan, garanti belgesi
                      verilmesinden, faturalandırılmasından ve sair diğer
                      gerekli belgenin tesliminden ve satış sonrası gereksinim
                      duyulan servis vs. hizmetlerinden ve ürünlerin süresinde
                      teslim edilmesinden yalnızca Satıcı'nın sorumlu olduğunu
                      kabul ve beyan eder.
                    </Text>
                    <Text>
                      4.5. Üye, Websitesi üzerinde gerçekleştirdiği işlemlerde
                      ve yazışmalarda, işbu Üyelik Sözleşmesi'nin hükümlerine,
                      Websitesi'nde belirtilen tüm koşullara, yürürlükteki
                      mevzuata ve ahlak kurallarına uygun olarak hareket
                      edeceğini kabul ve beyan eder. Üye'nin Websitesi dâhilinde
                      yaptığı işlem ve eylemlere ilişkin hukuki ve cezai
                      sorumluluk kendisine aittir.
                    </Text>
                    <Text>
                      4.6. ANIL, yürürlükteki mevzuat uyarınca yetkili
                      makamların talebi halinde, Üye'nin kendisinde bulunan
                      bilgilerini 6698 sayılı Kişisel Verilerin Korunması Kanunu
                      uyarınca gerekli olduğu takdirde Üye'yi önceden
                      bilgilendirmek suretiyle ve her durumda veri aktarım
                      kurallarına tabi olarak söz konusu makamlarla
                      paylaşabilecektir.
                    </Text>
                    <Text>
                      4.7. Websitesi'ne üyelik sırasında ve/veya alışveriş
                      sırasında Üyeler'den alınan kişisel veriler, Üye ve/veya
                      Satıcılar arasında sahtecilik, dolandırıcılık,
                      Websitesi'nin kötüye kullanımı, 6100 sayılı Türk Ticaret
                      Kanunu anlamında suç oluşturabilecek konularda çıkan
                      uyuşmazlıklarda, yalnızca talep edilen konu ile sınırlı
                      olmak üzere tarafların yasal haklarını kullanabilmeleri
                      amacıyla, 6698 sayılı Kişisel Verilerin Korunması Kanunu
                      uyarınca gerekli olduğu takdirde ilgili kişi önceden
                      bilgilendirilmek suretiyle ve her durumda veri aktarım
                      kurallarına tabi olarak uyuşmazlığa taraf olabilecek diğer
                      Üye ve/veya Satıcılar'a iletebilecektir.
                    </Text>
                    <Text>
                      4.8. Üye'nin Hesabım Sayfası'na erişmek ve Websitesi
                      üzerinden işlem gerçekleştirebilmek için ihtiyaç duyduğu
                      kullanıcı adı ve şifre bilgisi, Üye tarafından
                      oluşturulmakta olup, söz konusu bilgilerin güvenliği ve
                      gizliliği tamamen Üye'nin sorumluluğundadır. Üye,
                      kendisine ait kullanıcı adı ve şifre ile gerçekleştirilen
                      işlemlerin kendisi tarafından gerçekleştirilmiş olduğunu,
                      bu işlemlerden kaynaklanan sorumluluğunun peşinen
                      kendisine ait olduğunu, bu şekilde gerçekleştirilen iş ve
                      işlemleri kendisinin gerçekleştirmediği yolunda herhangi
                      bir def'i ve/veya itiraz ileri süremeyeceğini ve/veya bu
                      def'i veya itiraza dayanarak yükümlülüklerini yerine
                      getirmekten kaçınmayacağını kabul, beyan ve taahhüt eder.
                    </Text>
                    <Text>
                      4.9. Üye, Websitesi'ni aşağıda sayılan haller başta olmak
                      üzere hukuka ve ahlaka aykırı bir şekilde
                      kullanmayacaktır:
                    </Text>
                    <Text>
                      4.9.1. Websitesi'nin herhangi bir kişi adına veri tabanı,
                      kayıt veya rehber yaratmak, amacıyla kullanılması;
                    </Text>
                    <Text>
                      4.9.2. Websitesi'nin bütününün veya bir bölümünün bozma,
                      değiştirme veya ters mühendislik yapma amacıyla
                      kullanılması;
                    </Text>
                    <Text>
                      4.9.3. Yanlış bilgiler veya başka bir kişinin bilgileri
                      kullanılarak işlem yapılması, yanlış veya yanıltıcı
                      ikametgâh adresi, elektronik posta adresi, iletişim, ödeme
                      veya hesap bilgileri de dahil yanlış veya yanıltıcı
                      kişisel veriler kullanmak suretiyle gerçek olmayan Üyelik
                      hesapları oluşturulması ve bu hesapların Üyelik
                      Sözleşmesi'ne veya yürürlükte mevzuata aykırı şekilde
                      kullanılması, başka bir Üye'nin hesabının izinsiz
                      kullanılması, başka birinin yerine geçilerek ya da yanlış
                      bir isimle işlemlere taraf ya da katılımcı olunması;
                    </Text>
                    <Text>
                      4.9.4. Yorum ve puanlama sistemlerinin; Websitesi'ndeki
                      yorumları Websitesi dışında yayınlamak gibi Websitesi dışı
                      amaçlar için veya sistemleri manipüle edecek şekilde
                      kullanılma amaçları dışında kullanılması;
                    </Text>
                    <Text>
                      4.9.5. Virüs veya Websitesi'ne, Websitesi'nin veri
                      tabanına, Websitesi üzerinde yer alan herhangi bir içeriğe
                      zarar verici herhangi başka bir teknoloji yayılması;
                    </Text>
                    <Text>
                      4.9.6. Üyeler veya Satıcılar hakkında elektronik posta
                      adresleri de dâhil herhangi bir bilginin ilgili kişilerin
                      izni olmaksızın toplanması veya 6698 sayılı Kişisel
                      Verilerin Korunması Hakkında Kanun uyarınca ihlal teşkil
                      edecek diğer uygulamalarda bulunulması;
                    </Text>
                    <Text>
                      4.9.7. Websitesi tarafından belirlenmiş olan iletişimler
                      ve teknik sistemler üzerinde makul olmayan veya orantısız
                      derecede büyük yüklemeler yaratacak ya da teknik işleyişe
                      zarar verecek faaliyetlerde bulunulması, ANIL'nin önceden
                      yazılı iznini alınmaksızın Websitesi üzerinde otomatik
                      program, robot, web crawler, örümcek, veri madenciliği
                      (data mining) ve veri taraması (data crawling) gibi
                      'screen scraping' yazılımları veya sistemleri kullanılması
                      ve bu şekilde Websitesi'nde yer alan herhangi bir içeriğin
                      tamamının veya bir kısmının izinsiz kopyalanarak,
                      yayınlanması veya kullanılması;
                    </Text>
                    <Text>
                      4.9.8. Hizmetler'in, Websitesi'nde sunulan kampanya ve
                      avantajların kötü niyetle ve haksız fayda sağlanması
                      amacına yönelik kullanılması, kampanya koşullarının kötü
                      niyetle ihlal edilmesi, Websitesi'ni suistimal edecek
                      yüksek adetli toplu alımlar yapılması.
                    </Text>
                    <Text>
                      4.10. Üye, Websitesi'nde yaptığı işlemleri Websitesi'ne
                      teknik olarak hiçbir surette zarar vermeyecek şekilde
                      yürütmekle yükümlüdür. Üye, Websitesi'ne sağlayacağı tüm
                      bilgi, içerik, materyal ve sair içeriğin sisteme zarar
                      verecek her türlü program, virüs, yazılım, lisansız ürün,
                      truva atı vb. içermemesi için gerekli koruyucu yazılımları
                      ve lisanslı ürünleri kullanmak da dâhil olmak üzere
                      gerekli her türlü tedbiri aldığını kabul ve taahhüt eder.
                      Üye ayrıca Hesap Sayfası'na robot veya otomatik giriş
                      yöntemleriyle girmeyeceğini kabul eder.
                    </Text>
                  </View>
                  <View style={styles.contractTag}>
                    <Text style={styles.contractTagText}>
                      5. SÖZLEŞME’NİN FESHİ
                    </Text>
                  </View>
                  <View style={styles.contractText}>
                    <Text>
                      5.1. Taraflar'dan herhangi biri, işbu Üyelik Sözleşmesi'ni
                      tek taraflı olarak ve tazminat ödemeksizin her zaman
                      feshedilebilir. Böyle bir fesih halinde Taraflar fesih
                      tarihine kadar doğmuş olan hak ve borçları karşılıklı
                      olarak tamamen ifa edeceklerdir.
                    </Text>
                    <Text>
                      5.2. ANIL, Üye'nin işbu Üyelik Sözleşmesi'nin herhangi bir
                      maddesini ihlal ettiğini tespit etmesi veya buna ilişkin
                      makul bir şüphe duyması halinde üyeliği askıya alma,
                      sonlandırma, dava ve takip haklarına sahiptir.
                    </Text>
                  </View>
                  <View style={styles.contractTag}>
                    <Text style={styles.contractTagText}>
                      6. GİZLİLİK ve KİŞİSEL VERİLERİN KORUNMASI
                    </Text>
                  </View>
                  <View style={styles.contractText}>
                    <Text>
                      6.1. ANIL, Üye'nin Websitesi'nde sunulan Hizmetler'den
                      yararlanabilmek için Websitesi üzerinden kendisine
                      sağladığı kişisel verilerin 6698 sayılı Kişisel Verilerin
                      Korunması Kanunu da dahil her türlü mevzuata uygun bir
                      şekilde işlenmesine, güvenliğinin sağlanmasına ve
                      korunmasına önem vermektedir. ANIL, bu kapsamda Üye'nin
                      sağladığı kişisel verileri Websitesi'nde yer alanMüşteri
                      Kişisel Verilerinin İşlenmesine İlişkin Aydınlatma
                      Metni'neve Çerez (Cookie) Politikası'na uygun olarak
                      toplamakta, kullanmakta, aktarmakta ve diğer şekillerde
                      işleyebilmektedir. Üye, kişisel verilerinin kullanılması
                      ile ilgili koşullar ve bu konudaki haklarıyla ilgili
                      olarak daha fazla bilgi edinmek için Websitesi'nde yer
                      alan Müşteri Kişisel Verilerinin İşlenmesine İlişkin
                      Aydınlatma Metni'ni her zaman inceleyebileceğini ve burada
                      belirtilen şekilde info@anilyukselboya.com.tr adresine
                      elektronik posta göndererek ya da Websitesi'nde yer alan
                      Başvuru Formu'nda belirtilen diğer yöntemlerle
                      kullanabileceğini anlamaktadır.
                    </Text>
                    <Text>
                      6.2. Üye tarafından Websitesi'nde Üyelik oluşturmak veya
                      Websitesi'nden faydalanmak amacıyla paylaşılan kişisel
                      veriler; Üyelik Sözleşmesi ile belirlenen yükümlülüklerin
                      ifası, Websitesi'nin işletilmesi için gereken
                      uygulamaların yürütülmesi, Üye'ye çeşitli avantajların
                      sağlanıp sunulabilmesi, ödeme işlemlerinin
                      gerçekleştirilmesi, sipariş teslimatlarının yapılması,
                      müşteri hizmetleri ve şikayet takibi işlemlerinin
                      gerçekleştirilmesi ve Üye'ye özel reklam, satış,
                      pazarlama, anket, benzer amaçlı her türlü elektronik
                      iletişim, profilleme, istatistiksel çalışmalar yapılması
                      amacıyla ANIL ya da iş ortakları tarafından Müşteri
                      Kişisel Verilerinin İşlenmesine İlişkin Aydınlatma
                      Metni'ne ve Çerez (Cookie) Politikası'na uygun olarak
                      toplanmakta, saklanmakta ve işlenmektedir. Ayrıca, bu
                      kişisel veriler Üye'ye özel avantajların sunulabilmesi,
                      satış, pazarlama ve benzeri faaliyetlerin yapılabilmesine
                      yönelik olarak 6698 sayılı Kişisel Verilerin Korunması
                      Kanunu'ndaki ve ilgili diğer mevzuattan doğan
                      yükümlülükleri yerine getirilmesi şartıyla Üye ile
                      iletişime geçilmesi amacıyla ANIL'nin iştirakleriyle
                      paylaşılacaktır.
                    </Text>
                  </View>
                  <View style={styles.contractTag}>
                    <Text style={styles.contractTagText}>
                      7. FİKRİ MÜLKİYET HAKLARI
                    </Text>
                  </View>
                  <View style={styles.contractText}>
                    <Text>
                      Anilyukselboya markası ve logosu, Anıl mobil uygulamasının
                      ve Websitesi’nin tasarımı, yazılımı, alan adı ve bunlara
                      ilişkin olarak ANIL tarafından oluşturulan her türlü
                      marka, tasarım, logo, ticari takdim şekli, slogan ve diğer
                      tüm içeriğin her türlü fikri mülkiyet hakkı ile ANIL’nin
                      mülkiyetindedir. Üye, ANIL’nin veya bağlı şirketlerinin
                      mülkiyetine tabi fikri mülkiyet haklarını ANIL’nin izni
                      olmaksızın kullanamaz, paylaşamaz, dağıtamaz,
                      sergileyemez, çoğaltamaz veya bunlardan türemiş çalışmalar
                      yapamaz. Üye, anilyukselboya mobil uygulamasının veya
                      Websitesi’nin bütünü ya da bir kısmını başka bir ortamda
                      ANIL’nin izni olmaksızın kullanamaz. Üye’nin, üçüncü
                      kişilerin veya ANIL’nin fikri mülkiyet haklarını ihlal
                      edecek şekilde davranması halinde, Üye, ANIL’nin ve/veya
                      söz konusu üçüncü kişinin tüm doğrudan ve dolaylı
                      zararları ile masraflarını tazmin etmekle yükümlüdür.
                    </Text>
                  </View>
                  <View style={styles.contractTag}>
                    <Text style={styles.contractTagText}>
                      8. SÖZLEŞME DEĞİŞİKLİKLERİ
                    </Text>
                  </View>
                  <View style={styles.contractText}>
                    <Text>
                      ANIL, tamamen kendi takdirine bağlı olmak üzere, işbu
                      Üyelik Sözleşmesi’ni ve Websitesi’nde yer alan Müşteri
                      Kişisel Verilerinin İşlenmesine İlişkin Aydınlatma Metni
                      ve Çerez (Cookie) Politikası da dahil her türlü
                      politikayı, hüküm ve şartı uygun göreceği herhangi bir
                      zamanda, yürürlükteki mevzuat hükümlerine aykırı olmamak
                      kaydıyla Websitesi’nde ilan ederek tek taraflı olarak
                      değiştirebilir. İşbu Üyelik Sözleşmesi’nin değişen
                      hükümleri, Websitesi’nde ilan edildikleri tarihte
                      geçerlilik kazanacak, geri kalan hükümler aynen yürürlükte
                      kalarak hüküm ve sonuçlarını doğurmaya devam edecektir.
                      Şüpheye mahal vermemek adına, işbu Üyelik Sözleşmesi Üye
                      tarafından tek taraflı olarak değiştirilemez.
                    </Text>
                  </View>
                  <View style={styles.contractTag}>
                    <Text style={styles.contractTagText}>9. MÜCBİR SEBEP</Text>
                  </View>
                  <View style={styles.contractText}>
                    <Text>
                      Ayaklanma, ambargo, devlet müdahalesi, isyan, işgal,
                      savaş, seferberlik, grev, lokavt, iş eylemleri veya
                      boykotlar dahil olmak üzere işçi-işveren anlaşmazlıkları,
                      siber saldırı, iletişim sorunları, altyapı ve internet
                      arızaları, sisteme ilişkin iyileştirme veya yenileştirme
                      çalışmaları ve bu sebeple meydana gelebilecek arızalar,
                      elektrik kesintisi, yangın, patlama, fırtına, sel, deprem,
                      göç, salgın veya diğer bir doğal felaket veya ANIL’nin
                      kontrolü dışında gerçekleşen, kusurundan kaynaklanmayan ve
                      makul olarak öngörülemeyecek diğer olaylar ("Mücbir
                      Sebep") ANIL’nin işbu Üyelik Sözleşmesi’nden doğan
                      yükümlülüklerini ifa etmesini engeller veya geciktirirse,
                      ANIL ifası Mücbir Sebep sonucunda engellenen veya geciken
                      yükümlülüklerinden dolayı sorumlu tutulamaz ve bu durum
                      işbu Üyelik Sözleşmesi’nin bir ihlali olarak kabul
                      edilemez.
                    </Text>
                  </View>
                  <View style={styles.contractTag}>
                    <Text style={styles.contractTagText}>
                      10. MUHTELİF HÜKÜMLER
                    </Text>
                  </View>
                  <View style={styles.contractText}>
                    <Text>
                      10.1. Delil sözleşmesi. Üye, işbu Üyelik Sözleşmesi'nden
                      doğabilecek ihtilaflarda ANIL'nin resmi defter ve ticari
                      kayıtları ile ANIL'nin veri tabanında, sunucularında
                      tutulan e-arşiv kayıtlarının, elektronik bilgilerin,
                      elektronik yazışmaların ve bilgisayar kayıtlarının,
                      bağlayıcı, kesin ve münhasır delil teşkil edeceğini ve bu
                      maddenin 6100 sayılı Hukuk Muhakemeleri Kanunu'nun 193.
                      maddesi anlamında delil sözleşmesi niteliğinde olduğunu
                      kabul eder.
                    </Text>
                    <Text>
                      10.2. Uygulanacak Hukuk ve Uyuşmazlıkların Çözümü. İşbu
                      Üyelik Sözleşmesi münhasıran Türkiye Cumhuriyeti
                      kanunlarına tabi olacaktır. İşbu Üyelik Sözleşmesi'nden
                      kaynaklanan veya işbu Üyelik Sözleşmesi ile bağlantılı
                      olan her türlü ihtilaf, İstanbul Merkez (Çağlayan)
                      Mahkemeleri ve İcra Müdürlükleri'nin münhasır yargı
                      yetkisinde olacaktır.
                    </Text>
                    <Text>
                      10.3. Bildirim ANIL, Üye ile Üye'nin kayıt olurken
                      bildirmiş olduğu elektronik posta adresi vasıtasıyla veya
                      telefon numarasına arama yapmak ve SMS göndermek suretiyle
                      iletişim kuracaktır. Üye, elektronik posta adresini ve
                      telefon numarasını güncel tutmakla yükümlüdür.
                    </Text>
                    <Text>
                      10.4. Üyelik Sözleşmesi'nin Bütünlüğü ve
                      Bölünebilirliği. İşbu Üyelik Sözleşmesi, konuya ilişkin
                      olarak Taraflar arasındaki anlaşmanın tamamını
                      oluşturmaktadır. İşbu Üyelik Sözleşmesi'nin herhangi bir
                      hükmünün yetkili herhangi bir mahkeme, tahkim heyeti veya
                      idari makam tarafından tümüyle veya kısmen geçersiz veya
                      uygulanamaz olduğu veya makul olmadığına karar verilmesi
                      halinde, söz konusu geçersizlik, uygulanamazlık veya makul
                      olmama ölçüsünde işbu Üyelik Sözleşmesi bölünebilir olarak
                      kabul edilecek ve diğer hükümler tümüyle yürürlükte
                      kalmaya devam edecektir.
                    </Text>
                    <Text>
                      10.5. Üyelik Sözleşmesi'nin Devri. Üye, ANIL'nin önceden
                      yazılı onayını almaksızın işbu Üyelik Sözleşmesi'ndeki
                      haklarını veya yükümlülüklerini tümüyle veya kısmen temlik
                      edemeyecektir.
                    </Text>
                    <Text>
                      10.6. Tadil ve Feragat. Taraflar'dan birinin Üyelik
                      Sözleşmesi'nde kendisine verilen herhangi bir hakkı
                      kullanmaması ya da icra etmemesi, söz konusu haktan
                      feragat ettiği anlamına gelmeyecek veya söz konusu hakkın
                      daha sonra kullanılmasını ya da icra edilmesini
                      engellemeyecektir.
                    </Text>
                  </View>
                  <View style = {styles.contractText}>
                    <Text>10 (on) maddeden ibaret bu Üyelik Sözleşmesi, Üye tarafından her bir hükmü okunarak ve bütünüyle anlaşılarak elektronik ortamda onaylanmak suretiyle yürürlüğe girmiştir.</Text>
                  </View>
                  <View>
                    <Button
                      text="Kapat"
                      onPress={() => setMemberModalVisible(!memberModalVisible)}
                      marginHorizontal={20}
                    />
                  </View>
                </View>
              </ScrollView>
            </Modal>
            <Text style={{ fontSize: 16, marginLeft: 3 }}>Onaylıyorum.</Text>
          </View>
          <Button
            text={"Üye Ol"}
            onPress={signUp}
            backgroundColor="#343434"
            color="#FFF"
          />
        </View>
      </View>
    </ScrollView>
  );
}

export default SingUp;

const styles = StyleSheet.create({
  singUpContainer: {
    padding: 30,
    marginTop: -50,
  },
  signUpTagContainer: {
    alignItems: "center",
  },
  signUpTagText: {
    fontWeight: "bold",
    fontSize: 25,
  },
  logoContainer: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center",
  },
  inputInformations: {
    flexDirection: "column",
  },
  inputInformation: {
    borderWidth: 0,
    borderBottomWidth: 1,
    borderColor: "#777E90",
    padding: 10,
    marginTop: 10,
  },
  animatonBorderColor: {
    backgroundColor: "black",
    alignSelf: "flex-start",
    borderRadius: 50,
    borderColor: "black",
    height: 1,
  },
  contractTag: {
    marginVertical: 35,
    marginLeft: 15,
  },
  contractTagText: {
    fontWeight: "600",
  },
  contractText: {
    marginHorizontal: 15,
  },
});
