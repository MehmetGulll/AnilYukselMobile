import React, { useState } from "react";
import { View, StyleSheet, ScrollView, Text, Alert } from "react-native";
import { useNavigation } from "@react-navigation/native";
import Input from "../../components/Input";
import Button from "../../components/Button";
import BackIcon from "../../components/BackIcon";
import axios from "axios";

const apihost = "https://b2b.anilyukselboya.com.tr/json/api";

function DealerShip() {
  const navigation = useNavigation();
  const [name, setName] = useState("");
  const [applyData, setApplyData] = useState({
    adsoyad: "",
    telefon: "",
    email: "",
    firmaAdi: "",
    yetkili: "",
    sehir: "",
    ilce: "",
    adres: "",
    firmaTelefon: "",
    firmaEmail: "",
    firmaUnvan: "",
    vergiDairesi: "",
    vergiNumarasi: "",
  });
  const applyForm = async () => {
    try {
      const {
        adsoyad,
        telefon,
        email,
        firmaAdi,
        yetkili,
        sehir,
        ilce,
        adres,
        firmaTelefon,
        firmaEmail,
        firmaUnvan,
        vergiDairesi,
        vergiNumarasi,
      } = applyData;
      const response = await axios.get(
        `${apihost}/apply/?adsoyad=${adsoyad}&telefon=${telefon}&email=${email}&firmaAdi=${firmaAdi}&yetkili=${yetkili}&sehir=${sehir}&ilce=${ilce}&adres=${adres}&firmaTelefon=${firmaTelefon}&firmaEmail=${firmaEmail}&firmaUnvan=${firmaUnvan}&vergiDairesi=${vergiDairesi}&vergiNumarasi=${vergiNumarasi}`
      );
      console.log(response.data);
      if (response.data.response_status) {
        Alert.alert("Başarılı", response.data.response_message, [
          {
            text: "Tamam",
            onPress: () => navigation.navigate("SecondScreen"),
          },
        ]);
      } else {
        Alert.alert("Hata", response.data.response_message, [
          {
            text: "Tamam",
            onPress: () => navigation.navigate("SecondScreen"),
          },
        ]);
      }
    } catch (error) {
      console.log("Hata:", error);
    }
  };
  return (
    <ScrollView style={styles.dealershipContainer}>
      <BackIcon />
      <View style={styles.dealershipTag}>
        <Text style={styles.dealershipTagText}>Başvuru Bilgileri</Text>
      </View>
      <View style={styles.informationsInput}>
        <Input
          placeholder={"Adınız Soyadınız"}
          onChangeText={(text) => setName(text)}
        />
        <Input placeholder={"Telefon Numaranız"} />
        <Input placeholder={"E-posta Adresiniz"} />
        <Input placeholder={"Firma Adı"} />
        <Input placeholder={"Firma Yetkilisi"} />
        <Input placeholder={"Şehir"} />
        <Input placeholder={"İlçe"} />
        <Input placeholder={"Adres"} />
        <Input placeholder={"Firma Telefonu"} />
        <Input placeholder={"Firma E-posta"} />
        <Input placeholder={"Firma Unvanı"} />
        <Input placeholder={"Vergi Dairesi"} />
        <Input placeholder={"Vergi Numarası"} />
        <Button
          text={"Başvur"}
          onPress={applyForm}
          backgroundColor="#343434"
          color="#FFF"
        />
      </View>
    </ScrollView>
  );
}

export default DealerShip;

const styles = StyleSheet.create({
  dealershipContainer: {
    backgroundColor: "#FFF",
  },
  dealershipTag: {
    alignItems: "center",
    justifyContent: "center",
  },
  dealershipTagText: {
    color: "#545454",
    fontSize: 25,
    fontWeight: "bold",
  },
  informationsInput: {
    justifyContent: "center",
    marginHorizontal: 30,
  },
});
