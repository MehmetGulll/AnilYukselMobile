import React, { useEffect } from "react";
import { useNavigation,CommonActions } from "@react-navigation/native";
import { StyleSheet, View, ActivityIndicator, Text } from "react-native";
import Logo from "../../components/Logo";
import { globalStyles } from "../../theme/GlobalStyles";
function Start() {
  const navigation = useNavigation();
  useEffect(() => {
    const timer = setTimeout(() => {
      navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [
            { name: 'Transactional' },
          ],
        })
      );
    }, 3500);
    return () => clearTimeout(timer);
  }, []);
  return (
    <View style={styles.startContainer}>
      <View style={styles.startLogoContainer}>
        <Logo />
      </View>
      <View style={styles.indicatorContainer}>
        <ActivityIndicator size="large" color={globalStyles.colors.logoColor} />
        <Text style={styles.startTag}>
          <Text style={{ color: "grey" }}>İşinize Renk</Text>
          <Text style={{ color: "#b81414" }}> Markanıza Değer Katın</Text>
        </Text>
      </View>
    </View>
  );
}
export default Start;

const styles = StyleSheet.create({
  startContainer: {
    flex: 1,
    backgroundColor:'#fff'
  },
  startLogoContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-end",
    marginBottom: 60,
  },
  startTag: {
    color: "white",
    fontSize: 10,
    marginTop: 50,
  },
  indicatorContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    transform: [{ scale: 2 }],
  },
});
