import React, { useContext, useEffect, useRef, useState } from "react";
import { View, StyleSheet, Text, Animated, Alert } from "react-native";
import { useNavigation } from "@react-navigation/native";
import Button from "../../components/Button";
import Logo from "../../components/Logo";
import Input from "../../components/Input";
// import BackIcon from "../../components/BackIcon";
import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { GlobalContext } from "../../Context/GlobalStates";

const apihost = "https://b2b.anilyukselboya.com.tr/json/api";

function SingIn() {
  const navigation = useNavigation();
  const {currentType, setCurrentType} = useContext(GlobalContext);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const [dealerFocus, setDealerFocus] = useState(false);
  const [passwordFocus, setPasswordFocus] = useState(false);

  useEffect(() => {
    AsyncStorage.getItem("authUser").then((authUser) => {
      if (authUser !== null) {
        const userData = JSON.parse(authUser)
        setCurrentType(userData.current_type);
        navigation.navigate("SeventhScreen");
      }
    });
  },[]);

  const login = async () => {
    setLoading(true);
    try {
      const response = await axios.get(
        `${apihost}/login/?username=${username}&password=${password}`
      );
      if (response.data.response_status) {
        console.log(response);
        await AsyncStorage.removeItem("authUser");
        await AsyncStorage.setItem(
          "authUser",
          JSON.stringify(response.data.response_data)
        );
        console.log(response.data.response_data);
        setCurrentType(response.data.response_data.current_type)
        console.log(response.data.response_data.current_type)
        console.log("Yeni token var.");
        navigation.navigate('SeventhScreen');
        if (response.data.response_data.not_user) {
          setLoading(false);
          let responseSellers = [
            {
              id: 2402,
              PriceType_id: 1158,
              PriceTypeCode: "0",
            },
          ];
          await AsyncStorage.setItem(
            "sellers",
            JSON.stringify(responseSellers)
          );
          await AsyncStorage.setItem(
            "seller",
            JSON.stringify(responseSellers[0])
          );
          navigation.navigate("AnaSayfa");
        } else {
          const sellerResponse = await axios.get(
            `${apihost}/sellers/?token=${response.data.response_data.token}&start=0&end=100`
          );
          setLoading(false);
          if (sellerResponse.data.response_status) {
            try {
              AsyncStorage.setItem(
                "sellers",
                JSON.stringify(sellerResponse.data.response_data)
              );
            } catch (error) {
              console.log("Errorrrr", error);
            }
            if (sellerResponse.data.response_data.length > 0) {
              AsyncStorage.setItem(
                "seller",
                JSON.stringify(sellerResponse.data.response_data[0])
              );
            } else {
              Alert.alert("Uyarı", "Cari Bulunamadı.");
            }
          } else {
            Alert.alert("Oops..");
          }
        }
      }
    } catch (error) {
      console.log("Error", error);
    }
  };

  const animatedIsFocusDealer = useRef(new Animated.Value(0)).current;
  const animatedIsFocusPassword = useRef(new Animated.Value(0)).current;
  const handleFocusDealer = () => {
    setDealerFocus(true),
      Animated.timing(animatedIsFocusDealer, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: false,
      }).start();
  };
  const handleFocusPassword = () => {
    setPasswordFocus(true),
      Animated.timing(animatedIsFocusPassword, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: false,
      }).start();
  };
  const handleBlurDealer = () => {
    setDealerFocus(false),
      Animated.timing(animatedIsFocusDealer, {
        toValue: 0,
        duration: 1000,
        useNativeDriver: false,
      }).start();
  };
  const handleBlurPassword = () => {
    setPasswordFocus(false),
      Animated.timing(animatedIsFocusPassword, {
        toValue: 0,
        duration: 1000,
        useNativeDriver: false,
      }).start();
  };
  return (
    <View style={{ flex: 1, backgroundColor: "#fff" }}>
      {/* <BackIcon /> */}
      <View style={styles.logoContainer}>
        <Logo />
      </View>
      <View style={styles.signInTagContainer}>
        <Text style={styles.signInTagText}>Giriş Yap</Text>
      </View>
      <View style={styles.singInContainer}>
        <View style={styles.inputInformations}>
          <Input
            placeholder={"Bayi kodu"}
            onFocus={handleFocusDealer}
            onBlur={handleBlurDealer}
            onChangeText={(text) => setUsername(text)}
          />
          <Animated.View
            style={[
              styles.animatonBorderColor,
              {
                width: animatedIsFocusDealer.interpolate({
                  inputRange: [0, 1],
                  outputRange: ["0%", "100%"],
                }),
              },
            ]}
          />
          <Input
            placeholder={"Sifre"}
            secureTextEntry={true}
            onFocus={handleFocusPassword}
            onBlur={handleBlurPassword}
            onChangeText={(text) => setPassword(text)}
          />
          <Animated.View
            style={[
              styles.animatonBorderColor,
              {
                width: animatedIsFocusPassword.interpolate({
                  inputRange: [0, 1],
                  outputRange: ["0%", "100%"],
                }),
              },
            ]}
          />
        </View>
        <Button
          text={"Giriş Yap"}
          disabled={username.length === 0}
          onPress={login}
          backgroundColor="#343434"
          color="#FFF"
        />
        <Button text={"Kayıt Ol"} onPress={()=> navigation.navigate("SignUp")} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  singInContainer: {
    padding: 30,
    flex: 1,
    marginTop: 20,
  },
  signInTagContainer: {
    alignItems: "center",
  },
  signInTagText: {
    fontWeight: "bold",
    fontSize: 25,
  },
  logoContainer: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center",
  },
  inputInformations: {
    flexDirection: "column",
    marginBottom: 30,
  },
  inputInformation: {
    borderWidth: 0,
    borderBottomWidth: 1,
    borderColor: "#e9e9e9",
    padding: 10,
    marginTop: 10,
  },
  animatonBorderColor: {
    backgroundColor: "black",
    alignSelf: "flex-start",
    borderRadius: 50,
    borderColor: "black",
    height: 1,
  },
});

export default SingIn;
