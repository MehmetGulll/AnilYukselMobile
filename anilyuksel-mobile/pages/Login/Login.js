import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native";
import Button from "../../components/Button";
import Logo from "../../components/Logo";
function Login() {
  const navigation = useNavigation();
  return (
    <View style={styles.loginContainer}>
      <View style={{ flex: 1 }}>
        <Logo />
      </View>

      <View style={styles.signContainer}>
        <TouchableOpacity onPress={() => navigation.navigate("ThirthScreen")}>
          <View style={styles.signInButton}>
            <Text style={styles.signInText}>Giriş Yap</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate("FourthScreen")}>
          <View style={styles.signInButton}>
            <Text style={styles.signInText}>Üye Ol</Text>
          </View>
        </TouchableOpacity>
      </View>
      <View style={styles.buttonContainer}>
        <Button
          text={"BAYILIK BAŞVURUSU"}
          onPress={() => navigation.navigate("FifthScreen")}
          backgroundColor="#343434"
          color="#FFF"
        />
        <Button
          text={"ILETIŞIM"}
          onPress={() => navigation.navigate("SixthScreen")}
          backgroundColor="#343434"
          color="#FFF"
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  loginContainer: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center",
    backgroundColor: "#fff",
  },
  signContainer: {
    paddingVertical: 50,
  },
  signInButton: {
    borderWidth: 1,
    borderColor: "#e9e9e9",
    padding: 20,
    borderRadius: 10,
    marginTop: 18,
    alignItems: "center",
  },
  signInText: {
    color: "black",
    fontWeight: "bold",
    fontSize: 25,
    paddingHorizontal: 100,
  },
  stmaxLogo: {
    width: 270,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
  },
  buttonContainer: {
    marginBottom: 15,
  },
});

export default Login;
