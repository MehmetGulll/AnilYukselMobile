import React, { useContext } from "react";
import { View, StyleSheet, Text, TouchableOpacity,Image } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { GlobalContext } from "../../Context/GlobalStates";
import Logo from "../../components/Logo";
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItem,
} from "@react-navigation/drawer";
import B2CRouter from "../B2CRouter";
import Icon from "react-native-vector-icons/AntDesign";

function CustomDrawerContent(props) {


  return (
    <View style={{ flex: 1 }}>
      <DrawerContentScrollView {...props} style={{ backgroundColor: "#fff" }}>
        <View style={styles.avatarContainer}>
          <Image
            source={{
              uri: "https://img.freepik.com/free-vector/businessman-character-avatar-isolated_24877-60111.jpg?w=2000",
            }}
            style={styles.personAvatar}
          />
        </View>
        <DrawerItem
          icon={() => <Icon name="home" size={24} color={"#777E90"} />}
          label={() => <Text style={styles.labelStyle}>Ana Sayfa</Text>}
          onPress={() => props.navigation.navigate("B2CHome")}
        />
        <DrawerItem
          icon={() => <Icon name="shoppingcart" size={24} color={"#777E90"} />}
          label={() => <Text style={styles.labelStyle}>Sepetim</Text>}
          onPress={() => props.navigation.navigate("B2CBasket")}
        />
        <DrawerItem
          icon={() => <Icon name="phone" size={24} color={"#777E90"} />}
          label={() => <Text style={styles.labelStyle}>İletişim</Text>}
          onPress={() => props.navigation.navigate("B2CCommunication")}
        />
        <DrawerItem 
        icon = {()=> <Icon name = "login" size = {24} color={'#777E90'}/> }
        label = {()=> <Text style={styles.labelStyle} >Giriş Yap</Text>}
        onPress={()=> props.navigation.navigate("SignIn")}
        />
        <DrawerItem
        icon = {()=> <Icon name = "user" size = {24} color = {"#777E90"} />}
        label = {()=> <Text style = {styles.labelStyle}>Kayıt Ol</Text>}
        onPress = {() => props.navigation.navigate("SignUp")}        
        />
      </DrawerContentScrollView>
    </View>
  );
}

function B2CSideBar() {
  const Drawer = createDrawerNavigator();
  const navigation = useNavigation();
  const {addToCart} = useContext(GlobalContext);
  return (
    <Drawer.Navigator
      drawerContent={(props) => <CustomDrawerContent {...props} />}
    >
      <Drawer.Screen
        name="B2CAnaSayfa"
        component={B2CRouter}
        options={{
          headerTitle: () => (
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                transform: [{ scale: 0.3 }],
              }}
            >
              <Logo />
            </View>
          ),
          headerRight: () => {
            return (
              <View style={{ flexDirection: "row" }}>
                <TouchableOpacity onPress={() => navigation.navigate("B2CBasket")}>
                  <Icon
                    name="shoppingcart"
                    size={24}
                    style={{ marginRight: 10 }}
                  />
                  {addToCart.length > 0 && (
                    <View style={styles.notificationContainer}>
                      <Text style={styles.notificationText}>
                        {addToCart.length}
                      </Text>
                    </View>
                  )}
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.navigate("SignIn")}>
                  <Icon name="login" size={24} style={{ marginRight: 10 }} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.navigate("SignUp")}>
                  <Icon name="user" size={24} style={{ marginRight: 10 }} />
                </TouchableOpacity>
              </View>
            );
          },
        }}
      />
    </Drawer.Navigator>
  );
}

export default B2CSideBar;

const styles = StyleSheet.create({
  labelStyle: {
    color: "#777E90",
    textAlign: "center",
    fontSize: 16,
    fontWeight: "bold",
  },

  personAvatar: {
    width: 52,
    height: 52,
    borderRadius: 100,
  },
  avatarContainer: {
    alignItems: "center",
    backgroundColor: "#fff",
    flexDirection: "row",
    justifyContent:'center',
    padding:50
  },
  notificationContainer: {
    backgroundColor: "#b81414",
    alignItems: "center",
    borderRadius: 20,
    padding: 2,
    position: "absolute",
    left: 18,
    bottom: 10,
  },
  notificationText: {
    fontSize: 12,
    color: "white",
  },
});
