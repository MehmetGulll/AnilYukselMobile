import React, { useState, useContext } from "react";
import { View, StyleSheet, Text, Image, Alert } from "react-native";
import Input from "../../components/Input";
import Button from "../../components/Button";
import { useRoute, useNavigation } from "@react-navigation/native";
import { GlobalContext } from "../../Context/GlobalStates";

function B2CProductDetail() {
  const navigation = useNavigation();
  const { addToCart, setAddToCart } = useContext(GlobalContext);
  const [quantity, setQuantity] = useState(null);
  const route = useRoute();
  const item = route.params ? JSON.parse(route.params.item) : undefined;

  const handleAddToCart = (item) => {
    const existingCartItem = addToCart.find((cartItem) => cartItem.id === item.id);
  
    if (existingCartItem) {
      setAddToCart(addToCart.map(
        (cartItem) => cartItem.id === item.id 
          ? { ...cartItem, quantity: parseInt(quantity) + cartItem.quantity } 
          : cartItem
      ));
    } else {
      setAddToCart([...addToCart, { ...item, quantity: parseInt(quantity) }]);
    }
  
    navigation.navigate("B2CBasket");
  };
  

  return (
    <View style={styles.itemDetailMainContainer}>
      <View style={styles.itemDetailContainer}>
        <Image source={{ uri: item?.Picture[0] }} style={styles.itemImage} />
        <View style={styles.itemInformations}>
          <Text style={styles.itemText}>{item?.Name}</Text>
          <View style={styles.itemDownText}>
            {/* <Text>Stok Adeti: {parseInt(item?.StockCount)}</Text> */}
          </View>
          <Input
            placeholder="Adet Giriniz.."
            textAlign="center"
            keyboardType="numeric"
            onChangeText={(value) => setQuantity(value)}
          />
          <View style={{ marginBottom: 10 }}>
            <Button
              text="Sepete Ekle"
              onPress={() => handleAddToCart(item)}
              disabled={
                !quantity ||
                (item &&
                  item?.stock_unit &&
                  item?.stock_unit.length > 0 &&
                  !item?.stock_unit)
              }
            />
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  itemImage: {
    width: "100%",
    height: 300,
    flex: 1,
  },
  itemDetailMainContainer: {
    backgroundColor: "white",
    flex: 1,
  },

  itemDetailContainer: {
    backgroundColor: "white",
    flex: 1,
  },
  itemInformations: {
    alignItems: "center",
    borderRadius: 20,
    backgroundColor: "white",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 10,
  },
  itemText: {
    fontSize: 30,
    marginTop: 35,
  },
  itemDownText: {
    color: "#6a6a6a",
    fontWeight: "700",
    marginTop: 10,
  },
  itemDownTextEquial: {
    color: "#949494",
    marginVertical: 10,
  },
  itemNewPrice: {
    fontSize: 35,
  },
  unDiscountPrice: {
    textDecorationLine: "line-through",
  },
  itemDiscountContainer: {
    marginLeft: 10,
    justifyContent: "center",
    backgroundColor: "#b81414",
    padding: 5,
    borderRadius: 10,
  },
  priceContainer: {
    flexDirection: "row",
    margin: 10,
    alignItems: "center",
  },
  itemDiscountText: {
    color: "white",
  },
  itemOldPrice: {
    textDecorationLine: "line-through",
  },
});

export default B2CProductDetail;
