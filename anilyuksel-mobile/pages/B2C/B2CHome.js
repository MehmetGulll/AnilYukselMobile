import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  FlatList,
  Modal,
  Image,
  TouchableOpacity,
} from "react-native";
import { ActivityIndicator } from "react-native-paper";
import Button from "../../components/Button";
import axios from "axios";
import { globalStyles } from "../../theme/GlobalStyles";
import { useNavigation } from "@react-navigation/native";
import Icon from 'react-native-vector-icons/AntDesign'
import DropDown from './B2CDropDownFilter'

const apihost = "https://b2b.anilyukselboya.com.tr/json/api";

function B2CHome() {
  const navigation = useNavigation();
  const [filterModal, setFilterModal] = useState(false);
  const [searchTerm, setSearchTerm] = useState('');
  const [B2CallProducts, setB2CAllProducts] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [backgroundOpacity, setBackgroundOpacity] = useState(1);
  useEffect(() => {
    fetchB2CProducts();
  }, []);
  const fetchB2CProducts = async () => {
    // try {
    //   const response = await axios.get(`${apihost}/products-b2c`);
    //   setB2CAllProducts(response.data);
    // } catch (error) {
    //   console.log("Error", error);
    // }
    try {
      const response = await axios.get(`${apihost}/products-b2c`);
      // Fiyatı 1'den yüksek olan ürünleri filtrele
      const filtered = response.data.filter(
        (product) => Number(product.ItemPrice) > 1
      );
      setB2CAllProducts(filtered);
    } catch (error) {
      console.log("Error", error);
    }
  };

  const getProductDetail = (item) =>{
    navigation.navigate('B2CProductDetail', {item: JSON.stringify(item)})
  }

  const filteredProducts = B2CallProducts ? B2CallProducts.filter(product => 
    product.Name.toLowerCase().includes(searchTerm.toLowerCase()) || 
    product.ProductCode.toLowerCase().includes(searchTerm.toLocaleLowerCase())
  ) : [];

  return (
    <View style={{ backgroundColor: "#fff", flex: 1 }}>
      <View style={styles.searchContainer}>
        <TextInput style={styles.searchInput} placeholder={"Ürün Arayınız"} onChangeText={text => setSearchTerm(text)} />
        <TouchableOpacity>
          <Icon
            name="filter"
            size={24}
            color="#777E90"
            style={styles.filterIcon}
            onPress={() => {
              setFilterModal(true);
              setBackgroundOpacity(0.2);
            }}
          />
        </TouchableOpacity>
        <Modal
          animationType="slide"
          transparent={true}
          visible={filterModal}
          onRequestClose={()=>{
            setFilterModal(!filterModal)
            setBackgroundOpacity(1);
          }}
          style={{ pointerEvents: "none" }}
        >
          <View style={styles.filterModalContainer}>
            <View style={styles.filterTagContainer}>
              <Text style={styles.filterTag}>Filtreleme</Text>
              <Icon
                name="filter"
                size={20}
                color="#777E90"
                style={{ color: "#33302E" }}
              />
            </View>
            <View
              style={{
                borderBottomColor: "#F3F3F6",
                borderBottomWidth: 1,
                marginHorizontal: 25,
              }}
            />

            <DropDown />

            <View style={styles.moneyFilter}>
              <View style={{ alignItems: "center", marginTop: 40 }}>
                <Text style={{ color: "#33302E" }}>Fiyat</Text>
              </View>
              <View style={styles.minMaxMoney}>
                <TextInput
                  placeholder="En az"
                  style={styles.minMaxMoneyInput}
                  keyboardType="numeric"
                />
                <TextInput
                  placeholder="En çok"
                  style={styles.minMaxMoneyInput}
                  keyboardType="numeric"
                />
              </View>
              <View style={styles.filterButtons}>
                <Button
                  text={"Uygula"}
                  onPress={() => {
                    setFilterModal(false), setBackgroundOpacity(1);
                  }}
                />
              </View>
            </View>
          </View>
        </Modal>
      </View>
      <View style={styles.itemMainContainer}>
        <FlatList
          data={filteredProducts}
          onEndReached={fetchB2CProducts}
          onEndReachedThreshold={0.5}
          ListFooterComponent={isLoading ? <ActivityIndicator /> : null}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => (
            <TouchableOpacity
              style={styles.itemContainer}
              onPress={() => getProductDetail(item)}
            >
              <Image
                source={{ uri: item.Picture[0] }}
                Tamam
                style={styles.itemImage}
              />
              {/* <Text style={styles.itemStock}>Stok Adet: {parseInt(item?.StockCount)}</Text> */}
              <Text
                style={styles.itemName}
                numberOfLines={2}
                ellipsizeMode="tail"
              >
                {item.Name}
              </Text>
              <Text style={styles.itemCode}>{item.ProductCode}</Text>
              <Text style={styles.itemNewPrice}>
                {new Intl.NumberFormat("tr-TR", {
                  style: "currency",
                  currency: "TRY",
                }).format(Number(item.ItemPrice))}
              </Text>
            </TouchableOpacity>
          )}
          numColumns={2}
          key={2}
          contentContainerStyle={{ paddingBottom: 120 }}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  itemMainContainer: {
    alignItems: "center",
    marginTop: 10,
    marginBottom: 20,
  },
  itemContainer: {
    flexDirection: "column",
    alignItems: "center",
    width: "48%",
    marginBottom: 50,
    margin: 5,
    borderRadius: 15,
    backgroundColor: "#fff",
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.7,
    shadowRadius: 3.84,
    elevation: 9,
  },
  itemName: {
    color: "#212121",
    fontWeight: "700",
    padding: 10,
    textAlign: "center",
  },
  itemCode: {
    color: "grey",
  },
  priceContainer: {
    flexDirection: "row",
    margin: 10,
    alignItems: "center",
  },
  itemNewPrice: {
    marginVertical: 20,
    fontWeight: "700",
    fontSize: 20,
  },
  itemOldPrice: {
    textDecorationLine: "line-through",
    marginRight: 5,
  },
  itemDiscountContainer: {
    marginLeft: 10,
    justifyContent: "center",
    backgroundColor: globalStyles.colors.logoColor,
    padding: 5,
    borderRadius: 10,
    color: "#fff",
  },
  itemDiscountText: {
    fontSize: 10,
    color: "white",
  },
  searchContainer: {
    alignItems: "center",
    flexDirection: "row",
    marginTop: 20,
  },
  itemImage: {
    width: 150,
    height: 150,
    borderRadius: 28,
    margin: 10,
  },
  searchInput: {
    backgroundColor: "#FAFAFA",
    padding: 13,
    marginHorizontal: 32,
    borderRadius: 20,
    flex: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  filterIcon: {
    backgroundColor: "#FAFAFA",
    padding: 13,
    borderRadius: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginRight: 30,
  },
  filterModalContainer: {
    width: "70%",
    backgroundColor: "#FFF",
    borderTopRightRadius: 25,
    borderBottomRightRadius: 25,
    flex: 1,
  },
  filterTagContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 27,
  },
  filterTag: {
    color: "#33302E",
    fontSize: 20,
    fontWeight: "700",
  },
  moneyFilter: {
    flexDirection: "column",
  },
  minMaxMoney: {
    flexDirection: "row",
    justifyContent: "center",
  },
  minMaxMoneyInput: {
    borderWidth: 1,
    flex: 1,
    textAlign: "center",
    padding: 10,
    margin: 10,
    borderRadius: 50,
    borderColor: "#9B9B9B",
  },
  filterButtons: {
    paddingHorizontal: 40,
  },
});

export default B2CHome;
