import React, { useState, useEffect, useRef, useContext } from "react";
import {
  View,
  Text,
  Animated,
  TouchableOpacity,
  TextInput,
  Modal,
  FlatList,
} from "react-native";
import Button from "../../components/Button";
import axios from "axios";
import { GlobalContext } from "../../Context/GlobalStates";

const apihost = "https://b2b.anilyukselboya.com.tr/json/api";

function B2CDropDownFilter() {
  const [modalVisible, setModalVisible] = useState(false);
  const [categories, setCategories] = useState([]);
  const [newCategories,setNewCategories] = useState()

  useEffect(()=>{
    const fetchCategories = async()=>{
      const response = await axios.get(`${apihost}/categories`);
      const categoryNames = response.data.response_data.categories.map(category => category.Name);
      setCategories(categoryNames);
    }
    fetchCategories();
  },[])
  return (
    <View style={{ justifyContent: "center", alignItems: "center" }}>
      <TouchableOpacity
        onPress={() => setModalVisible(true)}
        style={{
          marginTop: 40,
          paddingHorizontal: 65,
          borderRadius: 50,
          paddingVertical: 15,
          borderWidth: 1,
          borderColor: "#777E90",
        }}
      >
        <Text style={{ color: "#777E90" }}>Kategori Seçiniz</Text>
      </TouchableOpacity>

      <Modal
        animationType="slide"
        transparent={false}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
        }}
      >
        <View style={{ flex: 1, justifyContent: "center" }}>
          <View style={{ alignItems: "center" }}>
            <TextInput
              placeholder="Kategori Arayınız..."
              style={{
                height: 40,
                borderColor: "#777E90",
                borderWidth: 1,
                marginTop: 15,
                paddingHorizontal: 80,
                borderRadius: 50,
              }}
              onChangeText={(text) => setSearchTerm(text)}
            />
          </View>

          <FlatList
            data={categories}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => (
              <TouchableOpacity
                style={{
                  marginHorizontal: 15,
                  marginTop: 25,
                  borderBottomWidth: 1,
                  borderColor: "#777E90",
                }}
                onPress={() => {
                  setModalVisible(false);
                }}
              >
                <Text>{item}</Text>
              </TouchableOpacity>
            )}
          />

          <TouchableOpacity
            onPress={() => setModalVisible(false)}
            style={{ alignItems: "center", marginBottom: 20 }}
          >
            <Text style={{ fontSize: 25 }}>Kapat</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    </View>
  );
}

export default B2CDropDownFilter;
