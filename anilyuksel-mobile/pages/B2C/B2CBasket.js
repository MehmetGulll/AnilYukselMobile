import React, { useContext, useState } from "react";
import {
  View,
  Text,
  FlatList,
  Image,
  StyleSheet,
  TouchableOpacity,
} from "react-native";
import { GlobalContext } from "../../Context/GlobalStates";
import Button from "../../components/Button";
import { useNavigation } from "@react-navigation/native";

function B2CBasket() {
  const navigation = useNavigation();
  const { addToCart, setAddToCart } = useContext(GlobalContext);

  const total = addToCart.reduce(
    (sum, item) => sum + item.ItemPrice * item.quantity,
    0
  );
  const totalTax = addToCart.reduce(
    (sum, item) => sum + (item.ItemPrice * item.Tax) / 100,
    0
  );
  const totalWithoutTax = addToCart.reduce(
    (sum, item) => sum + item.ItemPrice * (1 - item.Tax / 100),
    0
  );

  const ComplateOrder = () => {
    navigation.navigate("SignIn");
  };

  const removeItem = (itemToRemove) => {
    setAddToCart((prevCartItems) => {
      return prevCartItems.filter((item) => item.id !== itemToRemove.id);
    });
  };
  return (
    <View style={{ backgroundColor: "#FFF", flex: 1 }}>
      <FlatList
        data={addToCart}
        keyExtractor={(item) => item.id}
        renderItem={({ item }) => (
          <View style={styles.itemContainer}>
            {/* <Image source={item.image} style={styles.itemPicture} /> */}
            <View style={styles.itemInformations}>
              <Text style={styles.itemTitle}>{item.Name}</Text>
              <View style={{ flexDirection: "row" }}>
                <Text style={styles.itemNewPrice}>{item.quantity}</Text>
                <Text
                  style={{ fontWeight: "700", fontSize: 16, marginLeft: 5 }}
                >
                  {item.StockUnitName}
                </Text>
              </View>
              <View>
                <Text style = {{fontWeight:'500', fontSize:16, marginTop:5}}>{item.Code}</Text>
                </View>

              <View style={styles.itemPiece}>
                <Text style={styles.itemCode}>{item.StockCode}</Text>
                <TouchableOpacity
                  style={styles.itemCount}
                  onPress={() => removeItem(item)}
                >
                  <Text>Ürünü Sil</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        )}
      />
      <View style={styles.payInformations}>
        <View style={styles.payInformationContainer}>
          <Text style={styles.payInformation}>Ara Toplam:</Text>
          <Text style={styles.moneyInformation}>{new Intl.NumberFormat("tr-TR", {
              style: "currency",
              currency: "TRY",
            }).format(Number(totalWithoutTax))}</Text>
        </View>
        <View style={styles.payInformationContainer}>
          <Text style={styles.payInformation}>Toplam KDV:</Text>
          <Text style={styles.moneyInformation}>{new Intl.NumberFormat("tr-TR", {
              style: "currency",
              currency: "TRY",
            }).format(Number(totalTax))}</Text>
        </View>
        <View style={styles.totalCart}>
          <Text style={styles.payTotal}>Toplam:</Text>
          <Text style={styles.payTotal}>
            {new Intl.NumberFormat("tr-TR", {
              style: "currency",
              currency: "TRY",
            }).format(Number(total))}
          </Text>
        </View>
        <View style={styles.payButton} onPress>
          <Button
            onPress={ComplateOrder}
            text="Sipariş Oluştur"
            backgroundColor="#343434"
            color="#FFF"
          />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  itemContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    margin: 10,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: "#FBFBFB",
    backgroundColor: "white",
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 4,
  },
  itemImage: {
    width: 120,
    height: 120,
    borderRadius: 28,
    marginRight: 13,
  },
  itemInformations: {
    alignItems: "flex-start",
    padding: 20,
  },
  itemTitle: {
    fontSize: 13,
    color: "#212121",
    fontWeight: "700",
    marginBottom: 5,
  },
  itemCode: {
    color: "grey",
  },
  itemCount: {
    flexDirection: "row",
    borderRadius: 20,
    borderWidth: 1,
    borderColor: "#8E8E8E",
    paddingHorizontal: 15,
    paddingVertical: 3,
    alignItems: "center",
    justifyContent: "center",
    marginLeft: 15,
    marginTop: 7,
  },
  itemPiece: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    marginTop: 5,
  },
  itemPiecePlus: {
    marginRight: 15,
    fontSize: 20,
    color: "#8e8e8e",
  },
  itemPieceMinus: {
    marginLeft: 15,
    fontSize: 20,
    color: "#8e8e8e",
  },
  priceContainer: {
    flexDirection: "row",
    margin: 10,
    alignItems: "center",
  },
  itemNewPrice: {
    fontSize: 16,
    fontWeight: "700",
  },
  itemCancel: {
    marginTop: 5,
    borderWidth: 0,
    padding: 5,
    borderRadius: 8,
    textAlign: "center",
    color: "white",
    backgroundColor: "#b81414",
  },
  payInformations: {
    borderRadius: 20,
    backgroundColor: "white",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 8,
  },
  payInformationContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderBottomWidth: 1,
    padding: 24,
    borderBottomColor: "#e8e8e8",
  },
  payInformation: {
    color: "#8a8a8f",
    fontSize: 15,
    fontWeight: "700",
    color: "black",
    marginTop: 5,
  },
  totalCart: {
    marginTop: 20,
    paddingLeft: 24,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  payTotal: {
    fontSize: 20,
    fontWeight: "700",
    marginHorizontal: 10,
  },
  payButton: {
    marginTop: 15,
    alignItems: "center",
  },
  moneyInformation: {
    fontSize: 14,
    fontWeight: "700",
  },
  inputVariable: {
    marginHorizontal: 15,
  },
  pickerContainer: {
    borderWidth: 1,
    borderColor: "#D6D6D6",
    borderRadius: 15,
    marginTop: 15,
  },
});

export default B2CBasket;
