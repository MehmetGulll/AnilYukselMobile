import React, { useContext, useState, useEffect } from "react";
import { View, StyleSheet, Image, Text, Alert, TextInput } from "react-native";
import Button from "../../components/Button";
import Input from "../../components/Input";
import { GlobalContext } from "../../Context/GlobalStates";
import { useNavigation, useRoute } from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";

const apihost = "https://b2b.anilyukselboya.com.tr/json/api";

function ProductDetails() {
  const { currentType, setB2CAddToCart, b2cAddToCart } =
    useContext(GlobalContext);
  const [product, setProduct] = useState(null);
  const [authUser, setAuthUser] = useState(null);
  const [seller, setSeller] = useState(null);
  const [loading, setLoading] = useState(false);
  const [depot_id, setDepot_id] = useState(null);
  const [quantity, setQuantity] = useState(null);
  const [stock_unit, setStock_unit] = useState([]);
  const navigation = useNavigation();
  const route = useRoute();

  useEffect(() => {
    const fetchAuthAndProduct = async () => {
      try {
        const authUser = JSON.parse(await AsyncStorage.getItem("authUser"));
        const product = JSON.parse(route.params.product);
        console.log("Product Objesi:", product);
        if (!product) {
          console.log("Product null");
        }
        setAuthUser(authUser);
        setProduct(product);
      } catch (error) {
        console.log("Error", error);
      }
    };
    fetchAuthAndProduct();
  }, []);

  useEffect(() => {
    initiate();
  }, []);
  const initiate = async () => {
    try {
        setLoading(true);
        const seller = JSON.parse(await AsyncStorage.getItem("seller"));
        setSeller(seller);
      
    } catch (error) {
      console.log("Error", error);
    }
  };
  const addToCart = async (product) => {
    console.log("Tokenimiz", authUser.token);
    console.log("seller idimiz",seller.id);
    console.log(seller.PriceType_id);
    console.log(product.id);
    setLoading(true);
    if(currentType!=='b2c'){
      const response = await axios.get(
        `${apihost}/addtocart/?token=${authUser.token}&seller_id=${
          seller.id
        }&sellerPriceType=${seller.PriceType_id}&depot_id=1&product_id=${
          product.id
        }&quantity=${quantity.toString().replace(",", ".")}`,
        product?.stock_units.length > 0
          ? { stockUnitCode: stock_unit?.unit_id }
          : {}
      );
      await navigation.navigate("Sepet", {
        product: JSON.stringify(product),
        quantity: quantity,
      });
    }
    else{
      console.log(quantity);
      const response = await axios.get(
        `${apihost}/addtocart/?token=${authUser.token}&seller_id=${
          seller.id
        }&sellerPriceType=${seller.PriceType_id}&depot_id=1&product_id=${
          product.id
        }&quantity=${quantity.toString().replace(",", ".")}`,
        product?.stock_units.length > 0
          ? { stockUnitCode: stock_unit?.unit_id }
          : {}
      );
      await navigation.navigate("Sepet", {
        product: JSON.stringify(product),
        quantity: quantity,
      });
    }
      
  };


  return (
    <View style={styles.itemDetailMainContainer}>
      <View style={styles.itemDetailContainer}>
        <Image source={{ uri: product?.Picture[0] }} style={styles.itemImage} />
        <View style={styles.itemInformations}>
          <Text style={styles.itemText}>{product?.Name}</Text>
          <View style={styles.itemDownText}>
            <Text style={styles.itemDownTextEquial}>
              Stok Kodu: {product?.ProductCode}
            </Text>
            <Text>Stok Adeti: {Math.floor(product?.StockCount)}</Text>
          </View>
          <Input
            placeholder="Adet Giriniz.."
            textAlign="center"
            keyboardType="numeric"
            onChangeText={(value) => setQuantity(value)}
          />
          <View style={{ marginBottom: 10 }}>
            <Button
              text="Sepete Ekle"
              onPress={() => addToCart(product)}
              disabled={
                !quantity ||
                (product &&
                  product.stock_unit &&
                  product.stock_unit.length > 0 &&
                  !product.stock_unit)
              }
            />
          </View>
        </View>
      </View>
    </View>
  );
}
export default ProductDetails;

const styles = StyleSheet.create({
  itemImage: {
    width: "100%",
    height: 300,
    flex: 1,
  },
  itemDetailMainContainer: {
    backgroundColor: "white",
    flex: 1,
  },

  itemDetailContainer: {
    backgroundColor: "white",
    flex: 1,
  },
  itemInformations: {
    alignItems: "center",
    borderRadius: 20,
    backgroundColor: "white",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 10,
  },
  itemText: {
    fontSize: 30,
    marginTop: 35,
  },
  itemDownText: {
    color: "#6a6a6a",
    fontWeight: "700",
    marginTop: 10,
  },
  itemDownTextEquial: {
    color: "#949494",
    marginVertical: 10,
  },
  itemNewPrice: {
    fontSize: 35,
  },
  unDiscountPrice: {
    textDecorationLine: "line-through",
  },
  itemDiscountContainer: {
    marginLeft: 10,
    justifyContent: "center",
    backgroundColor: "#b81414",
    padding: 5,
    borderRadius: 10,
  },
  priceContainer: {
    flexDirection: "row",
    margin: 10,
    alignItems: "center",
  },
  itemDiscountText: {
    color: "white",
  },
  itemOldPrice: {
    textDecorationLine: "line-through",
  },
});
