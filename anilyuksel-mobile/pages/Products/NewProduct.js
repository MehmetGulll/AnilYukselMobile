import React, { useState, useContext } from "react";
import {
  View,
  StyleSheet,
  FlatList,
  Image,
  Text,
  TouchableOpacity,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import Input from "../../components/Input";
import { GlobalContext } from "../../Context/GlobalStates";
import Icon from "react-native-vector-icons/AntDesign";

function AllProducts() {
  const navigation = useNavigation();
  const [searchProduct, setSearchProduct] = useState("");
  const { product } = useContext(GlobalContext);
  const filteredProducts = product.filter(
    (item) =>
      item.title.toLowerCase().includes(searchProduct.toLowerCase()) ||
      item.productCode.toLowerCase().includes(searchProduct.toLowerCase())
  );
  return (
    <View>
      <View style={styles.searchContainer}>
        <Icon name="search1" size={24} color="grey" />
        <Input
          style={styles.searchInput}
          placeholder={"Ürün Arayınız"}
          onChangeText={(text) => setSearchProduct(text)}
        />
      </View>
      <View style={styles.itemMainContainer}>
        <FlatList
          data={filteredProducts}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => (
            <TouchableOpacity
              style={styles.itemContainer}
              onPress={() => navigation.navigate("EighthScreen", { item })}
            >
              <Image source={item.image} style={styles.itemImage} />
              <Text style={styles.itemStock}>Stok Adet: {item.stock}</Text>
              <Text style={styles.itemName}>{item.title}</Text>
              <Text style={styles.itemCode}>{item.productCode}</Text>
              <Text style={styles.itemNewPrice}>
                {Number(item.oldPrice) -
                  Number(item.oldPrice) * Number(item.discount / 100)}{" "}
                TL
              </Text>
              <View style={styles.priceContainer}>
                <Text style={styles.itemOldPrice}>{item.oldPrice} TL</Text>
                <View style={styles.itemDiscountContainer}>
                  <Text style={styles.itemDiscountText}>%{item.discount}</Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
          numColumns={2}
          key={2}
          contentContainerStyle={{ paddingBottom: 120 }}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  itemMainContainer: {
    alignItems: "center",
  },
  itemContainer: {
    flexDirection: "column",
    alignItems: "center",
    margin: 10,
    borderWidth: 1,
    borderRadius: 28,
    borderColor: "grey",
  },
  itemName: {
    color: "#212121",
    fontWeight: "700",
    padding: 10,
  },
  itemCode: {
    color: "grey",
  },
  priceContainer: {
    flexDirection: "row",
    margin: 10,
    alignItems: "center",
  },
  itemNewPrice: {
    marginTop: 5,
    fontWeight: "700",
  },
  itemOldPrice: {
    textDecorationLine: "line-through",
  },
  itemDiscountContainer: {
    marginLeft: 10,
    justifyContent: "center",
    backgroundColor: "#b81414",
    padding: 5,
    borderRadius: 10,
  },
  itemDiscountText: {
    fontSize: 10,
    color: "white",
  },
  searchContainer: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "center",
    backgroundColor: "white",
  },
  itemImage: {
    width: 150,
    height: 150,
    borderRadius: 28,
    margin: 10,
  },
});

export default AllProducts;
