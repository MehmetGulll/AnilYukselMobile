import React, { useState, useContext, useEffect } from "react";
import {
  View,
  StyleSheet,
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  TextInput,
  Modal,
} from "react-native";
import axios, { all } from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";
import DropDown from "../../components/DropDownFilter";
import { globalStyles } from "../../theme/GlobalStyles";
import { useNavigation } from "@react-navigation/native";
import { GlobalContext } from "../../Context/GlobalStates";
import Icon from "react-native-vector-icons/AntDesign";
import Button from "../../components/Button";
import { ActivityIndicator } from "react-native-paper";
import DropDownHome from "../../components/DropDownHome";

const apihost = "https://b2b.anilyukselboya.com.tr/json/api";

function AllProducts() {
  const navigation = useNavigation();
  const [searchProduct, setSearchProduct] = useState("");
  const [searchTerm, setSearchTerm] = useState("");
  const [filterModal, setFilterModal] = useState(false);
  const [backgroundOpacity, setBackgroundOpacity] = useState(1);
  const [productShow, setProductShow] = useState(10);
  const [hasFetched, setHasFetched] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [page, setPage] = useState(1);
  const [minPrice, setMinPrice] = useState();
  const [maxPrice, setMaxPrice] = useState();
  const [filteredProducts, setFilteredProducts] = useState([]);
  const { allProducts, setAllProducts, selectedCategoryId, currentType } =
    useContext(GlobalContext);
  const [seller, setSeller] = useState(null);
  const { selectedSellerName, setSelectedSellerName } =
    useContext(GlobalContext);

  useEffect(() => {
    fetchProducts();
  }, []);

  useEffect(() => {
    filterProducts();
  }, [allProducts, searchProduct]);

  const fetchProducts = async () => {
    setIsLoading(true);
    const authUser = await AsyncStorage.getItem("authUser");
    const token = authUser ? JSON.parse(authUser).token : "";
    const seller = await AsyncStorage.getItem("seller");
    const sellerId = seller ? JSON.parse(seller).id : "";
    const priceTypeId = seller ? JSON.parse(seller).PriceType_id : "";
    try {
      if (currentType === "b2c" && hasFetched) {
        return;
      }
      if (currentType === "b2c") {
        setHasFetched(true);
        const response = await axios.get(`${apihost}/products-b2c`);
        console.log("currenta göre olandayım");
        console.log(response.data);
        await setAllProducts(response.data);
      } else {
        console.log("b2b ye göre burdayım");
        const response = await axios.get(
          `${apihost}/products/?token=${token}&seller_id=${sellerId}&sellerPriceType=${priceTypeId}&page=${page}`
        );
        if (response.data.response_status) {
          await setAllProducts([
            ...allProducts,
            ...response.data.response_data.products,
          ]);
          setPage(page + 1);
          navigation.navigate("Ürün Ara");
        }
      }
    } catch (error) {
      console.log("Hata:", error);
    }
    setIsLoading(false);
  };

  const applyFilter = async () => {
    filterProducts();
    // if(selectedCategoryId){
    //   const authUser = await AsyncStorage.getItem("authUser");
    //   const token = authUser ? JSON.parse(authUser).token : "";
    //   const seller = await AsyncStorage.getItem("seller");
    //   const sellerId = seller ? JSON.parse(seller).id : "";
    //   const priceTypeId = seller ? JSON.parse(seller).PriceType_id : "";
    //   const response = await axios.get(`${apihost}/products/?token=${token}&seller_id=${sellerId}&sellerPriceType=${priceTypeId}&page=${page}&category_id=${selectedCategoryId}`)
    // }
    // if(minPrice !== '' && maxPrice !== ''){
    //   const result = allProducts.filter(item => {
    //     const price = Number(item.ItemPrice);
    //     return price >= minPrice && price <= maxPrice;
    //   });
    //   setFilteredProducts(result)
    // }
  };

  const getProductDetail = (item) => {
    navigation.navigate("EighthScreen", { product: JSON.stringify(item) });
  };

  // const filterProducts = () => {
  //   let filtered = allProducts;
  //   if (currentType === "b2c") {
  //     if (searchProduct) {
  //       const filtered = allProducts.filter(
  //         (product) =>
  //           product.Name.toLowerCase().includes(searchProduct.toLowerCase()) ||
  //           product.ProductCode.toLowerCase().includes(
  //             searchProduct.toLocaleLowerCase()
  //           )
  //       );
  //       setFilteredProducts(filtered);
  //     } else {
  //       setFilteredProducts(allProducts);
  //     }
  //   } else {
  //     if (searchProduct) {
  //       const filtered = allProducts.filter((product) =>
  //         product.Name.toLowerCase().includes(searchProduct.toLowerCase())
  //       );
  //       setFilteredProducts(filtered);
  //     } else {
  //       setFilteredProducts(allProducts);
  //     }
  //     if (minPrice) {
  //       filtered = filtered.filter(
  //         (product) => Number(product.ItemPrice) >= Number(minPrice)
  //       );
  //     }
  //     if (maxPrice) {
  //       filtered = filtered.filter(
  //         (product) => Number(product.ItemPrice) <= Number(maxPrice)
  //       );
  //     }
  //     setFilteredProducts(filtered);
  //   }
  // };
  const filterProducts = () => {
    let filtered = allProducts;
    if (currentType === "b2c") {
      if (searchProduct) {
        filtered = allProducts.filter(
          (product) =>
            product.Name.toLowerCase().includes(searchProduct.toLowerCase()) ||
            product.ProductCode.toLowerCase().includes(
              searchProduct.toLocaleLowerCase()
            )
        );
      }
      // Fiyatı 1'den yüksek olan ürünleri filtrele
      filtered = filtered.filter(
        (product) => Number(product.ItemPrice) > 1
      );
      setFilteredProducts(filtered);
    } else {
      if (searchProduct) {
        filtered = allProducts.filter((product) =>
          product.Name.toLowerCase().includes(searchProduct.toLowerCase())
        );
      }
      if (minPrice) {
        filtered = filtered.filter(
          (product) => Number(product.ItemPrice) >= Number(minPrice)
        );
      }
      if (maxPrice) {
        filtered = filtered.filter(
          (product) => Number(product.ItemPrice) <= Number(maxPrice)
        );
      }
      setFilteredProducts(filtered);
    }
  };

  console.log("Total:", allProducts.length);
  return (
    <View style={{ backgroundColor: "#fff", opacity: backgroundOpacity }}>
      {currentType !== "b2c" && <DropDownHome text={selectedSellerName} />}

      <View style={styles.searchContainer}>
        <TextInput
          style={styles.searchInput}
          placeholder={"Ürün Arayınız"}
          onChangeText={(text) => setSearchProduct(text)}
        />
        <TouchableOpacity
          onPress={() => {
            setFilterModal(true);
            setBackgroundOpacity(0.2);
          }}
        >
          <Icon
            name="filter"
            size={24}
            color="#777E90"
            style={styles.filterIcon}
          />
        </TouchableOpacity>
        <Modal
          animationType="slide"
          transparent={true}
          visible={filterModal}
          onRequestClose={() => {
            applyFilter();
            setFilterModal(!filterModal);
            setBackgroundOpacity(1);
          }}
          style={{ pointerEvents: "none" }}
        >
          <View style={styles.filterModalContainer}>
            <View style={styles.filterTagContainer}>
              <Text style={styles.filterTag}>Filtreleme</Text>
              <Icon
                name="filter"
                size={20}
                color="#777E90"
                style={{ color: "#33302E" }}
              />
            </View>
            <View
              style={{
                borderBottomColor: "#F3F3F6",
                borderBottomWidth: 1,
                marginHorizontal: 25,
              }}
            />

            <DropDown />

            <View style={styles.moneyFilter}>
              <View style={{ alignItems: "center", marginTop: 40 }}>
                <Text style={{ color: "#33302E" }}>Fiyat</Text>
              </View>
              <View style={styles.minMaxMoney}>
                <TextInput
                  placeholder="En az"
                  style={styles.minMaxMoneyInput}
                  keyboardType="numeric"
                  onChangeText={(text) => setMinPrice(text)}
                />
                <TextInput
                  placeholder="En çok"
                  style={styles.minMaxMoneyInput}
                  keyboardType="numeric"
                  onChangeText={(text) => setMaxPrice(text)}
                />
              </View>
              <View style={styles.filterButtons}>
                <Button
                  text={"Uygula"}
                  onPress={() => {
                    applyFilter();
                    setFilterModal(false), setBackgroundOpacity(1);
                  }}
                />
              </View>
            </View>
          </View>
        </Modal>
      </View>
      <View style={styles.itemMainContainer}>
        <FlatList
          data={filteredProducts}
          onEndReached={fetchProducts}
          onEndReachedThreshold={0.5}
          ListFooterComponent={isLoading ? <ActivityIndicator /> : null}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => {
              return (
                <TouchableOpacity
                  style={styles.itemContainer}
                  onPress={() => getProductDetail(item)}
                >
                  <Image
                    source={{ uri: item.Picture[0] }}
                    style={styles.itemImage}
                  />
                  <Text style={styles.itemStock}>
                    Stok Adet: {Math.floor(item.StockCount)}
                  </Text>
                  <Text
                    style={styles.itemName}
                    numberOfLines={2}
                    ellipsizeMode="tail"
                  >
                    {item.Name}
                  </Text>
                  <Text style={styles.itemCode}>{item.ProductCode}</Text>
                  <Text style={styles.itemNewPrice}>
                    {new Intl.NumberFormat("tr-TR", {
                      style: "currency",
                      currency: "TRY",
                    }).format(Number(item.ItemPrice))}
                  </Text>
                </TouchableOpacity>
              );
            
              return(
                <TouchableOpacity
                  style={styles.itemContainer}
                  onPress={() => getProductDetail(item)}
                >
                  <Image
                    source={{ uri: item.Picture[0] }}
                    style={styles.itemImage}
                  />
                  <Text style={styles.itemStock}>
                    Stok Adet: {Math.floor(item.StockCount)}
                  </Text>
                  <Text
                    style={styles.itemName}
                    numberOfLines={2}
                    ellipsizeMode="tail"
                  >
                    {item.Name}
                  </Text>
                  <Text style={styles.itemCode}>{item.ProductCode}</Text>
                  <Text style={styles.itemNewPrice}>
                    {new Intl.NumberFormat("tr-TR", {
                      style: "currency",
                      currency: "TRY",
                    }).format(Number(item.ItemPrice))}
                  </Text>
                </TouchableOpacity>
              );
            
          }}
          numColumns={2}
          key={2}
          contentContainerStyle={{ paddingBottom: 120 }}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  itemMainContainer: {
    alignItems: "center",
    marginTop: 10,
    marginBottom: 20,
  },
  itemContainer: {
    flexDirection: "column",
    alignItems: "center",
    width: "48%",
    marginBottom: 50,
    margin: 5,
    borderRadius: 15,
    backgroundColor: "#fff",
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.7,
    shadowRadius: 3.84,
    elevation: 9,
  },
  itemName: {
    color: "#212121",
    fontWeight: "700",
    padding: 10,
    textAlign: "center",
  },
  itemCode: {
    color: "grey",
  },
  priceContainer: {
    flexDirection: "row",
    margin: 10,
    alignItems: "center",
  },
  itemNewPrice: {
    marginVertical: 20,
    fontWeight: "700",
    fontSize: 20,
  },
  itemOldPrice: {
    textDecorationLine: "line-through",
    marginRight: 5,
  },
  itemDiscountContainer: {
    marginLeft: 10,
    justifyContent: "center",
    backgroundColor: globalStyles.colors.logoColor,
    padding: 5,
    borderRadius: 10,
    color: "#fff",
  },
  itemDiscountText: {
    fontSize: 10,
    color: "white",
  },
  searchContainer: {
    alignItems: "center",
    flexDirection: "row",
    marginTop: 20,
  },
  itemImage: {
    width: 150,
    height: 150,
    borderRadius: 28,
    margin: 10,
  },
  searchInput: {
    backgroundColor: "#FAFAFA",
    padding: 13,
    marginHorizontal: 32,
    borderRadius: 20,
    flex: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  filterIcon: {
    backgroundColor: "#FAFAFA",
    padding: 13,
    borderRadius: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginRight: 30,
  },
  filterModalContainer: {
    width: "70%",
    backgroundColor: "#FFF",
    borderTopRightRadius: 25,
    borderBottomRightRadius: 25,
    flex: 1,
  },
  filterTagContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 27,
  },
  filterTag: {
    color: "#33302E",
    fontSize: 20,
    fontWeight: "700",
  },
  moneyFilter: {
    flexDirection: "column",
  },
  minMaxMoney: {
    flexDirection: "row",
    justifyContent: "center",
  },
  minMaxMoneyInput: {
    borderWidth: 1,
    flex: 1,
    textAlign: "center",
    padding: 10,
    margin: 10,
    borderRadius: 50,
    borderColor: "#9B9B9B",
  },
  filterButtons: {
    paddingHorizontal: 40,
  },
});

export default AllProducts;
