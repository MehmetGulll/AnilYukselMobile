import React, { useState, useContext } from "react";
import {
  View,
  StyleSheet,
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  TextInput,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { GlobalContext } from "../../Context/GlobalStates";


function PromotionalProducts() {
  const navigation = useNavigation();
  const [searchProduct, setSearchProduct] = useState("");
  const { product } = useContext(GlobalContext);
  const filteredProducts = product.filter(
    (item) =>
      item.title.toLowerCase().includes(searchProduct.toLowerCase()) ||
      item.productCode.toLowerCase().includes(searchProduct.toLowerCase())
  );
  return (
    
      <View style={styles.promotionalProductsContainer}>
        <View style={styles.searchContainer}>
          <TextInput
            style={styles.searchInput}
            placeholder={"Ürün Arayınız"}
            onChangeText={(text) => setSearchProduct(text)}
          />
        </View>
        <View style={styles.pageTag}>
          <Text style={styles.pageTagText}>Kampanyalı Ürünler</Text>
        </View>
        <View style={styles.itemMainContainer}>
          <FlatList
            data={filteredProducts}
            keyExtractor={(item) => item.id}
            renderItem={({ item }) => (
              <TouchableOpacity
                style={styles.itemContainer}
                onPress={() => navigation.navigate("EighthScreen", { item })}
              >
                <Image source={item.image} style={styles.itemImage} />
                <Text style={styles.itemStock}>Stok Adet: {item.stock}</Text>
                <Text style={styles.itemName}>{item.title}</Text>
                <Text style={styles.itemCode}>{item.productCode}</Text>
                <Text style={styles.itemNewPrice}>
                  {new Intl.NumberFormat("tr-TR", {
                    style: "currency",
                    currency: "TRY",
                  }).format(
                    Number(item.oldPrice) -
                      Number(item.oldPrice) * Number(item.discount / 100)
                  )}
                </Text>
                <View style={styles.priceContainer}>
                  <Text style={styles.itemOldPrice}>
                    {new Intl.NumberFormat("tr-TR", {
                      style: "currency",
                      currency: "TRY",
                    }).format(Number(item.oldPrice))}
                  </Text>
                  <View style={styles.itemDiscountContainer}>
                    <Text style={styles.itemDiscountText}>
                      %{item.discount}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            )}
            numColumns={2}
            key={2}
            contentContainerStyle={{ paddingBottom: 120 }}
          />
        </View>
      </View>

  );
}

const styles = StyleSheet.create({
  promotionalProductsContainer:{
    flex:1,
    backgroundColor:'#FFF',
  },
  itemMainContainer: {
    alignItems: "center",
  },
  pageTag: {
    marginTop: 10,
    marginLeft: 32,
  },
  pageTagText: {
    fontWeight: "700",
    fontSize: 20,
  },
  itemContainer: {
    flexDirection: "column",
    alignItems: "center",
    margin: 10,
  },
  itemName: {
    color: "#212121",
    fontWeight: "700",
    padding: 10,
  },
  itemCode: {
    color: "grey",
  },
  priceContainer: {
    flexDirection: "row",
    margin: 10,
    alignItems: "center",
  },
  itemNewPrice: {
    marginTop: 5,
    fontWeight: "700",
  },
  itemOldPrice: {
    textDecorationLine: "line-through",
  },
  itemDiscountContainer: {
    marginLeft: 10,
    justifyContent: "center",
    backgroundColor: "#b81414",
    padding: 5,
    borderRadius: 10,
  },
  itemDiscountText: {
    fontSize: 10,
    color: "white",
  },
  searchContainer: {
    alignItems: "center",
    flexDirection: "row",
    marginTop: 20,
    paddingHorizontal:10
  },
  searchInput: {
    backgroundColor: "#FAFAFA",
    padding: 13,
    marginHorizontal: 32,
    borderRadius: 20,
    flex: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  itemImage: {
    width: 150,
    height: 150,
    borderRadius: 28,
    margin: 10,
  },
});

export default PromotionalProducts;
