import React, { useContext, useEffect, useState } from "react";
import {
  View,
  StyleSheet,
  Text,
  FlatList,
  Image,
  ScrollView,
  Alert,
} from "react-native";
import { GlobalContext } from "../../Context/GlobalStates";
import Icon from "react-native-vector-icons/AntDesign";
import { Picker } from "@react-native-picker/picker";
import DateTimePicker from "@react-native-community/datetimepicker";
import Button from "../../components/Button";
import Input from "../../components/Input";
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import { TouchableOpacity } from "react-native-gesture-handler";
import { useFocusEffect, useNavigation } from "@react-navigation/native";

const apihost = "https://b2b.anilyukselboya.com.tr/json/api";

function Basket() {
  const navigation = useNavigation();
  const {
    setBasket,
    setBasketItem,
    currentType,

  } = useContext(GlobalContext);
  const [products, setProducts] = useState([]);
  const [shipping, setShipping] = useState(1);
  const [total, setTotal] = useState();
  const [adress, setAdress] = useState("");
  const [shippingDate, setShippingDate] = useState(new Date());
  const [authUser, setAuthUser] = useState(null);
  const [seller, setSeller] = useState(null);
  const [loading, setLoading] = useState(false);
  const [paymentMethods, setPaymentMethods] = useState([]);
  const [selectedPaymentMethodID, setSelectedPaymentMethodID] = useState(null);
  const [currencies, setCurrencies] = useState(null);
  const [isModalVisible, setModalVisible] = useState(false);
  const [showPicker, setShowPicker] = useState(false);
  const [description, setDescription] = useState("");
  const [totalIsk, setTotalIsk] = useState(0);
  const [discount_show, setDiscountShow] = useState(true);
  const [discount, setDiscount] = useState({
    additionalDiscountRate: 0,
    additionalDiscountAmount: 0,
  });
  const [maturity_date, setMaturityDate] = useState(
    new Date().toISOString().split("T")[0]
  );
  const [today, setToday] = useState(new Date());
  const [maturity_date_time, setMaturityDateTime] = useState(1);

  // useEffect(() => {
  //   refresh();
  // },[]);

  useFocusEffect(
    React.useCallback(() => {
      refresh();
    }, [])
  );
  const refresh = async () => {
    const authUser = JSON.parse(await AsyncStorage.getItem("authUser"));
    console.log(authUser.token);
    setAuthUser(authUser);
    const seller = JSON.parse(await AsyncStorage.getItem("seller"));
    setSeller(seller);
    const response1 = await axios.get(`${apihost}/get-currencies/`, {});
    setCurrencies(response1.data.response_data);
    if (currentType !== "b2c") {
      const response2 = await axios.get(
        `${apihost}/cart/?token=${authUser.token}&seller_id=${seller.id}&sellerPriceType=${seller.PriceType_id}`
      );
      console.log(response2);
      if (response2.data.response_status) {
        let updateProducts = response2.data.response_data.cartItems;
        let updateTotal = response2.data.response_data.totals[0];
        setBasketItem(response2.data.response_data.totals[0]);
        for (const product of updateProducts) {
          product.newAmount = product.Amount;
          product.PriceDetail.OrginalPriceWithTax = parseFloat(
            product.PriceWithoutTax
          ).toFixed(2);
          product.cartPrice = parseFloat(product.PriceWithoutTax).toFixed(2);
          product.totalPrice = (
            parseFloat(product.PriceWithoutTax) * parseFloat(product.Amount)
          ).toFixed(2);
          if (authUser.not_user) {
            product.totalPrice = product.Product_id / 1000;
          }
          product.discount = 0;
          for (const curr of response1.data.response_data) {
            curr.Rate = parseFloat(curr.Rate).toFixed(3);
            if (curr.Name == product.PriceDetail.OrginalCurrency) {
              product.current = curr;
            }
          }
        }
        setProducts(updateProducts);
        setTotal(updateTotal);
      }
    } else {
      const response2 = await axios.get(
        `${apihost}/cart/?token=${authUser.token}&seller_id=${seller.id}&sellerPriceType=${seller.PriceType_id}`
      );
      if (response2.data.response_status) {
        if (response2.data.response_status) {
          let updateProducts = response2.data.response_data.cartItems;
          let updateTotal = response2.data.response_data.totals[0];
          setBasketItem(response2.data.response_data.totals[0]);
          for (const product of updateProducts) {
            product.newAmount = product.Amount;
            product.PriceDetail.OrginalPriceWithTax = parseFloat(
              product.PriceWithoutTax
            ).toFixed(2);
            product.cartPrice = parseFloat(product.PriceWithoutTax).toFixed(2);
            product.totalPrice = (
              parseFloat(product.PriceWithoutTax) * parseFloat(product.Amount)
            ).toFixed(2);
            if (authUser.not_user) {
              product.totalPrice = product.Product_id / 1000;
            }
            product.discount = 0;
            for (const curr of response1.data.response_data) {
              curr.Rate = parseFloat(curr.Rate).toFixed(3);
              if (curr.Name == product.PriceDetail.OrginalCurrency) {
                product.current = curr;
              }
            }
          }
          setProducts(updateProducts);
          setTotal(updateTotal);
        }
      }
    }

    const response3 = await axios.get(`${apihost}/paymentmethods/`);
    setPaymentMethods(response3.data.response_data);
  };

  const getSubtotal = () => {
    return products.reduce(
      (total, item) => total + parseInt(item.price) * item.quantity,
      0
    );
  };

  const ComplateOrder = async () => {
    console.log("Complete içindeyim");
    if (currentType === "b2c") {
      setShipping("R");
      setShippingDate(0);
      console.log("currenta göre yolladım");
      if (paymentMethods.length > 0) {
        Alert.alert("Siparişi Tamamla", `Sipariş Notu: ${description}`, [
          {
            text: "İptal",
            onPress: () => console.log("Cancel Clicked"),
            style: "cancel",
          },
          {
            text: "Siparişi Tamamla",
            onPress: () =>
              navigation.navigate("Ödeme Yap", {
                total: calculateTotalGrand(),
                ordernote: description,
                additionalDiscountRate: discount.additionalDiscountRate,
                additionalDiscountAmount: discount.additionalDiscountAmount,
                maturity_date: maturity_date,
                maturity_days: maturity_date_time,
                shipping: shipping,
                addres: adress,
              }),
          },
        ]);
      }
    } else {
      if (paymentMethods.length > 0) {
        Alert.alert("Siparişi Tamamla", `Sipariş Notu : ${description}`, [
          {
            text: "İptal",
            onPress: () => console.log("Cancel clicked"),
            style: "cancel",
          },
          { text: "Siparişi Tamamla", onPress: () => orderSend() },
        ]);
      }
    }
  };

  const orderSend = async () => {
    const params = {
      token: authUser.token,
      seller_id: seller.id,
      sellerPriceType: seller.PriceType_id,
      ordernote: description,
      paymentmethod: 1,
      additionalDiscountRate: discount.additionalDiscountRate,
      additionalDiscountAmount: discount.additionalDiscountAmount,
      maturity_date: maturity_date,
      maturity_days: maturity_date_time,
    };
    const originalPrice = "originalPrice__";
    const sellerDiscountRate = "sellerDiscountRate__";
    const quantity = "quantity__";
    const price = "price__";
    const currency = "currency__";
    const taxRate = "taxRate__";
    const productName = "productName__";
    for (const item of products) {
      params[originalPrice + item.id] = item.totalPrice;
      params[quantity + item.id] = item.newAmount;
      params[price + item.id] = item.totalPrice;
      params[currency + item.id] = item.current?.Code;
      params[taxRate + item.id] = item.Tax;
      params[productName + item.id] = item.productName;
      params["shipping"] = shipping;
      params["addres"] = adress;
      params["shippingdateformat"] = shippingDate;
    }

    try {
      console.log(discount.additionalDiscountAmount);
      console.log(discount.additionalDiscountRate);
      const response = await axios.get(
        `${apihost}/complete_order/?token=${authUser.token}&seller_id=${seller.id}&sellerPriceType=${seller.PriceType_id}&ordernote=${description}&paymentmethod=1&additionalDiscountRate=${discount.additionalDiscountRate}&additionalDiscountAmount=${discount.additionalDiscountAmount}&maturity_date=${maturity_date}&maturity_days=${maturity_date_time}&shipping=${shipping}&addres=${adress}`
      );
      console.log(response);
      console.log(response.data.response_status);
      if (response.data.response_status) {
        console.log("Başarılı");
        Alert.alert("Başarılı", "Sipariş Oluşturuldu.", [
          { text: "Tamam", onPress: () => navigation.navigate("Ürün Ara") },
        ]);
      } else {
        console.log("Hatalısın");
      }
    } catch (error) {
      console.log("Error", error);
    }
  };

  const removeItem = async (item) => {
   
      Alert.alert(
        "Ürün Silme Onayı",
        "Ürünü Sepetinizden Çıkartmak İstediğinize Emin Misiniz?",
        [
          {
            text: "İptal",
            style: "cancel",
            onPress: () => console.log("Kapatıldı"),
          },
          {
            text: "Sil",
            onPress: async () =>{
              await axios.get(
                `${apihost}/removefromcart/?token=${authUser.token}&product_id=${item.Product_id}`
              ),
              console.log("Ürün silindi")
              const newProducts = products.filter(product => product.id !== item.id);
              setProducts(newProducts);
            }
          },
        ]
      );
  };

  const increaseAmount = (item) => {
    const amount = parseInt(item.newAmount);
    if (amount > 1) {
      item.newAmount = amount - 1;
    }
    const price = parseFloat(item.cartPrice);
    const calculation = item.newAmount * price;
    item.totalPrice = calculation.toFixed(4);
  };

  const decreaseAmount = (item) => {
    item.newAmount = parseInt(item.newAmount) + 1;
    const price = parseFloat(item.cartPrice);
    const calculation = item.newAmount * price;
    item.totalPrice = calculation.toFixed(2);
  };

  const onChangeAmount = (value, item) => {
    const price = parseFloat(item.cartPrice);
    const calculation = value * price;
    item.totalPrice = calculation.toFixed(4);
  };

  const onChangePrice = (value, item) => {
    const calculation = item.newAmount * value;
    item.totalPrice = calculation.toFixed(2);
  };

  const onChangeDiscount = (value, item) => {
    value = value == "" || value == " " ? 0 : value;
    item.totalPrice = parseFloat(item.cartPrice) * parseFloat(item.newAmount);
    const disc = (parseFloat(value) * parseFloat(item.totalPrice)) / 100;
    item.totalPrice = (parseFloat(item.totalPrice) - disc).toFixed(2);
  };

  const calculateTotalGross = () => {
    let total = 0;
    if (products) {
      for (const prod of products) {
        const price = parseFloat(prod.totalPrice) * parseFloat(prod.Amount);
        const rate = prod.current ? parseFloat(prod.current.Rate) : 0;
        const priceWithCurr = price * rate;
        total += priceWithCurr;
      }
    }
    return total.toFixed(2);
  };

  const calculateTotalNew = () => {
    let total = 0;
    if (products) {
      for (const prod of products) {
        console.log(prod);
        const price = parseFloat(prod.totalPrice);
        const rate = prod.current ? parseFloat(prod.current.Rate) : 0;
        const priceWithCurr = price * rate;
        total += priceWithCurr;
      }
    }
    return (total - parseFloat(calculateTotalTax())).toFixed(2);
  };

  const calculateTotalIsk = () => {
    let total = 0;
    if (products) {
      for (const prod of products) {
        const price = parseFloat(prod.totalPrice);
        const rate = prod.current ? parseFloat(prod.current.Rate) : 0;
        const priceWithCurr = price * rate;
        total += priceWithCurr;
      }
    }
    return (total - parseFloat(calculateTotalGross())).toFixed(2);
  };

  const calculateTotal = () => {
    let total = 0;
    if (products) {
      for (const prod of products) {
        const price = parseFloat(prod.totalPrice);
        const rate = prod.current ? parseFloat(prod.current.Rate) : 0;
        const priceWithCurr = price * rate;
        total += priceWithCurr;
      }
    }
    console.log(calculateTotalTax());
    return (total + parseFloat(calculateTotalTax())).toFixed(2);
  };
  const calculateTotalTax = () => {
    let total = 0;
    if (products) {
      for (const prod of products) {
        const price = parseFloat(prod.totalPrice) * parseFloat(prod.Amount);
        const rate = prod.current ? parseFloat(prod.current.Rate) : 0;
        const priceWithCurr = price * rate;
        total += (priceWithCurr * prod.Tax) / 100;
      }
    }
    return total.toFixed(2);
  };

  const calculateTotalGrand = () => {
    const priceWithTax =
      parseFloat(calculateTotalGross()) + parseFloat(calculateTotalTax());
    return (priceWithTax + discount.additionalDiscountAmount).toFixed(2);
  };

  const onChangeDiscountRate = () => {
    discount.additionalDiscountRate = discount.additionalDiscountRate.replace(
      ",",
      "."
    );
    const totalPrice = parseFloat(calculateTotalGrand());
    const discountRate = discount.additionalDiscountRate;
    const discountAmount = (totalPrice * discountRate) / 100;
    discount.additionalDiscountAmount = discountAmount.toFixed(2);
  };
  const onChangeDiscountAmount = () => {
    discount.additionalDiscountAmount =
      discount.additionalDiscountAmount.replace(",", ".");
    const totalPrice = parseFloat(calculateTotalGrand());
    const discountAmount = discount.additionalDiscountAmount;
    const rate = (100 * discountAmount) / totalPrice;
    discount.additionalDiscountRate = rate.toFixed(2);
  };

  const calculateMaturityDate = (key) => {
    if (key === "date") {
      const now = new Date();
      const maturityDate = new Date(maturity_date);
      const diff = Math.abs(maturityDate.getTime() - now.getTime());
      const diffDays = Math.ceil(diff / (1000 * 3600 * 24));
      setMaturityDateTime(diffDays);
    }
  };

  const onChangeDateTime = (event, selectedDate) => {
    const currentDate = selectedDate || shippingDate;
    setShippingDate(currentDate);
    const formattedDate = currentDate.toISOString().split("T")[0];
    setMaturityDate(formattedDate);
    console.log(shippingDate);
    setShowPicker(false);
    if (selectedDate === undefined) {
      setShowPicker(false);
    }
  };


  return (
    <ScrollView style={{ backgroundColor: "white" }}>
      <View>
        <FlatList
          data={products}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => (
            <View style={styles.itemContainer}>
              <Image source={item.image} style={styles.itemImage} />
              <View style={styles.itemInformations}>
                <Text style={styles.itemTitle}>
                  {item.Name.length > 25
                    ? item.Name.slice(0, 25) + "..."
                    : item.Name}
                </Text>
                <Text style={styles.itemNewPrice}>
                  {Math.floor(item.newAmount)}

                  <Text>{item.StockUnitName}</Text>
                </Text>

                <View style={styles.itemPiece}>
                  <Text style={styles.itemCode}>{item.StockCode}</Text>
                  <TouchableOpacity
                    style={styles.itemCount}
                    onPress={() => removeItem(item)}
                  >
                    <Text>Ürünü Sil</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          )}
        />

        <View style={styles.payInformations}>
          {currentType !== "b2c" && (
            <View style={styles.payInformationContainer}>
              <Text style={styles.payInformation}>Toplam İskonto:</Text>
              <Text style={styles.moneyInformation}>
                {new Intl.NumberFormat("tr-TR", {
                  style: "currency",
                  currency: "TRY",
                }).format(Number(calculateTotalIsk()))}
              </Text>
            </View>
          )}
          <View style={styles.payInformationContainer}>
            <Text style={styles.payInformation}>Ara Toplam:</Text>
            <Text style={styles.moneyInformation}>
              {new Intl.NumberFormat("tr-TR", {
                style: "currency",
                currency: "TRY",
              }).format(Number(calculateTotalGross()))}
            </Text>
          </View>
          <View style={styles.payInformationContainer}>
            <Text style={styles.payInformation}>Toplam KDV:</Text>
            <Text style={styles.moneyInformation}>
              {new Intl.NumberFormat("tr-TR", {
                style: "currency",
                currency: "TRY",
              }).format(Number(calculateTotalTax()))}
            </Text>
          </View>
          <View style={styles.totalCart}>
            <Text style={styles.payTotal}>Toplam:</Text>
            <Text style={styles.payTotal}>
              {new Intl.NumberFormat("tr-TR", {
                style: "currency",
                currency: "TRY",
              }).format(Number(calculateTotalGrand()))}
            </Text>
          </View>
          <View style={styles.inputVariable}>
            {currentType !== "b2c" && (
              <>
                <Text
                  style={{ color: "black", marginTop: 15, fontWeight: "700" }}
                >
                  Proje Kodunu Seçiniz
                </Text>
                <View style={styles.pickerContainer}>
                  <Picker
                    selectedValue={shipping}
                    onValueChange={(itemValue, itemIndex) =>
                      setShipping(itemValue)
                    }
                  >
                    <Picker.Item label="R" value="1" />
                    <Picker.Item label="G" value="2" />
                  </Picker>
                </View>
              </>
            )}

            <Text style={{ color: "black", marginTop: 15, fontWeight: "700" }}>
              {" "}
              Sipariş Notunuzu Giriniz
            </Text>
            <Input
              placeholder={"Sipariş Notunuz"}
              onChangeText={(text) => setDescription(text)}
            />
            {currentType !== "b2c" && (
              <>
                <Text
                  style={{ color: "black", marginTop: 15, fontWeight: "700" }}
                >
                  Teslimat Adresini Giriniz
                </Text>
                <Input
                  placeholder={"Teslimat Adresiniz"}
                  onChangeText={(text) => setAdress(text)}
                />
                <Text
                  style={{ color: "black", marginTop: 15, fontWeight: "700" }}
                >
                  Teslim Tarihi Seçiniz
                </Text>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <TouchableOpacity
                    style={{ marginRight: 20 }}
                    onPress={() => setShowPicker(true)}
                  >
                    <Icon name="calendar" size={32} />
                  </TouchableOpacity>
                  <Button
                    text={shippingDate.toLocaleDateString()}
                    onPress={() => setShowPicker(true)}
                  />
                  {showPicker && (
                    <DateTimePicker
                      value={shippingDate}
                      mode={"date"}
                      display="default"
                      onChange={onChangeDateTime}
                    />
                  )}
                </View>
              </>
            )}
          </View>
          <View style={styles.payButton}>
            <Button
              onPress={ComplateOrder}
              text="Sipariş Oluştur"
              backgroundColor="#343434"
              color="#FFF"
            />
          </View>
        </View>
      </View>
    </ScrollView>
  );
}

export default Basket;

const styles = StyleSheet.create({
  itemContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    margin: 10,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: "#FBFBFB",
    backgroundColor: "white",
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 4,
  },
  itemImage: {
    width: 100,
    height: 100,
    borderRadius: 28,
    backgroundColor: "grey",
  },
  itemInformations: {
    alignItems: "flex-start",
    padding: 20,
  },
  itemTitle: {
    fontSize: 13,
    color: "#212121",
    fontWeight: "700",
    marginBottom: 5,
  },
  itemCode: {
    color: "grey",
  },
  itemCount: {
    flexDirection: "row",
    borderRadius: 20,
    borderWidth: 1,
    borderColor: "#8E8E8E",
    paddingHorizontal: 15,
    paddingVertical: 3,
    alignItems: "center",
    justifyContent: "center",
    marginLeft: 15,
  },
  itemPiece: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    marginTop: 5,
  },
  itemPiecePlus: {
    marginRight: 15,
    fontSize: 20,
    color: "#8e8e8e",
  },
  itemPieceMinus: {
    marginLeft: 15,
    fontSize: 20,
    color: "#8e8e8e",
  },
  priceContainer: {
    flexDirection: "row",
    margin: 10,
    alignItems: "center",
  },
  itemNewPrice: {
    fontSize: 16,
    fontWeight: "700",
  },
  itemCancel: {
    marginTop: 5,
    borderWidth: 0,
    padding: 5,
    borderRadius: 8,
    textAlign: "center",
    color: "white",
    backgroundColor: "#b81414",
  },
  payInformations: {
    borderRadius: 20,
    backgroundColor: "white",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 8,
  },
  payInformationContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderBottomWidth: 1,
    padding: 24,
    borderBottomColor: "#e8e8e8",
  },
  payInformation: {
    color: "#8a8a8f",
    fontSize: 15,
    fontWeight: "700",
    marginTop: 5,
  },
  totalCart: {
    marginTop: 20,
    paddingLeft: 24,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  payTotal: {
    fontSize: 20,
    fontWeight: "700",
    marginHorizontal: 10,
  },
  payButton: {
    alignItems: "center",
  },
  moneyInformation: {
    fontSize: 16,
    fontWeight: "700",
  },
  inputVariable: {
    marginHorizontal: 15,
  },
  pickerContainer: {
    borderWidth: 1,
    borderColor: "#D6D6D6",
    borderRadius: 15,
    marginTop: 15,
  },
});
