import React, { useEffect, useState } from "react";
import { View, StyleSheet, FlatList, Text } from "react-native";
import { useRoute, useNavigation } from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";

function TransactionDetail() {
  const [authUser, setAuthUser] = useState(null);
  const [seller, setSeller] = useState(null);
  const [transaction, setTransaction] = useState(null);
  const navigation = useNavigation();
  const route = useRoute();

  useEffect(() => {
    const fetchAuthUserAndSeller = async () => {
      try {
        const authUser = JSON.parse(await AsyncStorage.getItem("authUser"));
        const seller = JSON.parse(await AsyncStorage.getItem("seller"));
        const transaction = JSON.parse(route.params.transaction);
        console.log("Transaction objesi :", transaction);
        if (!transaction) {
          console.log("Transaction of null");
        }
        setAuthUser(authUser);
        setSeller(seller);
        setTransaction(transaction);
      } catch (error) {
        console.log("Error", error);
      }
    };
    fetchAuthUserAndSeller();
  }, []);

  const returnInteger = (str) => {
    return parseInt(str);
  };

  const totalDiscount = (entry) => {
    return (
      parseFloat(entry.Discount_1) +
      parseFloat(entry.Discount_2) +
      parseFloat(entry.Discount_3)
    );
  };
  const totalTransactionItem = (entry) => {
    return parseFloat(entry.GrossTotal) - totalDiscount(entry);
  };
  return (
    <View style={{ flex: 1, backgroundColor: "#fff" }}>
      <View style={styles.transactionIdContainer}>
        {transaction && (
          <Text style={{ fontSize: 30, fontWeight: "500" }}>
            {transaction.id}
          </Text>
        )}
      </View>
      <View>
        <View style = {{borderBottomWidth:1, padding:5}}>
          <View style={styles.documentNoInformation}>
            <Text style = {{fontWeight:500}}>Evrak No:</Text>
            <Text>{transaction?.OrderCode}</Text>
          </View>
          <View style={styles.documentNoInformation}>
            <View style={{ flexDirection: "row" }}>
              <Text>Tarih:</Text>
              <Text style={{ marginLeft: 5 }}>{transaction?.CreateDate}</Text>
            </View>
          </View>
          <View style={styles.documentNoInformation}>
            <Text
              style={{
                color: transaction?.OutAmount !== "0" ? "red" : "green",
              }}
            >
              Yekün:{" "}
            </Text>
            <Text
              style={{
                color: transaction?.OutAmount !== "0" ? "red" : "green",
              }}
            >
              {transaction?.OutAmount !== "0"
                ? transaction?.OutAmount
                : transaction?.InAmount}{" "}
              {transaction?.Currency}
            </Text>
          </View>
        </View>

        <FlatList
          data={transaction?.Detail}
          keyExtractor={(item, index) =>
            index && index.id ? index.id.toString() : ""
          }
          renderItem={({ item }) => (
            <View style={styles.transactionDetailContainer}>
              <Text>{item?.StockCode}</Text>
              <Text>{item?.StockName}</Text>
              <Text>{item?.StockAmount}</Text>
              <Text>
                Tutar: {item.GrossTotal} {transaction?.Currency}
              </Text>
              <Text style={{ textAlign: "right" }}>
                ISK: {totalDiscount(item)} {transaction?.Currency}
                {"\n"}
                <Text style={{ fontWeight: "bold" }}>
                  {totalTransactionItem(item)} {transaction?.Currency}
                </Text>
                {"\n"}
                KDV: {item?.Stock_Tax} {transaction?.Currency}
              </Text>
            </View>
          )}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  transactionIdContainer: {
    alignItems: "center",
    marginTop: 15,
  },
  documentNoInformation: {
    flexDirection: "row",
    justifyContent: "space-between",
    margin: 5,
  },
  transactionDetailContainer:{
    margin:15,
    padding:15,
    backgroundColor:'white',
    borderRadius: 10,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 4,
  }
});

export default TransactionDetail;
