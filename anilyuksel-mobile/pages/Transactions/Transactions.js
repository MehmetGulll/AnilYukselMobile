import React, { useEffect, useState, useContext } from "react";
import { View, Text, StyleSheet, Alert, TouchableOpacity } from "react-native";
import Button from "../../components/Button";
import { useNavigation, useRoute } from "@react-navigation/native";
import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";

const apihost = "https://b2b.anilyukselboya.com.tr/json/api";

function Transactions() {
  const navigation = useNavigation();
  const route = useRoute();
  const [transactions, setTransactions] = useState([]);
  const [current, setCurrent] = useState(null);
  const [seller, setSeller] = useState(null);
  const [authUser, setAuthUser] = useState(null);
  const [balance, setBalance] = useState(null);
  const [totalAmount, setTotalAmount] = useState(0);
  const [show, setShow] = useState("all");
  const [totalDays, setTotalDays] = useState(0);
  const [totalMix, setTotalMix] = useState(0);
  const [averageDay, setAverageDay] = useState(0);

  useEffect(() => {
    const fetchAuthUserAndSellerAndCurrent = async () => {
      const authUser = JSON.parse(await AsyncStorage.getItem("authUser"));
      const seller = JSON.parse(await AsyncStorage.getItem("seller"));
      const current = route.params.current;

      setAuthUser(authUser);
      setSeller(seller);
      setCurrent(current);
    };
    fetchAuthUserAndSellerAndCurrent();
  }, []);

  useEffect(() => {
    if (authUser && seller && current) {
      refresh();
    }
  }, [authUser, seller, current]);

  const getMonth = (month) => {
    switch (month) {
      case "01":
        return "Ocak";
      case "02":
        return "Şubat";
      case "03":
        return "Mart";
      case "04":
        return "Nisan";
      case "05":
        return "Mayıs";
      case "06":
        return "Haziran";
      case "07":
        return "Temmuz";
      case "08":
        return "Ağustos";
      case "09":
        return "Eylül";
      case "10":
        return "Ekim";
      case "11":
        return "Kasım";
      case "12":
        return "Aralık";
      default:
        return "";
    }
  };

  const refresh = async () => {
    try {
      const response = await axios.get(
        `${apihost}/transactions/?token=${authUser.token}&seller_id=${seller.id}&current_id=${current.id}`
      );
      if (response.data.response_status) {
        setTransactions(response.data.response_data.transactions);
        setBalance(response.data.response_data.balance[0]);
        getAvarageDay();
      }
    } catch (error) {
      console.log("error", error);
    }
  };
  const getDetail = (transaction) => {
    if (transaction.Detail.length > 0) {
      transaction.Currency = current.Currency;
      navigation.navigate("TransactionDetail", {
        transaction: JSON.stringify(transaction),
      });
      // console.log(route.params.transaction);
      if (!transaction) {
        console.log("Transaction is undefined");
        return;
      }
    } else {
      console.log("Null geliyor");
    }
  };

  const getAvarageDay = () => {
    let total_amount = 0;
    let total_days = 0;
    let total_mix = 0;
    for (let item of transactions) {
      if (item.DueType == "Ödeme Bekleniyor " && item.Type == "Borç") {
        total_amount += parseFloat(item.InAmount);
        total_days += parseFloat(item.DueType);
        total_mix += parseFloat(item.DueDay) * parseFloat(item.InAmount);
      } else if (item.DueType == "Kısmi Ödendi" && item.Type == "Borç") {
        total_amount += parseFloat(item.InAmount);
        total_days += parseFloat(item.DueDay);
        total_mix += parseFloat(item.DueDay) * parseFloat(item.InAmount);
      }
    }
    setTotalAmount(total_amount);
    setTotalDays(total_days);
    setTotalMix(total_mix);

    const average =
      total_amount !== 0 ? Math.round(total_mix / total_amount) : 0;
    setAverageDay(average);
  };

  return (
    <View style={{ backgroundColor: "white", flex: 1 }}>
      <View style={styles.myCurrentContainer}>
        <Text style={{ fontSize: 18, fontWeight: 700 }}>
          {current && current.Code ? current.Code : "Loading...."}
        </Text>
        <Text style={{ fontSize: 14, fontWeight: 500, color: "#777E90" }}>
          Ortalama Vade : {averageDay} gün
        </Text>
        <View style={{ marginTop: 15 }}>
          <Button text={"Mail Olarak Gönder"} backgroundColor="#343434" color="#FFF" />
        </View>
      </View>
      <View style={styles.myCurrentCaseContainer}>
        <TouchableOpacity style={styles.myCurrentCase}>
          <Text style={{ color: show === "all" ? "grey" : "lightgrey" }}>
            {current ? current.Balance : "Loading..."}{" "}
            {current ? current.Currency : ""}
          </Text>
          <Text>Tümü</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.myCurrentCase}>
          <Text style={{ color: show === "OutAmount" ? "grey" : "lightgrey" }}>
            {balance?.OutAmount}
          </Text>
          <Text>Borç</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.myCurrentCase}>
          <Text style={{ color: show === "InAmount" ? "grey" : "lightgrey" }}>
            {balance?.InAmount}
          </Text>
          <Text>Alacak</Text>
        </TouchableOpacity>
      </View>
      <View>
        {transactions?.length === 0 && (
          <View style={{ alignItems: "center" }}>
            <Text style={{ fontSize: 15, fontWeight:'700' }}>Cari Hareket Bulunamadı!</Text>
          </View>
        )}
        {transactions.map(
          (transaction, index) =>
            transaction.Type === "Borç" && (
              <TouchableOpacity
                key={index}
                style={styles.transactionsContainer}
                onPress={() => getDetail(transaction)}
              >
                <View>
                  <Text>{transaction.CreateDate.split(".")[0]}</Text>
                  <Text>{getMonth(transaction.CreateDate.split(".")[1])}</Text>
                  <Text>{transaction.CreateDate.split(".")[2]}</Text>
                </View>
                <View>
                  <Text style={{ margin: 2 }}>#{transaction.OrderCode}</Text>
                  <Text style={{ margin: 2 }}>
                    Son Ödeme: {transaction.DueDate}
                  </Text>
                  <Text style={{ margin: 2 }}>{transaction.TransType}</Text>
                  <Text style={{ margin: 2, fontWeight: "700" }}>
                    {transaction.DueType}
                  </Text>
                </View>
                <View>
                  {transaction.InAmount !== "0" && (
                    <View style={{ alignItems: "center" }}>
                      <Text style={{ fontSize: 20, fontWeight: "700" }}>
                        {transaction.Type}
                      </Text>
                      <Text
                        style={{
                          marginTop: 15,
                          fontSize: 16,
                          fontWeight: "600",
                        }}
                      >
                        {transaction.InAmount} {current.Currency}
                      </Text>
                    </View>
                  )}
                  {transaction.OutAmount !== "0" && (
                    <View>
                      <Text>
                        {transaction.Type}
                        {"\n"}
                      </Text>
                      <Text>
                        {transaction.OutAmount} {current.Currency}
                      </Text>
                    </View>
                  )}
                </View>
              </TouchableOpacity>
            )
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  myCurrentContainer: {
    alignItems: "center",
    backgroundColor: "white",
    margin: 15,
    padding: 15,
    borderRadius: 10,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 4,
  },
  myCurrentCaseContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    margin: 15,
    padding: 5,
    borderRadius: 10,
    backgroundColor: "white",
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 4,
  },
  myCurrentCase: {
    alignItems: "center",
  },
  transactionsContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
    backgroundColor: "white",
    margin: 15,
    padding: 15,
    borderRadius: 10,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 4,
  },
});

export default Transactions;
