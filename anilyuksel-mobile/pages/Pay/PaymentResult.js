import React from "react";
import { View,Text} from 'react-native'
import Button from "../../components/Button";
import { useNavigation} from "@react-navigation/native";
function PaymentResult(){
    const navigation = useNavigation()
    return(
        <View>
            <Text>Ödeme Başarılı</Text>
            <View style ={{padding:15}}>
            <Button text={"Ana sayfaya dön"} onPress={()=>navigation.navigate("AnaSayfa")} />
            </View>
        </View>
    )
}
export default PaymentResult;