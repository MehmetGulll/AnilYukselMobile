import React, { useState, useEffect, useContext, useRef } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  Alert,
  Modal,
} from "react-native";
import { Picker } from "@react-native-picker/picker";
import {WebView} from 'react-native-webview'
import { useNavigation, useRoute } from "@react-navigation/native";
import DropDownHome from "../../components/DropDownHome";
import Input from "../../components/Input";
import Button from "../../components/Button";
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import { GlobalContext } from "../../Context/GlobalStates";
import PaymentConfirm from "./PaymentConfirm";
import * as Linking from 'expo-linking'
import * as WebBrowser from "expo-web-browser";

const apihost = "https://b2b.anilyukselboya.com.tr/json/api";
const api = "https://b2b.anilyukselboya.com.tr";

function Payment() {
  const navigation = useNavigation();
  const route = useRoute();

  const { selectedSellerName, setSelectedSellerName, currentType,b2cAddToCart } =
    useContext(GlobalContext);

  const [paymentMethodSelected, setPaymentMethodSelected] = useState("card");
  const [paymentId, setPaymentId] = useState(null);
  const [card, setCard] = useState({});
  const [auth, setAuth] = useState(null);
  const [seller, setSeller] = useState(null);
  const [currents, setCurrents] = useState();
  const [loading, setLoading] = useState(false);
  const [gateways, setGateways] = useState();
  const [gateway, setGateway] = useState(0);
  const [installments, setInstallments] = useState();
  const [pos, setPos] = useState(0);
  const [currentId, setCurrentId] = useState();
  const [amount, setAmount] = useState();
  const [installment, setInstallment] = useState(1);
  const [transactionCurrent, setTransactionCurrent] = useState();
  const [transactionId, setTransactionId] = useState();
  const [transactionAmount, setTransactionAmount] = useState();
  const [campaigns, setCampaigns] = useState();
  const [selectedCampaign, setSelectedCampaign] = useState(0);
  const [modalVisible, setModalVisible] = useState(false);
  const [cardNumber, setCardNumber] = useState();
  const [description, setDescription] = useState("");
  const [discountRate, setDiscountRate] = useState(0)
  const [discountAmount, setDiscountAmount] = useState(0);
  const [maturity_date, setMaturityDate] = useState(new Date().toISOString().split("T")[0])
  const [maturity_date_time, setMaturityDateTime] = useState(1);
  const [shipping, setShipping] = useState(1);
  const [adress, setAdress] = useState("");
  const [webViewModal, setWebViewModal] = useState(false);
  const [url, setUrl] = useState(null);
  const webviewRef = useRef(null);
  const total = route.params?.total ? route.params.total : 0;
  const ordernote = route.params?.ordernote;
  const discountR = route.params?.additionalDiscountRate;
  const discountA = route.params?.additionalDiscountAmount;
  const maturitydate = route.params?.maturity_date;
  const maturitydays = route.params?.maturity_days;
  const shippingC = route.params?.shipping;
  const address = route.params?.addres;


  useEffect(() => {
    const fetchAuthandSeller = async () => {
      const auth = JSON.parse(await AsyncStorage.getItem("authUser"));
      setAuth(auth);
      if (auth) {
        const seller = JSON.parse(await AsyncStorage.getItem("seller"));
        setSeller(seller);
      }
      setLoading(false);
      if (route.params?.current) {
        setTransactionCurrent(route.params.current);
        setCurrentId(route.params.current);
      }
      if (route.params?.amount) {
        setTransactionAmount(route.params.amount);
        setAmount(route.params.amount);
      }
      if (route.params?.transaction) {
        setTransactionId(route.params.transaction);
      }
    };
    fetchAuthandSeller();
  }, []);

  useEffect(() => {
    const fetchData = async () => {
      if (auth && seller) {
        await refresh();
        if(currentType ==='b2c'){
          setAmount(total);
          setDescription(ordernote);
          setMaturityDate(maturitydate)
          setMaturityDateTime(maturitydays);
          setShipping(shippingC);
          setAdress(address);
          setDiscountAmount(discountAmount);
          setDiscountRate(discountRate);
          console.log("Tokenimiz", auth.token)
          console.log("Sellerid", seller.id)
          console.log("sellerPrice type:", seller.PriceType_id);
          console.log("Amountumuz:",total);
          console.log("Notumuz", description);
          console.log("Maturitydatemiz:", maturity_date);
          console.log("maturity date timemiz",maturity_date_time);
          console.log("Shipiingimiz",shipping);
          console.log("adresimiz",address);
          console.log("discountRatemiz", discountRate);
          console.log("discountAmount", discountAmount);
        }
      }
    };
    fetchData();
  }, [auth, seller]);

  useEffect(() => {
    if (amount) {
      getInstallments();
    }
  }, [amount]);

  const refresh = async () => {
    if (auth) {
      const responseCurrents = await axios.get(
        `${apihost}/currents/?token=${auth.token}&seller_id=${seller.id}`
      );
      if (responseCurrents.data.response_status) {
        setCurrents(responseCurrents.data.response_data);
      }
      const responseGateways = await axios.get(
        `${apihost}/gateways/?token=${auth.token}&seller_id=${seller.id}`
      );
      if (responseGateways.data.response_status) {
        setGateways(responseGateways.data.response_data);
      }
    } else {
      console.log("sayfayı yenile");
    }
  };

  const makeStripePayment = async () => {
    if(currentType==="b2c"){
      setAmount(total);
      console.log(total)
      console.log("currentType aldım");
    }
    let params = {
      gateway_id: gateway,
      token: auth.token,
      current_id: currentId,
      name: card.name,
      expm: card.exp_month,
      expy: card.exp_year,
      ccno: card.ccno,
      amount: amount,
      cvv: card.cvc,
      installment_id: installment,
      User_id: auth.userid,
      campaignkoicode: selectedCampaign ? 2 : 0,
    };
    console.log(params);
    if (transactionId) {
      params["transaction_id"] = transactionId;
    }
    setModalVisible(!modalVisible);
    await doPayment(params);
  };

  const getInstallments = async () => {
    try {
      console.log(auth.token, amount, card.ccno);
      const response = await axios.get(
        `${apihost}/binlist_check/?token=${auth.token}&module=paynow&amount=${amount}&binNumber=${card.ccno}`
      );
      setInstallments(response.data.Installments);
      setCampaigns(response.data.campaignMessages);
    } catch (error) {
      console.log("eror", error);
    }
  };
  const createInAppBrowser = async (payment_id) => {
    console.log("create içindeyim");
    console.log("payment idimiz", payment_id);
    console.log("discountRate:",discountRate);
    console.log("discountAmount",discountAmount);
    const url = `${api}/paynow/summary/${auth.token}/${payment_id}`;
    setUrl(url);
    setPaymentId(payment_id);
    setWebViewModal(!webViewModal); 

  };
  const onNavigationStateChange = async(navState) => {
    if (navState.url.includes('/payment-success')) {
      if(currentType!=='b2c'){
        webviewRef.current.stopLoading();
        Alert.alert("Siparişiniz Oluşturuldu","",[
          {text:'Tamam', onPress : ()=>navigation.navigate('Ürün Ara'), style:'cancel'}
        ])
        console.log('Payment Successful');
      }
      else{
        console.log("Burası b2c için");
        try {
          const response = await axios.get(`${apihost}/complete_order/?token=${auth.token}&seller_id=${seller.id}&sellerPriceType=${seller.PriceType_id}&ordernote=${description}&paymentmethod=1&additionalDiscountRate=${discountRate}&additionalDiscountAmount=${discountAmount}&maturity_date=${maturity_date}&maturity_days=${maturity_date_time}&shipping=${shipping}&addres=${adress}`)
          console.log("complete response:", response);
          if(response.data.response_status){
            Alert.alert("Siparişiniz Oluşturuldu","",[
              {text:'Tamam', onPress:()=>navigation.navigate('Ürün Ara'), style:'cancel'}
            ])
            console.log("İşlem tamamlandı")
          }
          else{
            console.log("Sipariş sırasında hata oluştu.");
          }
        } catch (error) {
          console.log("Erorororor",error)
        }
      }
    setWebViewModal(!webViewModal); 
    } else if (navState.url.includes('/payment-fail')) {
      console.log('Payment Failed');
      const responsePayment = await axios.get(`${apihost}/get-transaction-status/?token=${auth.token}&current_id=${currentId}`)
      Alert.alert("Hata Oluştu, Ödeme Alınamadı",responsePayment.data.response_message,[
        {text:'Tamam', style:'cancel'}
      ] )
      webviewRef.current.stopLoading(); 
      setWebViewModal(!webViewModal); 
    }
  };


  const doPayment = async (params) => {
    if (pos == 0 || pos == "0") {
      console.log(params.token);
      console.log(params.campaignkoicode);
      const response1 = await axios.get(
        `${apihost}/prepare-payment/?token=${params.token}&gateway_id=${params.gateway_id}&current_id=${params.current_id}&name=${params.name}&expm=${params.expm}&expy=${params.expy}&ccno=${params.ccno}&amount=${params.amount}&cvv=${params.cvv}&installment_id=${params.installment_id}&User_id=${params.User_id}&campaignkoicode=${params.campaignkoicode}`
      );
      console.log("pos seçili");
      console.log("response_message:", response1.data.response_message);
      console.log("response_code:", response1.data.response_code);
      if (
        response1.data.response_message == "Başarılı." ||
        response1.data.response_code
      ) {
        console.log("açıyorum başarılı");
        createInAppBrowser(response1.data.response_data.payment_id);
        console.log("açıyorum");
      }
    } else if (pos == 1 || pos == "1") {
      const response2 = await axios.get(`${apihost}/paynow`);
      if (response2.data.response_status == true) {
        navigation.navigate("PaymentResult", { response: true });
      }
    } else {
      console.log("hata");
    }
  };

  return (
    <ScrollView style = {{flex:1, backgroundColor:'#fff'}}>
      <View style={styles.payContainer}>
        {currentType !== "b2c" && <DropDownHome text={selectedSellerName} />}

        <View style={styles.creditCardContainer}>
          <View style={styles.cardNumberContainer}>
            <Text style={styles.cardNumber}>
              {cardNumber ? cardNumber.replace(/(\d{4})/g, "$1 ").trim() : ""}
            </Text>
          </View>
          {/* <View style={styles.validityDateContainer}>
            <Text style={styles.validityDateText}>Geçerlilik Tarihi</Text>
            <Text style={styles.validityDate}>
              {selectedMonth}/{selectedYear.slice(-2)}
            </Text>
          </View> */}
        </View>
        {paymentId && (
           <Modal visible={webViewModal} transparent={true} style = {{backgroundColor:'red', width:'100%', height:'100%'}}>
           <WebView
             ref={webviewRef}
             source={{ uri: url }}
             onNavigationStateChange={onNavigationStateChange}
           />
         </Modal>
        )}
        <View style={styles.cardInformations}>
         
            <Picker
              selectedValue={currentId}
              onValueChange={(itemValue) => setCurrentId(itemValue)}
            >
              {currents &&
                currents.map((current) => (
                  <Picker.Item
                    key={current.id}
                    label={`${current?.Code} (${current?.Balance} ${current?.Currency})`}
                    value={current.id}
                  />
                ))}
            </Picker>
          

          <Input
            placeholder={"Kart Numarınızı Giriniz"}
            keyboardType="numeric"
            maxLength={16}
            value={card.ccno}
            onChangeText={(text) =>
              setCard({ ...card, ccno: text }, setCardNumber(text))
            }
          />
          <Input
            placeholder={"Kart Üzerindeki Ad Soyad"}
            value={card.name}
            onChangeText={(text) => setCard({ ...card, name: text })}
          />
          <View style={styles.cardDateInformations}>
            <Input
              placeholder={"MM"}
              value={card.exp_month}
              onChangeText={(text) => setCard({ ...card, exp_month: text })}
            />
            <Input
              placeholder={"YY"}
              value={card.exp_year}
              onChangeText={(text) => setCard({ ...card, exp_year: text })}
            />
          </View>
          <Input
            placeholder={"CVC"}
            value={card.cvc}
            onChangeText={(text) => setCard({ ...card, cvc: text })}
            keyboardType={"numeric"}
          />
          {installments && (
            <Picker
              selectedValue={installment}
              onValueChange={(itemValue) => setInstallment(itemValue)}
            >
              {installments.map((ins) => (
                <Picker.Item
                  key={ins.id}
                  label={`${ins.Count}${
                    ins.ExtraOnstallment ? ` + ${ins?.ExtraOnstallment}` : ""
                  }`}
                  value={ins?.id}
                />
              ))}
            </Picker>
          )}
          {campaigns && campaigns !== "" && (
            <Picker
              selectedValue={selectedCampaign}
              onValueChange={(itemValue) => setSelectedCampaign(itemValue)}
            >
              {campaigns.map((cmp) => (
                <Picker.Item key={cmp} label={cmp} value={cmp} />
              ))}
            </Picker>
          )}
          <Picker
            selectedValue={pos}
            onValueChange={(itemValue) => setPos(itemValue)}
          >
            <Picker.Item label="3D Pos" value="0" />
            <Picker.Item label="Normal Pos" value="1" />
          </Picker>
          {currentType !=='b2c' ? ( <Input
            placeholder={"Tutar Giriniz"}
            keyboardType="numeric"
            onChangeText={(text) => {
              setAmount(text);
              if (text) getInstallments();
            }}
          /> ) : (<View style = {{marginTop:15, flexDirection:'row'}}>
            <Text style = {{fontSize:25}}>Ödenecek Tutar:</Text>
            <Text style = {{fontSize:25, fontWeight:'500'}}> {new Intl.NumberFormat("tr-TR", {
                  style: "currency",
                  currency: "TRY",
                }).format(Number(total))}</Text>
          </View>)}
          
          <Button text={"Ödeme Yap"} onPress={() => makeStripePayment()} />
        </View>
      </View>
    </ScrollView>
  );
}
export default Payment;

const styles = StyleSheet.create({
  payContainer: {
    backgroundColor: "white",
    flex: 1,
  },
  creditCardContainer: {
    backgroundColor: "#09A3C5",
    borderRadius: 10,
    marginTop: 15,
    marginHorizontal: 23,
  },
  cardNumberContainer: {
    marginTop: 122,
    padding: 15,
  },
  cardNumber: {
    color: "#fff",
    fontSize: 20,
    marginBottom: 20,
    marginLeft: 16,
    letterSpacing: 3,
  },
  validityDateContainer: {
    alignItems: "flex-end",
    marginRight: 16,
    marginBottom: 15,
  },
  validityDate: {
    color: "#fff",
    fontWeight: "600",
  },
  validityDateText: {
    color: "#fff",
    fontWeight: "800",
    fontSize: 15,
  },
  cardInformations: {
    margin: 15,
  },
  cardDateInformations: {
    flexDirection: "row",
  },
  paymentVarieties: {
    marginBottom: 10,
    alignItems: "center",
    borderColor: "#FBFBFB",
    borderRadius: 20,
    backgroundColor: "white",
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 4,
  },
  banksContainer: {
    justifyContent: "center",
  },
  bankContainer: {
    alignItems: "center",
  },
  bankLogo: {
    width: "80%",
    height: 100,
  },
  paymentAdvance: {
    padding: 15,
  },
  installmentContainer: {
    flexDirection: "row",
    padding: 15,
    borderWidth: 1,
    borderColor: "#FBFBFB",
  },
});
