import React,{useState, useEffect} from 'react'
import {View,Text,StyleSheet} from 'react-native';
import Button from '../../components/Button';
import { useNavigation, useRoute } from '@react-navigation/native';

function PaymentConfirm(){
    const navigation = useNavigation();
    const route = useRoute();
    const [paymentData, setPaymentData] = useState(null);
    const [currents, setCurrents] = useState(null);
    const [gateways, setGateways] = useState(null);
    const [installments, setInstallments] = useState(null);
    const [pos, setPos] = useState(null);
    const [campaign, setCampaign] = useState(null)


    useEffect(()=>{
        console.log('PaymentConfirmPage Loaded');
        if(route.params?.paymentInfo){
            setPaymentData(route.params?.paymentInfo);
        }
        if(route.params?.currents){
            setCurrents(route.params?.currents);
        }
        if(route.params?.gateways){
            setGateways(route.params?.gateways);
        }
        if(route.params?.installments){
            setInstallments(route.params?.installments);
        }
        if(route.params?.pos){
            setPos(route.params?.pos);
        }
        if(route.params?.campaign){
            setCampaign(route.params?.campaigns);
        }
    })

    const closeModal = (acception)=>{
        navigation.goBack();
    }

    // useEffect(()=>{
    //     setPaymentData(JSON.parse(route.params.paymentInfo));
    //     setCurrents(JSON.parse(route.params.currents))
    //     setGateways(JSON.parse(route.params.gateways));
    //     setInstallments(JSON.parse(route.params.installments));
    //     setPos(route.params.pos);
    //     setCampaign(route.params.campaign);
    // },[])

    // const closeModal = (acception) =>{
    //     let data = {'response': acception};
    //     navigation.goBack();
    //     if(data.response){
    //         doPayment(params);
    //     }
    // }
    return(
        <View style = {{backgroundColor:'#FFF', flex:1}}>
            {/* <Text style={styles.textStyle}>{currents?.Code + '('+ (currents?.Balance) + ' ' + currents?.Currency + ')'}</Text> */}
            <Text style={styles.textStyle}>Tutar: {paymentData?.amount} TL</Text>
            <Text style={styles.textStyle}>Ad Soyad : {paymentData?.name}</Text>
            <Text style={styles.textStyle}>Kart : {gateways?.Name}</Text>
            <Text style={styles.textStyle}>Kart Numarası : {paymentData?.ccno}</Text>
            <Text style={styles.textStyle}>Taksit:{installments?.Label}</Text>
            <Button text={"İptal"} onPress={()=> closeModal(false)} marginHorizontal={50} />
            <Button text={"Onayla"} onPress={()=> closeModal(true)} marginHorizontal={50} />
        </View>
    )
}

const styles = StyleSheet.create({
    textStyle:{
        fontSize:20
    }
})

export default PaymentConfirm;