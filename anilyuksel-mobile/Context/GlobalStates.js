import React, { createContext, useState } from "react";

export const GlobalContext = createContext();

export const GlobalProvider = ({ children }) => {
  const [basket, setBasket] = useState([]);
  const [b2cAddToCart, setB2CAddToCart] = useState([]);
  const [addToCart, setAddToCart] = useState([]);
  const [basketItem, setBasketItem] = useState();
  const [allProducts, setAllProducts] = useState([]);
  const [selectedSellerName, setSelectedSellerName] = useState("Cari Seçiniz");
  const [currents, setCurrents] = useState([]);
  const [currentType, setCurrentType] = useState("");
  const [selectedCategoryId, setSelectedCategoryId] = useState(null);
  const [product, setProduct] = useState([
    {
      id: "1",
      title: "Smart 3000",
      image: require("../assets/smart3000.jpg"),
      oldPrice: 49900,
      discount: 25,
      productCode: "206-B-14",
      stock: 5,
      iskonto: 15,
    },
    {
      id: "2",
      title: "Tempo 3000",
      image: require("../assets/tempo3000.jpg"),
      oldPrice: 38915,
      discount: 25,
      productCode: "206-D-47",
      stock: 2,
      iskonto: 15,
    },
    {
      id: "3",
      title: "Kobra 5000",
      image: require("../assets/kobra500.jpg"),
      oldPrice: 40000,
      discount: 25,
      productCode: "206-E-100",
      stock: 0,
      iskonto: 15,
    },
  ]);

  return (
    <GlobalContext.Provider
      value={{
        basket,
        setBasket,
        product,
        setProduct,
        basketItem,
        setBasketItem,
        allProducts,
        setAllProducts,
        currents,
        setCurrents,
        selectedCategoryId,
        setSelectedCategoryId,
        selectedSellerName,
        setSelectedSellerName,
        addToCart,
        setAddToCart,
        currentType,
        setCurrentType,
        b2cAddToCart,
        setB2CAddToCart
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};
