import React, { useState, useEffect, useRef, useContext } from "react";
import {
  View,
  Text,
  Animated,
  TouchableOpacity,
  TextInput,
  Modal,
  FlatList,
} from "react-native";
import Button from "./Button";
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import { GlobalContext } from "../Context/GlobalStates";

const apihost = "https://b2b.anilyukselboya.com.tr/json/api";

const DropDownHome = ({ text }) => {
  const { selectedSellerName, setSelectedSellerName } =
    useContext(GlobalContext);
  const [seller, setSeller] = useState(null);
  const [sellers, setSellers] = useState([]);
  const [page, setPage] = useState(0);
  const [searchTerm, setSearchTerm] = useState("");
  const [isSearchVisible, setIsSearchVisible] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [displaySellers, setDisplaySellers] = useState([]);

  const sellerChange = async (value) => {
    const selectedSeller = displaySellers.find((seller) => seller.id === value);
    if (selectedSeller) {
      await AsyncStorage.setItem("seller", JSON.stringify(selectedSeller));
      setSeller(selectedSeller);
      setSelectedSellerName(selectedSeller.Name);
      setModalVisible(false);
   
    } else {
      console.log("Error");
    }
  };

  useEffect(()=>{
    const fetchSeller = async()=>{
      const nowSeller = JSON.parse(await AsyncStorage.getItem('seller'))
      if(nowSeller){
        const { Name } = nowSeller
        setSelectedSellerName(Name);
      }
    }
    fetchSeller()
  },[])

  const getSellers = async (page, numberofpage) => {
    const authUser = JSON.parse(await AsyncStorage.getItem("authUser"));
    const response = await axios.get(
      `${apihost}/sellers/?token=${authUser.token}&start=${page}&end=${
        page + numberofpage
      }`
    );
    const responseSellers = response.data;
    setSellers((prevSellers) => [
      ...prevSellers,
      ...responseSellers.response_data,
    ]);
    setPage(page + numberofpage);
  };

  useEffect(() => {
    getSellers(page, 20);
  }, [page]);

  useEffect(() => {
    setSelectedSellerName(selectedSellerName);
  }, [selectedSellerName]);

  useEffect(() => {
    setDisplaySellers(
      sellers.filter((seller) =>
        seller.Name.toLowerCase().includes(searchTerm.toLowerCase())
      )
    );
  }, [searchTerm, sellers]);

  return (
    <View style={{ justifyContent: "center", alignItems: "center" }}>
      <View style ={{width:'100%'}}>
        <View style ={{marginHorizontal:15}}>
        <Button
          style={{ color: "#777E90"}}
          color={"black"}
          text={selectedSellerName}
          onPress={() => setModalVisible(true)}
        />
        </View>
      </View>
      <Modal
        animationType="slide"
        transparent={false}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
        }}
      >
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <TextInput
            placeholder="Cari Arayınız..."
            style={{
              height: 40,
              borderColor: "#777E90",
              borderWidth: 1,
              marginTop: 15,
              paddingHorizontal: 80,
              borderRadius: 50,
            }}
            value={searchTerm}
            onChangeText={(text) => setSearchTerm(text)}
          />

          <FlatList
            data={displaySellers}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item }) => (
              <TouchableOpacity
                onPress={() => sellerChange(item.id)}
                style={{
                  marginHorizontal: 15,
                  marginTop: 25,
                  borderBottomWidth: 1,
                  borderColor: "#777E90",
                }}
              >
                <Text>{item.Name}</Text>
              </TouchableOpacity>
            )}
          />

          <TouchableOpacity onPress={() => setModalVisible(false)} style = {{marginBottom:20 , alignItems:'center'}}>
            <Text style = {{fontSize:25}}>Kapat</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    </View>
  );
};

export default DropDownHome;
