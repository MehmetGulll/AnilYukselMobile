import { TouchableOpacity, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/AntDesign";
import { useNavigation } from "@react-navigation/native";

function BackIcon() {
    const navigation = useNavigation();
  return (
    <TouchableOpacity style = {styles.backIcon} onPress={()=>navigation.goBack()}>
      <Icon name="arrowleft" size={30} color={"#777E90"} />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  backIcon: {
    marginTop: 50,
    flexDirection: "row",
    marginLeft: 20,
  },
});

export default BackIcon;
