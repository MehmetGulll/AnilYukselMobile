import React, { useContext,useState } from "react";
import { View, Text, TouchableOpacity, StyleSheet, Image } from "react-native";
import {
  DrawerContentScrollView,
  DrawerItem,
  createDrawerNavigator,
} from "@react-navigation/drawer";
import { GlobalContext } from "../Context/GlobalStates";
import Logo from "./Logo";
import Icon from "react-native-vector-icons/AntDesign";
import Communication from "../pages/Login/Communication";
import { useNavigation } from "@react-navigation/native";
import StackRender from "../pages/StackRender";
import Basket from "../pages/Basket/Basket";
import Payment from "../pages/Pay/Payment";
import AsyncStorage from "@react-native-async-storage/async-storage";
import BottomTab from "./BottomTab";


const Drawer = createDrawerNavigator();

function CustomDrawerContent(props) {
  const {currentType} = useContext(GlobalContext);
  const [authUser, setAuthuser] = useState(null);
  const [seller,setSeller] = useState(null);
  const [sellers, setSellers] = useState(null);
  const navigation = useNavigation();




  const logOut = async()=>{
    await AsyncStorage.removeItem('authUser')
    await AsyncStorage.removeItem('seller');
    await navigation.navigate('SignIn');
  }
  return (
    <View style={{ flex: 1 }}>
      <DrawerContentScrollView {...props} style={{ backgroundColor: "#fff" }}>
        <View style={styles.avatarContainer}>
          <Image
            source={{
              uri: "https://img.freepik.com/free-vector/businessman-character-avatar-isolated_24877-60111.jpg?w=2000",
            }}
            style={styles.personAvatar}
          />
          <Text style={styles.avatarName}></Text>
        </View>
        <DrawerItem
          icon={() => <Icon name="home" size={24} color={"grey"} />}
          label={() => <Text style={styles.labelStyle}>Ana Sayfa</Text>}
          onPress={() => props.navigation.navigate("Ana Sayfa")}
          
        />
        <DrawerItem
          icon={() => <Icon name="inbox" size={24} color={"grey"} />}
          label={() => <Text style={styles.labelStyle}>Ürünler</Text>}
          onPress={() => props.navigation.navigate("Ürün Ara")}
          
        />
        <DrawerItem
          icon={() => <Icon name="shoppingcart" size={24} color={"grey"} />}
          label={() => <Text style={styles.labelStyle}>Sepetim</Text>}
          onPress={() => props.navigation.navigate("Sepet")}
          
        />
        {currentType !=="b2c" && <DrawerItem
          icon={() => <Icon name="wallet" size={24} color={"grey"} />}
          label={() => <Text style={styles.labelStyle}>Cari Hesabım</Text>}
          onPress={() => props.navigation.navigate("Currents")}
          
        /> }
        <DrawerItem
          icon={() => <Icon name="creditcard" size={24} color={"grey"} />}
          label={() => <Text style={styles.labelStyle}>Ödeme Yap</Text>}
          onPress={() => props.navigation.navigate("Ödeme Yap")}
          
        />

        <DrawerItem
          icon={() => <Icon name="phone" size={24} color={"#777E90"} />}
          label={() => <Text style={styles.labelStyle}>İletişim</Text>}
          onPress={() => props.navigation.navigate("Iletişim")}
          
        />
        <DrawerItem
          label={()=><Text style = {{...styles.labelStyle, borderWidth:1, padding:8, borderRadius:50, borderColor:'#777E90', marginTop:20,}}>Çıkış Yap</Text>}
          onPress={logOut}
        />
      </DrawerContentScrollView>
    </View>
  );
}

function SideBar() {
  const navigation = useNavigation();
  const { currentType, b2cAddToCart } = useContext(GlobalContext);
  return (
    <Drawer.Navigator
      drawerContent={(props) => <CustomDrawerContent {...props} />}
    >
      <Drawer.Screen
        name="AnaSayfa"
        component={StackRender}
        options={{
          headerTitle: () => (
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                transform: [{ scale: 0.3 }],
              }}
            >
              <Logo />
            </View>
          ),
          headerRight: () => {
            return (
              <TouchableOpacity onPress={() => navigation.navigate("Sepet")}>
                <Icon
                  name="shoppingcart"
                  size={24}
                  style={{ marginRight: 10 }}
                />
                {currentType ==='b2c' && b2cAddToCart.length > 0 && (
                  <View style={styles.notificationContainer}>
                    <Text style={styles.notificationText}>{b2cAddToCart.length}</Text>
                  </View>
                )}
              </TouchableOpacity>
            );
          },
        }}
      />
      <Drawer.Screen name="İletişim" component={Communication} />
      <Drawer.Screen name="Sepetim" component={Basket} />
      <Drawer.Screen name="OdemeYap" component={Payment} />
    </Drawer.Navigator>
  );
}

export default SideBar;

const styles = StyleSheet.create({
  avatarContainer: {
    alignItems: "center",
    backgroundColor: "#fff",
    flexDirection: "row",
    justifyContent:'center',
    padding:50
  },
  personAvatar: {
    width: 52,
    height: 52,
    borderRadius: 100,
  },
  avatarName: {
    fontSize: 16,
    fontWeight: "700",
    marginLeft:17
  },
  labelStyle: {
    color: "#777E90",
    textAlign: "center",
    fontSize:16,
    fontWeight: "bold",
  },
  notificationContainer: {
    backgroundColor: "#b81414",
    alignItems: "center",
    borderRadius: 20,
    padding: 2,
    position: "absolute",
    left: 18,
    bottom: 10,
  },
  notificationText: {
    fontSize: 12,
    color: "white",
  },
});
