import React from "react";
import { View, StyleSheet, Text, TouchableOpacity } from "react-native";

function Button({ text,paddingHorizontal,disabled, onPress, marginBottom,backgroundColor,color, marginHorizontal }) {
  const displayText = text.length > 20 ? `${text.slice(0,20)}...` : text;
  return (
    <TouchableOpacity disabled = {disabled} onPress={onPress} >
      <View style={[styles.buttonContainer, {paddingHorizontal:paddingHorizontal, marginBottom:marginBottom, backgroundColor:backgroundColor, marginHorizontal: marginHorizontal}, disabled && styles.disabledButton]} >
        <Text style={[styles.buttonText, disabled && styles.disabledText,{color:color}]} >{displayText}</Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  buttonContainer: {
    padding: 20,
    marginTop:15,
    borderRadius: 50,
    backgroundColor:'#343434',
    borderWidth: 1,
    alignItems: "center",
  },
  disabledButton:{
    borderColor:"#e9e9e9"
  },
  buttonText: {
    color: "#fff",
    fontSize:16,
    paddingHorizontal:50
  },
  disabledText:{
    color : "#e9e9e9"
  }
});

export default Button;
