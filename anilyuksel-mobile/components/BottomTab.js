import React, { useContext } from "react";
import { View, StyleSheet, Text } from "react-native";
import "react-native-gesture-handler";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { GlobalContext } from "../Context/GlobalStates";
import Home from "../pages/Home/Home";
import Icon from "react-native-vector-icons/AntDesign";
import AllProducts from "../pages/Products/AllProducts";
import Basket from "../pages/Basket/Basket";
import Payment from "../pages/Pay/Payment";

const Tab = createBottomTabNavigator();

function BottomTab() {
  const { allProducts, b2cAddToCart,currentType } = useContext(GlobalContext);
  return (
    <Tab.Navigator screenOptions={{ headerShown: false }}>
      <Tab.Screen
        name="Ana Sayfa"
        component={Home}
        options={{
          tabBarIcon: () => <Icon name="home" size={24} color={"black"} />,
        }}
      />
      <Tab.Screen
        name="Ürün Ara"
        component={AllProducts}
        options={{
          tabBarIcon: () => <Icon name="search1" size={24} color="black" />,
        }}
      />
      <Tab.Screen
        name="Sepet"
        component={Basket}
        options={{
          tabBarIcon: () => (
            <>
              <Icon name="shoppingcart" size={24} color="black" />
              {currentType === 'b2c' && b2cAddToCart.length > 0 && (
                <View style={styles.notificationContainer}>
                  <Text style={styles.notificationText}>{b2cAddToCart.length}</Text>
                </View>
              )}
            </>
          ),
        }}
      />
      <Tab.Screen
        name="Ödeme Yap"
        component={Payment}
        options={{
          tabBarIcon: () => <Icon name="creditcard" size={24} color="black" />,
        }}
      />
    </Tab.Navigator>
  );
}

const styles = StyleSheet.create({
  notificationContainer: {
    backgroundColor: "#b81414",
    alignItems: "center",
    borderRadius: 20,
    padding: 2,
    position: "absolute",
    left: 55,
    bottom: 12,
  },
  notificationText: {
    fontSize: 12,
    color: "white",
  },
});

export default BottomTab;
