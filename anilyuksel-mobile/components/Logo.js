import React from 'react'
import {View,Image,StyleSheet} from 'react-native'

function Logo(){
    return(
        <View style ={styles.logoContainer}>
            <Image style={styles.stmaxLogo} source={require("../assets/logo.png")} />
        </View>
    )
}
export default Logo;

const styles = StyleSheet.create({

    stmaxLogo: {
        alignItems: "center",
        justifyContent: "center",
        transform : [{scale:0.3}]
      },
})