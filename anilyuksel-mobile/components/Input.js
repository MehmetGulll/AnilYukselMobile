import React from "react";
import { TextInput, View, StyleSheet } from "react-native";
import { globalStyles } from "../theme/GlobalStyles";
function Input({
  placeholder,
  onChangeText,
  onFocus,
  onBlur,
  secureTextEntry,
  textAlign,
  keyboardType,
  maxLength,
  value
}) {
  return (
    <View>
      <TextInput
        style={styles.inputInformation}
        placeholder={placeholder}
        onChangeText={onChangeText}
        onFocus={onFocus}
        onBlur={onBlur}
        secureTextEntry={secureTextEntry}
        textAlign={textAlign}
        keyboardType={keyboardType}
        maxLength={maxLength}
        value={value}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  inputInformation: {
    borderWidth: 0,
    borderBottomWidth: 1,
    borderColor: globalStyles.colors.borderBottomColor,
    width: "100%",
    marginTop: 10,
    padding:10
  },
});

export default Input;
