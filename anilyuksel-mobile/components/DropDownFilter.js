import React, { useState, useEffect,useContext } from 'react'
import {View,Modal,FlatList,TouchableOpacity,Text,TextInput} from 'react-native'
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import { GlobalContext } from "../Context/GlobalStates";

const apihost = "https://b2b.anilyukselboya.com.tr/json/api";


function DropDownFilter(){
  const [categoryId, setCategoryId] = useState(null);
  const [categories,setCategories] = useState([]);
  const [parentCategory, setParentCategory] = useState(null);
  const [title,setTitle] = useState('');
  const [authUser,setAuthUser] = useState(null)
  const [modalVisible, setModalVisible] = useState(false);
  const [displayCategories,setDisplayCategories] = useState([])
  const [searchTerm,setSearchTerm] = useState('')
  const { setSelectedCategoryId, selectedCategoryId } = useContext(GlobalContext);

  useEffect(()=> {
    const fetchAuthUser = async()=>{
      const authUser = JSON.parse(await AsyncStorage.getItem('authUser'))
      setAuthUser(authUser)
    };
    fetchAuthUser()
  },[])

  useEffect(()=>{
    const fetchCategories = async() =>{
      try {
        if(categoryId == null){
          setTitle('Kategoriler');
          const authUser = JSON.parse(await AsyncStorage.getItem('authUser'))
          const response = await axios.get(`${apihost}/categories/?token=${authUser.token}`);
          if(response.data.response_status){
            setCategories(response.data.response_data.categories);
          }

        }else{
          const response = await axios.get(`${apihost}/categories/?parent=${categoryId}`)
          if(response.data.response_status){
            setCategories(response.data.response_data.categories)
            setParentCategory(response.data.response_data.parentCategory);
            setTitle(parentCategory!=null ? parentCategory.Name: 'Kategoriler')
          }
        }
      } catch (error) {
        console.log("Error",error)
      }
    }
    fetchCategories()
  },[])

  useEffect(()=>{
    if(searchTerm ===''){
      setDisplayCategories(categories)
    }else{
      setDisplayCategories(categories.filter(category => category.Name.toLowerCase().includes(searchTerm.toLowerCase())))
    }
  },[searchTerm,categories])

  return(
    <View style={{ justifyContent: "center", alignItems: "center" }}>
      <TouchableOpacity
        onPress={() => setModalVisible(true)}
        style={{
          marginTop:40,
          paddingHorizontal: 65,
          borderRadius: 50,
          paddingVertical: 15,
          borderWidth: 1,
          borderColor: "#777E90",
        }}
      >
        <Text style={{ color: "#777E90" }}>Kategori Seçiniz</Text>
      </TouchableOpacity>

      <Modal
        animationType="slide"
        transparent={false}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
        }}
      >
        <View
          style={{ flex: 1, justifyContent: "center" }}
        >
          <View style = {{alignItems:'center'}}>
          <TextInput
            placeholder="Kategori Arayınız..."
            style={{
              height: 40,
              borderColor: "#777E90",
              borderWidth: 1,
              marginTop: 15,
              paddingHorizontal: 80,
              borderRadius: 50,
            }}
            onChangeText={text => setSearchTerm(text)}
          />
          </View>
         

          <FlatList
            data={displayCategories}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item }) => (
              <TouchableOpacity
                style={{
                  marginHorizontal: 15,
                  marginTop: 25,
                  borderBottomWidth: 1,
                  borderColor: "#777E90",
                }}
                onPress={()=>{
                  setSelectedCategoryId(item.id)
                  setModalVisible(false)
                }}
              >
                {console.log(selectedCategoryId)}
                <Text>{item.Name}</Text>
              </TouchableOpacity>
            )}
          />

          <TouchableOpacity onPress={() => setModalVisible(false)} style = {{alignItems:'center', marginBottom:20}}>
            <Text style = {{fontSize:25}}>Kapat</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    </View>
  )
}

export default DropDownFilter;

